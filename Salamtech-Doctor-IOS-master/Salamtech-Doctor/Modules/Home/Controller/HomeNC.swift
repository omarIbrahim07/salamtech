//
//  AppointmentsNC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/17/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class HomeNC: UINavigationController {
    // MARK:-Outlets
    
    // MARK:-Properties
    
    // MARK:-Override Function
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configuration()
        setupUI()
    }
    
    // MARK:-Methods
    private func configuration(){
        
    }
    private func setupUI(){
        let image = UIImage(named: "back_arrow")
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = false
        
    }
    
    // MARK:-Actions
    
    
}
extension HomeNC: StoryboardMakeable{
    static var storyboardName: String = Storyboard.homeSB.rawValue
    typealias StoryboardMakeableType = HomeNC
}
