//
//  AppointmentsVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/17/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit
import SideMenu
import SwiftEntryKit
class HomeVC: UIViewController, UINavigationControllerDelegate {
    
    //MARK:-Variables
    var initialTouchPoint:CGPoint = CGPoint(x: 0, y: 0)
    lazy var burgerSideMenu = BurgerSideMenuVC.make()
    lazy var menu = SideMenuNavigationController(rootViewController: burgerSideMenu)
    var calenderDates = [CalenderDates]()
    var data = [Appointments]()
    var patients = [Patient]()
    var selectedPatientIndex:IndexPath!
    var consultationID:Int?
    
    //MARK:-Outlets
    @IBOutlet weak var calenderTableView: SelfSizedTableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var todayAppointmentsCollectionView: UICollectionView!
    @IBOutlet weak var numberOfAppointmentsLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var todayLabel: UIButton!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        headerView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 20)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AppointmentDetailsSegue" {
            let indexPath = sender as? IndexPath
            if let vc = segue.destination as? AppointmentDetailsVC{
                vc.delegate = self
                vc.patientAppointmentDetails = data[indexPath!.row]
                vc.selectedPatientIndexPath = selectedPatientIndex
                let cell = calenderTableView.cellForRow(at: indexPath!) as? CalenderCell
                if cell?.cancelledLabel.isHidden == false {
                    vc.isAppointmentCanceled = true
                }else {
                    vc.isAppointmentCanceled = false
                }
            }
            
        } else if segue.identifier == "ChatSegue" {
            if let vc = segue.destination as? ChatVC {
                vc.messagesResponse = sender as? getPatientMessagesResponse
                vc.consultationID = consultationID
            }
        }
    }
    //MARK:-Methods
    private func configuration() {
        configureTodayAppointmentCollectionView()
        configureCalenderTableView()
        getTodayAppointments()
        prepareCalendarDates(with: Date())
    }
    
    private func configureTodayAppointmentCollectionView() {
        let todayAppointmentNib = UINib(nibName: TodayAppointmentCell.identifier, bundle: nil)
        todayAppointmentsCollectionView.register(todayAppointmentNib, forCellWithReuseIdentifier: TodayAppointmentCell.identifier)
        todayAppointmentsCollectionView.allowsMultipleSelection = false
        todayAppointmentsCollectionView.delegate = self
        todayAppointmentsCollectionView.dataSource = self
    }

    private func configureCalenderTableView() {
        let calenderNib = UINib(nibName: CalenderCell.identifier, bundle: nil)
        calenderTableView.register(calenderNib, forCellReuseIdentifier: CalenderCell.identifier)
        calenderTableView.delegate = self
        calenderTableView.dataSource = self
    }
    var subView:UIView?
    private func handleView(action:String) {
        if action == "Delete" {
            subView?.alpha = 1.0
            subView = nil
            subView?.removeFromSuperview()
        }else if action == "Add" {
            subView = UIView()
            view.addSubview(subView!)
            subView?.frame = view.frame
            subView?.alpha = 0.5
        }
    }
    
    private func presentBurgerSideMenu() {
        menu.delegate = self
        menu.leftSide = true
        menu.menuWidth = view.frame.width
        menu.presentationStyle = .menuSlideIn
        burgerSideMenu.dismissingDelegate = self
        present(menu, animated: true, completion: nil)
    }
    private func getTodayAppointments() {
        let todayDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let todayDateInString = formatter.string(from: todayDate)
        getAppointments(date: todayDateInString)
    }
    private func handleSuccess(response:AppointmentsResponse) {
        guard let appointments = response.appointments?.data else {return}
        self.data = appointments
        patients = data.map{$0.patient ?? Patient()}
        numberOfAppointmentsLabel.isHidden = false
        numberOfAppointmentsLabel.text = "\(appointments.count) Appointments"
        UserDefaults.Account.set(appointments.count, forKey: .noOfAppointments)
        calenderTableView.reloadData()
    }
    private func getAppointments(date:String,limit:Int = 1000) {
        showLoading()
        let parameters = AppointmentsParameters(date: date, limit: limit)
        RM.request(service: HomeService.getAppointments(with: parameters)) {[weak self] (result:Result<AppointmentsResponse, RequestError>) in
            guard let self = self else {
                return
            }
            self.hideLoading()
            switch result {
            case .success(let response):
                self.handleSuccess(response: response)
            case .failure(let error):
                let _ = self.bottomAlert(title: error.message,
                                         message: error.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    
    private func prepareCalendarDates(with date: Date?){
        calenderDates.removeAll()
        let dates = Dates.printDatesBetweenInterval(date!,Calendar.current.date(byAdding: .day, value: 6, to: date!)!)
        dates.forEach { (date) in
            calenderDates.append(CalenderDates(date: date, isCellSelected: false))
        }
        print(self.calenderDates.count)
        if dates.count > 0{ calenderDates[0].isCellSelected = true }
        self.todayAppointmentsCollectionView.reloadData()
    }
    
    private func getPatientMessages(with userID:Int) {
        showLoading()
        let parameters = ConsultationParameters(userID: userID, limit: 100)
        print(parameters)
        RM.request(service: HomeService.getPatientMessages(with: parameters)) {[weak self] (result:Result<getPatientMessagesResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success(let response):
                print(response)
                self.performSegue(withIdentifier: "ChatSegue", sender: response)
            case .failure(let error):
                let _ = self.bottomAlert(title: error.message,
                                         message: error.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    
    private func handleTodayAppointmentCellDequeuing(index:Int,cell:TodayAppointmentCell) {
        if calenderDates[index].isCellSelected {
            cell.isCellSelected = true
        }else {
            cell.isCellSelected = false
        }
    }
    
    //MARK:-Actions
    @IBAction func onTapMenu(_ sender: UIBarButtonItem) {
        presentBurgerSideMenu()
        view.alpha = 0.5
        navigationController?.navigationBar.alpha = 0.5
    }
    @IBAction func onTapCalender(_ sender: Any) {
        let _ = showCalender(view: view, viewController: self)
    }
    @IBAction func onTapSearch(_ sender: Any) {
        performSegue(withIdentifier: "SearchSegue", sender: nil)
    }
    @IBAction func onTapOnlineConsultation(_ sender: Any) {
        performSegue(withIdentifier: "ConsultationSegue", sender: nil)
    }
    
}
//MARK:-TableView
extension HomeVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CalenderCell.identifier, for: indexPath) as! CalenderCell
        let time = data[indexPath.row].time
        let timeInDateFormat = Date(timeIntervalSinceReferenceDate: TimeInterval(time ?? 0))
        let timeInString = timeInDateFormat.convertFromTimeStamp()
        cell.configure(patient: patients[indexPath.row], time: timeInString)
        
        if data[indexPath.row].doctorCanceled ?? false {
            cell.cancelledLabel.isHidden = false
            cell.cancelledLabel.text = "Canceled by you"
        }else {
            cell.cancelledLabel.isHidden = true
        }
        return cell
    }
}
extension HomeVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPatientIndex = indexPath
        performSegue(withIdentifier: "AppointmentDetailsSegue", sender:indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
//MARK:-CollectionView
extension HomeVC:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case todayAppointmentsCollectionView:
            return calenderDates.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case todayAppointmentsCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TodayAppointmentCell.identifier, for: indexPath) as! TodayAppointmentCell
            handleTodayAppointmentCellDequeuing(index: indexPath.row, cell: cell)
            cell.configure(date: calenderDates[indexPath.row].date)
            return cell
        default:
            return UICollectionViewCell()
        }        
    }
}
extension HomeVC:UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case todayAppointmentsCollectionView:
            
            for i in calenderDates.indices{
                calenderDates[i].isCellSelected = false
            }
            calenderDates[indexPath.row].isCellSelected = true
            todayAppointmentsCollectionView.reloadData()
            
            getAppointments(date: calenderDates[indexPath.row].date)
        default:
            return
        }
    }
}
extension HomeVC:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case todayAppointmentsCollectionView:
            return CGSize(width: 40, height: 50)
        default:
            return CGSize(width: 40, height: 50)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}


extension HomeVC: StoryboardMakeable{
    static var storyboardName: String = Storyboard.homeSB.rawValue
    typealias StoryboardMakeableType = HomeVC
}
extension HomeVC: SideMenuNavigationControllerDelegate {
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        handleView(action: "Delete")
    }
}
extension HomeVC:IAppointmentDetails {
    func goToMessages(userID: Int, consultationID: Int) {
        getPatientMessages(with: userID)
        self.consultationID = consultationID
    }
    
    func appointmentIsCanceledByDoctor(indexPath: IndexPath) {
        let calenderCell = calenderTableView.cellForRow(at: indexPath) as? CalenderCell
        calenderCell?.cancelledLabel.text = "Cancelled by you"
        calenderCell?.cancelledLabel.isHidden = false
    }
}
extension HomeVC:IBurgerSideMenu{
    func menuIsDismissed() {
        view.alpha = 1.0
        navigationController?.navigationBar.alpha = 1.0
    }
}
extension HomeVC:CalendarViewDelegate {
    func didSelectDate(date:String) {
        print(date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateInDateFormat = dateFormatter.date(from: date)
        SwiftEntryKit.dismiss {
            self.prepareCalendarDates(with: dateInDateFormat)
            self.getAppointments(date: date)
        }
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let dateInString = dateFormatter.string(from: dateInDateFormat!)
        todayLabel.setTitle(dateInString, for: .normal)
    }
}
extension UICollectionView {
    func deselectAllItems(animated: Bool = false) {
        for indexPath in self.indexPathsForSelectedItems ?? [] {
            self.deselectItem(at: indexPath, animated: animated)
        }
    }
}
