//
//  HomeModel.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/13/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation

struct AppointmentsParameters:Codable {
    let date:String
    let limit:Int?
}

struct AppointmentsResponse:Codable {
    let appointments: AppointmentsData?
//    let vacations: [JSONAny]?
}

struct AppointmentsData:Codable {
    let data:[Appointments]?
}
struct Appointments:Codable {
    let id, time, type: Int?
    let date: String?
    let visitReason: String?
    let userCanceled, doctorCanceled: Bool?
    let doctor: Doctor?
    let patient: Patient?
    let address: Address?
    let userFamily: String?

    enum CodingKeys: String, CodingKey {
        case id, date, time
        case visitReason = "visit_reason"
        case type
        case userCanceled = "user_canceled"
        case doctorCanceled = "doctor_canceled"
        case doctor, address
        case userFamily = "user_family"
        case patient = "user"
    }
}
struct Patient:Codable {
    var id,noAppointments:Int?
    var name,code,image,phone,email:String?
    
    enum CodingKeys:String,CodingKey {
        case id,name,code,image,phone,email
        case noAppointments = "no_appointments"
    }
}
struct Address: Codable {
    let id, blockNo: Int?
    let address: String?
//    let floorNo: Int?
    let latitude, longitude: Double?

    enum CodingKeys: String, CodingKey {
        case id
//        case floorNo = "floor_no"
        case blockNo = "block_no"
        case address, latitude, longitude
    }
}


struct CalenderCellData {
    let dayFirstLetter:String
    let dayNumber:String
}

struct CalenderDates {
    var date:String
    var isCellSelected:Bool
}
