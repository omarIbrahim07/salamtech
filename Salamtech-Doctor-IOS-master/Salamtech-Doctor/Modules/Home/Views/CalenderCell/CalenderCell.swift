//
//  CalenderCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/17/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class CalenderCell: UITableViewCell {
    static let identifier = "CalenderCell"
    
    @IBOutlet weak var patientImageView: UIImageView!
    @IBOutlet weak var patientTimeLabel: UILabel!
    @IBOutlet weak var patientcodeLabel: UILabel!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var cancelledLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .default
    }
    
    func configure(patient:Patient,time:String) {
        patientNameLabel.text = patient.name
        patientcodeLabel.text = "Code:\(patient.code ?? "")"
        patientTimeLabel.text = time
        patientImageView.addImage(withImage: patient.image, andPlaceHolder: "profile")
    }
}
