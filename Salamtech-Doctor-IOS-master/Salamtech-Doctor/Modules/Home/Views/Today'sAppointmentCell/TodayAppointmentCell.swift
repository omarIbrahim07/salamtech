//
//  TodayAppointmentCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/17/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class TodayAppointmentCell: UICollectionViewCell {
    
    static let identifier = "TodayAppointmentCell"
    @IBOutlet weak var dayNumberLabel: UILabel!
    @IBOutlet weak var dayNameFirstLetterLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var dotView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }
    var isCellSelected:Bool = false {
        didSet{
            if isCellSelected {
                containerView.alpha = 1.0
                dotView.alpha = 1.0
                dotView.isHidden = false
            }else {
                containerView.alpha = 0.4
                dotView.alpha = 0.4
                dotView.isHidden = true
            }
        }
    }
    //MARK:-Methods
    private func getDayFirstLetter(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "E"
        let day = dateFormatter.string(from: date!)
        return String(day.first!)
    }
    func getDayInNumber(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateInDateFormat = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "d"
        let dateInStringFormat = dateFormatter.string(from: dateInDateFormat ?? Date())
        return dateInStringFormat
    }
    func configure(date:String){
        let dayFirstLetter = getDayFirstLetter(date: date)
        let dayNumber = getDayInNumber(date: date)
        dayNameFirstLetterLabel.text = dayFirstLetter
        dayNumberLabel.text = dayNumber
    }
}
