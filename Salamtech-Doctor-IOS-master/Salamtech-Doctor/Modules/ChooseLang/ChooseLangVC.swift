//
//  ChooseLangVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/17/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import UIKit
class ChooseLangVC:UIViewController {
    
    @IBOutlet weak var arabicBttn: UIButton!
    @IBOutlet weak var englishBttn: UIButton!
    @IBOutlet weak var arabicCheckMark: UIImageView!
    @IBOutlet weak var englishCheckMark: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //MARK:-Methods
    private func handleEnglishSelection() {
        englishBttn.setBackgroundImage(UIImage(named: "English 0"), for: .normal)
        englishCheckMark.isHidden = false
        arabicCheckMark.isHidden = true
    }
    private func handleArabicSelection() {
        englishBttn.setBackgroundImage(UIImage(named: "English"), for: .normal)
        englishCheckMark.isHidden = true
        arabicCheckMark.isHidden = false
    }
    //MARK:-Actions
    @IBAction func onTapEnglish(_ sender: Any) {
        handleEnglishSelection()
    }
    @IBAction func onTapArabic(_ sender: Any) {
        handleArabicSelection()
    }
    @IBAction func onTapNext(_ sender: Any) {
        if englishCheckMark.isHidden && arabicCheckMark.isHidden {
            alert(title: "Warning", message: "Please select the language")
        }else {
            let onBoarding = OnBoardingVC.make()
            onBoarding.modalPresentationStyle = .fullScreen
            present(onBoarding, animated: true, completion: nil)
        }
    }
    
}
extension ChooseLangVC:StoryboardMakeable {
    static var storyboardName: String = Storyboard.chooseLangSB.rawValue
    typealias StoryboardMakeableType = ChooseLangVC
}
