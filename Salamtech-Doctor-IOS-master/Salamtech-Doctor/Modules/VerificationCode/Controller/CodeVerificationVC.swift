//
//  CodeVerificationVC.swift
//  salamtech
//
//  Created by hesham ghalaab on 5/19/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import UIKit

protocol CodeVerificationProtocol {
    func didEndVerification()
}

class CodeVerificationVC: UIViewController {
    
    // MARK: Outlets
    @IBOutlet var codeFields: [UITextField]!
    @IBOutlet var codePlaceholders: [UIImageView]!
    @IBOutlet var codeLines: [UIImageView]!
    
    // MARK: Properties
    var delegate: CodeVerificationProtocol?
    var verifiedCode: NSNumber? = 0
    var email = ""
    var password = ""
    var phone = ""
    var fullName = ""
    var checkCode: Int?
    
    // MARK: Override Functions
    // viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configuration()
        setupUI()
    }
    
    // MARK: Methods
    // configuration: to configure any protocols
    private func configuration(){
        if delegate == nil{ fatalError("you must confirm to CodeVerificationProtocol") }
        codeFields.forEach { (field) in
            field.delegate = self
            field.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        }
    }
    
    // setupUI: to setup data or make a custom design
    private func setupUI(){
        let field = codeFields.first { $0.tag == 0 }
        field?.becomeFirstResponder()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.reloadFieldsUI()
        }
    }
    
    @objc func textFieldDidChanged(_ textField: UITextField){
        let text = textField.text ?? ""
        print("textFieldDidChanged with text: \(text)")
        
        // when writes one digit
        let currentTag = textField.tag
        if text.count == 1{
            if let nextField = codeFields.first(where: { $0.tag == currentTag + 1 }) {
                nextField.becomeFirstResponder()
            }else{
                textField.resignFirstResponder()
            }
            
            if let code = prepareCode() {
                check(code: code)
            }
        }
        
        reloadFieldsUI()
    }
    
    private func reloadFieldsUI(){
        codeFields.forEach { (field) in
            
            let codePlaceholder = codePlaceholders.first(where: { $0.tag == field.tag })
            let codeLine = codeLines.first(where: { $0.tag == field.tag })
            
            if field.isFirstResponder{
                codePlaceholder?.isHidden = true
                codeLine?.image = UIImage(named: "line_waiting")
            }else{
                if let text = field.text, !text.isEmpty{
                    codePlaceholder?.isHidden = true
                    codeLine?.image = UIImage(named: "line_active")
                }else{
                    codePlaceholder?.isHidden = false
                    codeLine?.image = UIImage(named: "line_inactive")
                }
            }
        }
    }
    
    private func prepareCode() -> String?{
        var code = String()
        for field in codeFields {
            guard let text = field.text , !text.isEmpty else {return nil}
            code.append(text)
        }
        return code
    }
    
    private func check(code: String) {
        print("Code is ready: \(code)")
        
        if code != String(checkCode ?? 0000) {
            self.errorToast(with: "Cannot find verification code , please try again later.")
            return
        }
        
        self.dismiss(animated: true) {
            self.delegate?.didEndVerification()
        }
        
//        if code == verifiedCode.stringValue {
//            self.dismiss(animated: true) {
//                self.delegate?.didEndVerification()
//            }
//        }else {
//            let _ = self.bottomAlert(title: "Warning", message: "Wrong code")
//        }
    }
   
    
    // MARK: Actions
    @IBAction func onTapClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension CodeVerificationVC: StoryboardMakeable{
    static var storyboardName: String = Storyboard.codeVerificationSB.rawValue
    typealias StoryboardMakeableType = CodeVerificationVC
}

extension CodeVerificationVC: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString = currentString.replacingCharacters(in: range, with: string) as NSString
//        return newString.length <= maxLength
        
        let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}
