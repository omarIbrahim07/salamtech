//
//  VerificationParameters.swift
//  salamtech
//
//  Created by hesham ghalaab on 7/2/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import Foundation

struct VerificationParameters: Codable {
    let id: String
    let code: String
}
