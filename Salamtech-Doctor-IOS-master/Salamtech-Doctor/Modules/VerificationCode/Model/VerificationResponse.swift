//
//  VerificationResponse.swift
//  salamtech
//
//  Created by hesham ghalaab on 7/2/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import Foundation

struct VerificationResponse: Codable {
    let code:Int
}
