//
//  OnBoardingVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/20/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import UIKit

class OnBoardingVC:UIViewController {
    //MARK:-Variables
    var scrollView:UIScrollView?
    var pageView:UIView?
    let titles = ["Welcome","Salamtech","Guide"]
    let descriptions = ["Your health matters","Register now","Stay safe"]
    //MARK:-Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var customStackView: CustomStackView!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        customStackView.numberOfPages = 3
        customStackView.setupStackViews()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
  
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        configuration()
        setupUI()
    }
    //MARK:-Methods
    private func configuration() {
        pageControl.numberOfPages = titles.count
        setupHorizontalPaging()
    }
    private func setupUI() {
        
    }
    
    private func setupHorizontalPaging() {
        configureScrollView()
        configurePageView()
    }
    private func configureScrollView() {
        scrollView = UIScrollView()
        scrollView?.frame = containerView.bounds
        scrollView?.delegate = self
        scrollView?.showsHorizontalScrollIndicator = false
        containerView.addSubview(scrollView!)
        scrollView?.contentSize = CGSize(width: Int(containerView.frame.size.width) * titles.count, height: 0)
        scrollView?.isPagingEnabled = true
    }
    private func configurePageView() {
        for x in 0..<3 {
            pageView = UIView(frame: CGRect(x: CGFloat(x) * (containerView.frame.size.width), y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height))
            scrollView?.addSubview(pageView!)
            configureDescriptionLabel(x: x)
        }
    }
    private func configureDescriptionLabel(x:Int) {
        let descriptionLabel = UILabel()
        pageView?.addSubview(descriptionLabel)
        descriptionLabel.text = descriptions[x]
        descriptionLabel.font = UIFont(name: "MuseoSans-700", size: 36)
        descriptionLabel.textColor = UIColor.SALAMTECH.blue
        descriptionLabel.numberOfLines = 0
        descriptionLabel.setAnchor(top: nil, left: pageView?.leftAnchor, right: pageView?.rightAnchor, bottom: pageView?.bottomAnchor, paddingBottom: 0, paddingLeft: 40, paddingRight: 102, paddingTop: 0, height: 0, width: 0)
        configureTitleLabel(descriptionLabel: descriptionLabel, x: x)
    }
    private func configureTitleLabel(descriptionLabel:UILabel,x:Int) {
        let titleLabel = UILabel()
        pageView?.addSubview(titleLabel)
        titleLabel.text = titles[x]
        titleLabel.font = UIFont(name: "MuseoSans-300", size: 30)
        titleLabel.textColor = UIColor.SALAMTECH.skyBlue
        titleLabel.setAnchor(top: nil, left: pageView?.leftAnchor, right: nil, bottom: descriptionLabel.topAnchor, paddingBottom: 10, paddingLeft: 40, paddingRight: 0, paddingTop: 0, height: 0, width: 0)
        configureImageView()
    }
    private func configureImageView() {
        let imageView = UIImageView()
        pageView?.addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "OnBoarding")
        imageView.setAnchor(top: pageView?.topAnchor, left: nil, right: pageView?.rightAnchor, bottom: nil, paddingBottom: 0, paddingLeft: 0, paddingRight: -110, paddingTop: 14, height: 354, width: 332)
    }

    //MARK:-Actions
    @IBAction func onTapSkip(_ sender: Any) {
        UserDefaults.Settings.set(true, forKey: .isAppLaunchedBefore)
        guard let window = view.window else {return}
        MainFlowPresenter().navigateToAuthenticationNC(window: window)
    }
    
}
extension OnBoardingVC:UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage = scrollView.contentOffset.x / scrollView.frame.size.width
        customStackView.currentContentOffset = scrollView.contentOffset
        customStackView.currentPage = Int(currentPage)
        self.pageControl.currentPage = Int(currentPage)
    }
}
extension OnBoardingVC: StoryboardMakeable{
    static var storyboardName: String = Storyboard.onBoardingSB.rawValue
    typealias StoryboardMakeableType = OnBoardingVC
}


class Core {
    static let shared = Core()
    
    func isNewUser() -> Bool {
        return !UserDefaults.Settings.bool(forKey: .isAppLaunchedBefore)
    }
    func setIsNotNewUser() {
        return UserDefaults.Settings.set(true, forKey: .isAppLaunchedBefore)
    }
}
