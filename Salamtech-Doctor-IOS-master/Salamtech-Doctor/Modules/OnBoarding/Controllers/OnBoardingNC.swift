//
//  OnBoardingNC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/11/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class OnBoardingNC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
extension OnBoardingNC: StoryboardMakeable{
    static var storyboardName: String = Storyboard.onBoardingSB.rawValue
    typealias StoryboardMakeableType = OnBoardingNC
}
