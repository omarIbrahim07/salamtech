//
//  PatientProfileVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/18/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit
protocol IPatientProfile:class {
    func messageIsTapped(patientID:Int)
}
class PatientProfileVC: UIViewController {
    //MARK:-Variables
    var reportState = ReportState.filled
    var documentState = DocumentsState.filled
    var medicineState = MedicineState.filled
    weak var delegate:IPatientProfile?
    var profileData:PatientEMRResponse?
    //MARK:-Outlets
    @IBOutlet weak var documentsTableView: SelfSizedTableView!
    @IBOutlet weak var addDocumentsView: UIView!
    @IBOutlet weak var addMedicineView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var addReportView: UIView!
    @IBOutlet weak var prescriptionTableView: SelfSizedTableView!
    @IBOutlet weak var medicalReportView: UIView!
    @IBOutlet weak var medicinesTableView: SelfSizedTableView!
    @IBOutlet weak var editMedicalReportBttn: UIButton!
    @IBOutlet weak var editPrescriptionBttn: UIButton!
    @IBOutlet weak var editDocumentsBttn: UIButton!
    @IBOutlet weak var patientCodeLabel: UILabel!
    @IBOutlet weak var patientImageView: UIImageView!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var doctorSpecialityImageView: UIImageView!
    @IBOutlet weak var numberOfVisitsLabel: UILabel!
    @IBOutlet weak var patientCodeUnderNameLabel: UILabel!
    @IBOutlet weak var reportContentLabel: UILabel!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getEMRData()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddPrescriptionSegue" {
            if let vc = segue.destination as? AddPrescriptionVC {
                vc.profileData = profileData
                vc.delegate = self
            }
        }else if segue.identifier == "AddDocumentSegue" {
            if let vc = segue.destination as? AddDocumentsVC {
                vc.profileData = profileData
                vc.delegate = self
            }
        }else if segue.identifier == "AddReportSegue" {
            if let vc = segue.destination as? AddReportVC {
                vc.profileData = profileData
                vc.delegate = self
            }
        }
    }
    //MARK:-Methods
    private func configuration() {
        configurePrescriptionTableView()
        configureDocumentsTableView()
        setPatientData()
    }
    private func setupUI() {
        containerView.roundCorners(corners: [.topRight,.topLeft], radius: 20)
        addReportView.setDashedBorderLine(boundsColor: UIColor.SALAMTECH.skyBlue!, cornerRadius: 5)
        addMedicineView.setDashedBorderLine(boundsColor: UIColor.SALAMTECH.skyBlue!, cornerRadius: 5)
        addDocumentsView.setDashedBorderLine(boundsColor: UIColor.SALAMTECH.skyBlue!, cornerRadius: 5)
        checkState()
    }
    private func setPatientData() {
        patientCodeLabel.text = profileData?.emr?.patient?.code
        patientNameLabel.text = profileData?.emr?.patient?.name
        patientCodeUnderNameLabel.text = "Code:\( profileData?.emr?.patient?.code ?? "")"
        patientImageView.addImage(withImage: profileData?.emr?.patient?.image, andPlaceHolder: "profile")
        numberOfVisitsLabel.text = "\(profileData?.emr?.noAppointments ?? 0) Visits"
        reportContentLabel.text = profileData?.emr?.report
        
    }
    private func checkState() {
        checkReportState()
        checkDocumentState()
        checkMedicineState()
    }
    private func checkReportState() {
        switch  reportState {
        case .filled:
            addReportView.isHidden = true
            editMedicalReportBttn.isHidden = false
            
        case .unfilled:
            medicalReportView.isHidden = true
            editMedicalReportBttn.isHidden = true
            addReportView.isHidden = false
        }
    }
    private func checkDocumentState() {
        switch documentState {
        case .filled:
            addDocumentsView.isHidden = true
            documentsTableView.isHidden = false
            editDocumentsBttn.isHidden = false
            
        case .unfilled:
            documentsTableView.isHidden = true
            editDocumentsBttn.isHidden = true
            addDocumentsView.isHidden = false
            
        }
    }
    private func checkMedicineState() {
        switch medicineState {
        case .filled:
            addMedicineView.isHidden = true
            
        case .unfilled:
            prescriptionTableView.isHidden = true
            editPrescriptionBttn.isHidden = true
            addMedicineView.isHidden = false
        }
    }
    private func configurePrescriptionTableView() {
        let prescriptionNib = UINib(nibName: PrescriptionCell.identifier, bundle: nil)
        prescriptionTableView.register(prescriptionNib, forCellReuseIdentifier: PrescriptionCell.identifier)
        prescriptionTableView.estimatedRowHeight = 55
        prescriptionTableView.delegate = self
        prescriptionTableView.dataSource = self
    }
    private func configureDocumentsTableView() {
        let documentsNib = UINib(nibName: DocumentsCell.identifier, bundle: nil)
        documentsTableView.register(documentsNib, forCellReuseIdentifier: DocumentsCell.identifier)
        documentsTableView.estimatedRowHeight = 60
        documentsTableView.delegate = self
        documentsTableView.dataSource = self
    }
    private func getEMRData() {
        showLoading()
        guard let patientID = profileData?.emr?.patient?.id else {
            let _ = bottomAlert(title: "Alert", message: "Something went wrong,please try again later")
            print("there is no patient ID")
            return
        }
        let parameters = EMRParameters(userID: patientID)
        
        RM.request(service: HomeService.getPatientEMR(with: parameters)) { [weak self] (result:Result<PatientEMRResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success(let response):
                print(response)
                self.profileData = response
                self.setPatientData()
                self.changeState()
                self.checkState()
                self.prescriptionTableView.reloadData()
                self.documentsTableView.reloadData()
            case .failure(let err):
                let _ = self.bottomAlert(title: err.message,
                                         message: err.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    private func changeState() {
        self.documentState = (profileData?.emr?.documents.count != 0) ? .filled:.unfilled
        self.medicineState = (profileData?.emr?.medecines.count != 0) ? .filled:.unfilled
        self.reportState = (profileData?.emr?.report != "") ? .filled:.unfilled
    }
    //MARK:-Actions
    @IBAction func onTapAddReport(_ sender: Any) {
        performSegue(withIdentifier: "AddReportSegue", sender: nil)
    }
    @IBAction func onTapAddPrescription(_ sender: Any) {
        performSegue(withIdentifier: "AddPrescriptionSegue", sender: nil)
    }
    @IBAction func onTapAddDocuments(_ sender: Any) {
        performSegue(withIdentifier: "AddDocumentSegue", sender: nil)
    }
    @IBAction func onTapClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func onTapMessage(_ sender: Any) {
        dismiss(animated: true) {
            self.delegate?.messageIsTapped(patientID: self.profileData?.emr?.patient?.id ?? 0)
        }
    }
    @IBAction func onTapEditMedicalReport(_ sender: Any) {
        self.performSegue(withIdentifier: "AddReportSegue", sender: nil)
    }
    @IBAction func onTapEditPrescriptions(_ sender: Any) {
        self.performSegue(withIdentifier: "AddPrescriptionSegue", sender: nil)
    }
    @IBAction func onTapEditDocuments(_ sender: Any) {
        self.performSegue(withIdentifier: "AddDocumentSegue", sender: nil)
    }
    
    @IBAction func generalHealthInfoButtonIsPressed(_ sender: Any) {
        print("General Health Info")
    }
    
}
extension PatientProfileVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case prescriptionTableView:
            return profileData?.emr?.medecines.count ?? 0
        case documentsTableView:
            print(profileData?.emr?.documents.count ?? 0)
            return profileData?.emr?.documents.count ?? 0
           
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case prescriptionTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: PrescriptionCell.identifier, for: indexPath) as! PrescriptionCell
            cell.configure(medicine: profileData?.emr?.medecines[indexPath.row] ?? Medecine())
            return cell
        case documentsTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: DocumentsCell.identifier, for: indexPath) as! DocumentsCell
            cell.configureForAlreadyUploadedDocuments(document: profileData?.emr?.documents[indexPath.row] ?? Document())
            return cell
        default:
            return UITableViewCell()
        }
        
    }
}
extension PatientProfileVC:UITableViewDelegate {
    
}
extension PatientProfileVC:AddPrescriptionVCDelegate,AddDocumentsVCDelegate,AddReportVCDelegate {
    func addDocumentsIsDismissed() {
        getEMRData()
    }
    func addReportIsDismissed() {
        getEMRData()
    }
    
    func addPrescriptionIsDismissed() {
        getEMRData()
    }
}

enum ReportState {
    case filled
    case unfilled
}
enum DocumentsState {
    case filled
    case unfilled
}
enum MedicineState {
    case filled
    case unfilled
}
