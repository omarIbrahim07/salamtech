//
//  PrescriptionCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/19/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class PrescriptionCell: UITableViewCell {
    //MARK:-Variables
    @IBOutlet weak var medicineTitleLabel: UILabel!
    @IBOutlet weak var medicineDescriptionLabel: UILabel!
    
    static let identifier = "PrescriptionCell"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    func configure(medicine:Medecine) {
        medicineTitleLabel.text = medicine.title
        medicineDescriptionLabel.text = medicine.body
    }
}
