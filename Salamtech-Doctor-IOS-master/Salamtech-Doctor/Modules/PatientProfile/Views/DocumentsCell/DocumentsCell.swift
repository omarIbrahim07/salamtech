//
//  DocumentsCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/19/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit
protocol DocumentsCellDelegate:class {
    func deleteDocument(cell:DocumentsCell)
}
class DocumentsCell: UITableViewCell {
    
    @IBOutlet weak var documentTitleLabel: UILabel!
    @IBOutlet weak var documentSizeLabel: UILabel!
    @IBOutlet weak var documentImageView: UIImageView!
    
    static let identifier = "DocumentsCell"
    weak var delegate:DocumentsCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    func configureForAlreadyUploadedDocuments(document:Document) {
        documentSizeLabel.text = document.size
        documentTitleLabel.text = document.title
        documentImageView.addImage(withImage: document.link, andPlaceHolder: "profile")
    }
    func configureForUploadingDocuments(document:DocumentUploadingDetails,numberOfAlreadyUploadedDocuments:Int,index:Int) {
        documentSizeLabel.text = document.size
        documentTitleLabel.text = document.title
        if index < numberOfAlreadyUploadedDocuments {
            documentImageView.image = document.image
        }else {
            documentImageView.image = document.image
        }
    }
    
    
    @IBAction func onTapDeleteDocument(_ sender: Any) {
        delegate?.deleteDocument(cell: self)
    }
}
