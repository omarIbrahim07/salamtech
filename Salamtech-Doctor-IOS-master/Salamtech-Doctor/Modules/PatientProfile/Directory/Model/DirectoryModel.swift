//
//  DirectoryModel.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/15/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import UIKit

struct UpdateEMRParameters:Codable {
    let emrID:Int
    var report:String
    var medicine:[Medecine]
    var document:[Document]?
    
    enum CodingKeys:String,CodingKey {
        case emrID = "emr_id"
        case report,medicine,document
    }
}

struct DocumentUploadingDetails {
    var id:Int?
    var image:UIImage?
    let size:String
    let title:String
    var imageUrl:String?
}

struct DeleteDocumentParameters:Codable {
    let documentID:Int?
    
    enum CodingKeys:String,CodingKey {
        case documentID = "document_id"
    }
}
