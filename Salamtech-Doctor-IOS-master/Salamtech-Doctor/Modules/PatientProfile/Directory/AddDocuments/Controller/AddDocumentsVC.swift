//
//  AddDocumentsVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 10/11/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit
protocol AddDocumentsVCDelegate:class {
    func addDocumentsIsDismissed()
}
class AddDocumentsVC: UIViewController {
    //MARK:-Variables
    var profileData:PatientEMRResponse?{
        didSet{
            self.documents = profileData?.emr?.documents ?? []
        }
    }
    var documents = [Document](){
        didSet{
            handleDocuments()
        }
    }
    var delegate:AddDocumentsVCDelegate?
    var documentsForUploading = [DocumentUploadingDetails]()
    lazy var numberOfDocumentsHaveAlreadyBeenUploaded = documents.count
    var isNumberOfAlreadyUploadedDocumentsRemoved = false
    //MARK:-Outlets
    @IBOutlet weak var tableView: SelfSizedTableView!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        delegate?.addDocumentsIsDismissed()
    }
    //MARK:-Functions
    private func configuration() {
        configureTableView()
    }
    private func setupUI() {
        
    }
    private func configureTableView() {
        let addDocumentsNib = UINib(nibName: DocumentsCell.identifier, bundle: nil)
        tableView.register(addDocumentsNib, forCellReuseIdentifier: DocumentsCell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
    }
    private func handleDocuments() {
        documents.forEach { [weak self] (document) in
            self?.showLoading()
            DispatchQueue.global().async { [weak self] in
                if let data = try? Data(contentsOf: URL(string:document.link ?? "")!) {
                    let image = UIImage(data: data)
                    self?.documentsForUploading.append(DocumentUploadingDetails(id: document.id, image: image, size: document.size ?? "", title: document.title ?? "", imageUrl: document.link ?? ""))
                }
                DispatchQueue.main.async { [weak self] in
                    self?.hideLoading()
                    self?.tableView.reloadData()
                }
            }
        }
    }
    private func deleteDocument(documentID:Int,completion:@escaping (()->())) {
        showLoading()
        let parameters = DeleteDocumentParameters(documentID: documentID)
        RM.request(service: HomeService.deleteDocument(with: parameters)) { [weak self]  (result:Result<TheDefaultResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success:
                completion()
            case .failure(let error):
                let _ = self.bottomAlert(title: error.message,
                                         message: error.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    private func handleDocumentSelection() {
        ImagePickerManager().pickImage(self) { [weak self] (image,imageName)  in
            let data = image.jpegData(compressionQuality: 1.0)!
            let size = Float(Double((data.count)/1024/1024))
            let sizeWithTwoDecimalsInMB = Double(round(10 * size) / 10)
            if sizeWithTwoDecimalsInMB < 1 {
                let sizeInKB = ((data.count)/1024)
                self?.documentsForUploading.append(DocumentUploadingDetails(image: image, size: "\(sizeInKB)KB", title: imageName))
            }else {
                self?.documentsForUploading.append(DocumentUploadingDetails(image: image, size: "\(sizeWithTwoDecimalsInMB)MB", title: imageName))
            }
            self?.tableView.reloadData()
        }
    }
    private func removeAlreadyUploadedDocuments() {
        if isNumberOfAlreadyUploadedDocumentsRemoved == false {
            for _ in 0..<numberOfDocumentsHaveAlreadyBeenUploaded {
                documentsForUploading.remove(at: 0)
            }
            print(documentsForUploading)
            isNumberOfAlreadyUploadedDocumentsRemoved = true
        }else {
            print(documentsForUploading)
            return
        }
    }
    private func updateDocuments(completion:@escaping () -> () ) {
        showLoading()
        guard let emrID = profileData?.emr?.id else {
            let _ = bottomAlert(title: "Warning ", message: "Something went wrong")
            return
        }
        let medicalReport = profileData?.emr?.report ?? ""
        let medicines = profileData?.emr?.medecines ?? []
        let parameters = UpdateEMRParameters(emrID: emrID, report: medicalReport, medicine: medicines)
        removeAlreadyUploadedDocuments()
        RM.updateEMR(with: parameters, documents: documentsForUploading) { [weak self] (result:Result<TheDefaultResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success:
                print("Succeeded")
                completion()
            case .failure(let error):
                let _ = self.bottomAlert(title: error.message,
                                         message: error.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    //MARK:-Actions
    @IBAction func onTapAddDocument(_ sender: Any) {
        handleDocumentSelection()
        tableView.reloadData()
    }
    @IBAction func onTapUpdateDocuments(_ sender: Any) {
        updateDocuments {
            self.dismiss(animated: true) {
                self.delegate?.addDocumentsIsDismissed()
            }
        }
    }
    
    @IBAction func closeButtonIsPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension AddDocumentsVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documentsForUploading.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DocumentsCell.identifier, for: indexPath) as! DocumentsCell
        cell.configureForUploadingDocuments(document: documentsForUploading[indexPath.row],numberOfAlreadyUploadedDocuments: numberOfDocumentsHaveAlreadyBeenUploaded,index: indexPath.row)
        cell.delegate = self
        return cell
    }
}
extension AddDocumentsVC:DocumentsCellDelegate {
    func deleteDocument(cell: DocumentsCell) {
        print(documents.count,documentsForUploading.count)
        guard let index = tableView.indexPath(for: cell) else {return}
        guard let documentID = documentsForUploading[index.row].id else {
            documentsForUploading.remove(at: index.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [index], with: .left)
            tableView.endUpdates()
            return
        }
        deleteDocument(documentID: documentID) { [weak self] in
            self?.documentsForUploading.remove(at: index.row)
            self?.tableView.beginUpdates()
            self?.tableView.deleteRows(at: [index], with: .left)
            self?.tableView.endUpdates()
            self?.numberOfDocumentsHaveAlreadyBeenUploaded -= 1
        }
    }
}
