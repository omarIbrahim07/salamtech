//
//  AddPrescriptionVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 10/11/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import UIKit
protocol AddPrescriptionVCDelegate:class {
    func addPrescriptionIsDismissed()
}
class AddPrescriptionVC:UIViewController {
    //MARK:-Variables
    var profileData:PatientEMRResponse?
    var medicines = [Medecine]()
    var delegate:AddPrescriptionVCDelegate?
    //MARK:-Outlets
    @IBOutlet weak var tableView: SelfSizedTableView!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print(profileData?.emr?.medecines.count ?? 0)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        delegate?.addPrescriptionIsDismissed()
    }
    //MARK:-Functions
    private func configuration() {
        medicines = profileData?.emr?.medecines ?? []
        configureTableView()
    }
    private func setupUI() {
        
    }
    private func configureTableView() {
        let prescriptionNib = UINib(nibName: AddPrescriptionCell.identifier, bundle: nil)
        tableView.register(prescriptionNib, forCellReuseIdentifier: AddPrescriptionCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
    }
    private func updateMedicines(completion:@escaping () -> () ) {
        showLoading()
        let report = profileData?.emr?.report ?? ""
        guard let emrID = profileData?.emr?.id else {
            let _ = bottomAlert(title: "Warning", message: "Something went wrong")
            return
        }
        let parameters = UpdateEMRParameters(emrID: emrID, report: report, medicine: medicines)
        RM.updateEMR(with: parameters) { [weak self] (result:Result<TheDefaultResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success:
                print("Succeeded")
                completion()
            case .failure(let error):
                let _ = self.bottomAlert(title: error.message,
                                         message: error.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    //MARK:-Actions
    @IBAction func onTapAddMedicines(_ sender: Any) {
        medicines.append(Medecine(title: "", body: "Medicine description"))
        tableView.reloadData()
    }
    
    @IBAction func onTapUpdateMedicines(_ sender: Any) {
        updateMedicines {
            self.dismiss(animated: true) {
                self.delegate?.addPrescriptionIsDismissed()
            }
        }
    }
    
    @IBAction func closeButtonIsPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
//MARK:-Extension
extension AddPrescriptionVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return medicines.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AddPrescriptionCell.identifier, for: indexPath) as! AddPrescriptionCell
        cell.configure(medicine: medicines[indexPath.row])
        cell.delegate = self
        return cell
    }
}

extension AddPrescriptionVC:AddPrescriptionCellDelegate {
    func didTypeMedicineTitle(medicineTitle: String, cell: AddPrescriptionCell) {
        print(medicineTitle)
        if let index = tableView.indexPath(for: cell)?.row {
            if medicineTitle == "" {
                medicines.remove(at: index)
            } else {
                medicines[index].title = medicineTitle
            }
        }
    }
    func didTypeMedicineDescription(medicineDescription: String, cell: AddPrescriptionCell) {
        if let index = tableView.indexPath(for: cell)?.row {
            medicines[index].body = medicineDescription
        }
    }
}
