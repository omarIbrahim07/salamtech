//
//  AddPrescriptionCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/19/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit
protocol AddPrescriptionCellDelegate:class {
    func didTypeMedicineTitle(medicineTitle:String,cell:AddPrescriptionCell)
    func didTypeMedicineDescription(medicineDescription:String,cell:AddPrescriptionCell)
}
class AddPrescriptionCell: UITableViewCell {
    
    static let identifier = "AddPrescriptionCell"
    weak var delegate:AddPrescriptionCellDelegate?
    @IBOutlet weak var medicineTitleTxtField: UITextField!
    @IBOutlet weak var medicineBodyTxtView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        medicineBodyTxtView.delegate = self
        medicineTitleTxtField.delegate = self
    }
    
    func configure(medicine:Medecine) {
        medicineTitleTxtField.text = medicine.title
        medicineBodyTxtView.text = medicine.body
    }
}

extension AddPrescriptionCell:UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
            delegate?.didTypeMedicineTitle(medicineTitle: textField.text ?? "", cell: self)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension AddPrescriptionCell:UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Medicine description" {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Medicine description"
        } else {
            delegate?.didTypeMedicineDescription(medicineDescription: textView.text ?? "", cell: self)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n")
        {
            contentView.endEditing(true)
            return false
        }
        else
        {
            return true
        }
    }
}
