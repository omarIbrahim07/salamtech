//
//  SearchResultCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/17/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class SearchResultCell: UITableViewCell {
    static let identifier = "SearchResultCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
}
