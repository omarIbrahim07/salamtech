//
//  SearchController.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/17/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import UIKit

//the shadowView,tableView and result Label are initially hidden and when fetching succeeds they will appear.
class SearchVC:UIViewController {
    //MARK:-Variables
    
    //MARK:-Outlets
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var searchResultsTableView: UITableView!
    @IBOutlet weak var resultsLabel: UILabel!
    @IBOutlet weak var searchTxtField: UITextField!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        headerView.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 20)
    }
    //MARK:-Methods
    private func configuration() {
        configureSearchResultTableView()
    }
    private func setupUI() {
        
    }
    private func configureSearchResultTableView() {
        let nib = UINib(nibName: SearchResultCell.identifier, bundle: nil)
        searchResultsTableView.register(nib, forCellReuseIdentifier: SearchResultCell.identifier)
        searchResultsTableView.delegate = self
        searchResultsTableView.dataSource = self
    }
    //MARK:-Actions
    @IBAction func onTapClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
extension SearchVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchResultCell.identifier, for: indexPath) as! SearchResultCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}
extension SearchVC:UITableViewDelegate {
    
}
