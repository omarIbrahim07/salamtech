//
//  SearchNC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/17/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import UIKit
class SearchNC: UINavigationController {
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
        // Do any additional setup after loading the view.
    }
    private func configuration(){
        
    }
    
    // setupUI: to setup data or make a custom design
    private func setupUI(){
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.isTranslucent = false
    }
    //MARK:-Methods
}
extension SearchNC:StoryboardMakeable {
    static var storyboardName: String = Storyboard.searchSB.rawValue
    typealias StoryboardMakeableType = SearchNC
}
