//
//  ConsultationNC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/18/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import UIKit
class ConsultationNC: UINavigationController {

    // MARK: Outlets
    
    // MARK: Properties
    
    // MARK: Override Functions
    // viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configuration()
        setupUI()
    }
    
    // MARK: Methods
    // configuration: to configure any protocols
    private func configuration(){
        
    }
    
    // setupUI: to setup data or make a custom design
    private func setupUI(){
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.isTranslucent = false
    }
    
    // MARK: Actions
}

extension ConsultationNC: StoryboardMakeable{
    static var storyboardName: String = Storyboard.consultationSB.rawValue
    typealias StoryboardMakeableType = ConsultationNC
}
