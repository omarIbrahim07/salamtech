//
//  ConsultationVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/18/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class ConsultationVC: UIViewController {
    //MARK:-Variables
    
    //MARK:-Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noMessagesView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var messagesView: UIView!
    @IBOutlet weak var noMessageImageView: UIImageView!
    @IBOutlet weak var noMessageLabel: UILabel!
    
    var consultations = [Consultations]()
    var selectedConsultationID:Int!
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        headerView.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 20)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.isHidden = false
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ChatSegue" {
            if let vc = segue.destination as? ChatVC {
                vc.messagesResponse = sender as? getPatientMessagesResponse
                vc.consultationID = selectedConsultationID
            }
        }
    }
    
    //MARK:-Methods
    private func configuration() {
        configureTableView()
        getUsersForConsultations()
    }
    private func setupUI() {
        setupForEmptyConsultations()
    }
    private func setupForEmptyConsultations() {
        if consultations.isEmpty {
            tableView.isHidden = true
        }else {
            tableView.isHidden = false
        }
    }
    private func configureTableView(){
        let nib = UINib(nibName: MessageCell.identifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: MessageCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
    }
    private func getUsersForConsultations() {
        showLoading()
        RM.request(service: HomeService.getPatientConsultations) {[weak self] (result:Result<ConsultationsResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success(let response):
                self.handleGetConsultationMessages(response: response)
            case .failure(let error):
                let _ = self.bottomAlert(title: error.message,
                                         message: error.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    private func handleGetConsultationMessages(response:ConsultationsResponse) {
        if response.consulations.data.count > 0 {
            noMessageLabel.isHidden = true
            noMessageImageView.isHidden = true
            tableView.isHidden = false
        }
        self.consultations = response.consulations.data
        tableView.reloadData()
    }
    private func getPatientMessages(with index:Int) {
        showLoading()
        guard let userID = consultations[index].user?.id else {return}
        let parameters = ConsultationParameters(userID: userID, limit: 100)
        print(parameters)
        RM.request(service: HomeService.getPatientMessages(with: parameters)) {[weak self] (result:Result<getPatientMessagesResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success(let response):
                print(response)
                self.performSegue(withIdentifier: "ChatSegue", sender: response)
            case .failure(let error):
                let _ = self.bottomAlert(title: error.message,
                                         message: error.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    //MARK:-Actions
    @IBAction func onTapClose(_ sender: Any) {
//        dismiss(animated: true, completion: nil)
        guard let window = view.window else {return}
        MainFlowPresenter().navigateToHomeNC(window: window)
    }
    
}
extension ConsultationVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return consultations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MessageCell.identifier, for: indexPath) as! MessageCell
        cell.configure(patient: consultations[indexPath.row].user ?? Patient())
        return cell
    }
}
extension ConsultationVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
        selectedConsultationID = consultations[indexPath.row].id
        getPatientMessages(with: indexPath.row)
    }
}

