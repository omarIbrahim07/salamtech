//
//  MessageCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/18/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    //MARK:-Variables
    static let identifier = "MessageCell"
    @IBOutlet weak var patientImageView: UIImageView!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var patientNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    
    func configure(patient:Patient){
        patientNameLabel.text = patient.name
        codeLabel.text = "Code:\(patient.code ?? "")"
        patientImageView.addImage(withImage: patient.image, andPlaceHolder: "profile")
    }
}
