//
//  ConsultationModel.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/13/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation

struct ConsultationParameters:Codable {
    let userID,limit:Int
    
    enum CodingKeys:String,CodingKey {
        case userID = "user_id"
        case limit
    }
}
struct ConsultationsResponse: Codable {
    let consulations: ConsulationsData
}

// MARK: - Consulations
struct ConsulationsData: Codable {
    let data: [Consultations]
}

// MARK: - Datum
struct Consultations: Codable {
    let id, type: Int?
    var user: Patient?
}

//MARK:- Consultations Messages Model

struct getPatientMessagesResponse: Codable {
    let user: Patient?
    let messsages: MesssagesData?
}

// MARK: - Messsages
struct MesssagesData: Codable {
    let data: [Messages]
    let pagination: Pagination?
}
struct Messages:Codable {
    let id, sender: Int?
    let message,date:String
//    let id: Int?
//    let message,sender,date:String
    let isSeen:Bool
    
    enum CodingKeys:String,CodingKey {
        case id,sender,date
        case message = "msg"
        case isSeen = "seen"
    }
}
// MARK: - Pagination
struct Pagination: Codable {
    let currentPage,from,lastPage,total,to,perPage: Int?
    let firstPageURL,lastPageURL,nextPageURL,path,prevPageURL: String?

    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case firstPageURL = "first_page_url"
        case from
        case lastPage = "last_page"
        case lastPageURL = "last_page_url"
        case nextPageURL = "next_page_url"
        case path
        case perPage = "per_page"
        case prevPageURL = "prev_page_url"
        case to, total
    }
}
