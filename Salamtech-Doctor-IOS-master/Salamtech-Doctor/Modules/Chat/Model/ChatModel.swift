//
//  ChatModel.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/14/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
struct SendMessageParameters:Codable {
    let consultationID:Int
    let message:String
    
    enum CodingKeys:String,CodingKey {
        case consultationID = "consultation_id"
        case message = "msg"
    }
}
