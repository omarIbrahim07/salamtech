//
//  RecieverCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/18/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class RecieverCell: UITableViewCell {
    
    @IBOutlet weak var messageTimeLabel: UILabel!
    @IBOutlet weak var patientImageView: UIImageView!
    @IBOutlet weak var recieverMessageLabel: UILabel!
    static let identifier = "RecieverCell"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    //MARK:-Configure
    func configure(patient:getPatientMessagesResponse,index:Int) {
        patientImageView.addImage(withImage: patient.user?.image, andPlaceHolder: "profile")
        recieverMessageLabel.text = patient.messsages?.data[index].message
        let timeInDateFormat = Date(timeIntervalSinceReferenceDate: TimeInterval(patient.messsages?.data[index].date ?? "0") ?? 0)
        let time = timeInDateFormat.convertFromTimeStamp()
        messageTimeLabel.text = time
    }
}
