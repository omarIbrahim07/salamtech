//
//  SenderCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/18/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class SenderCell: UITableViewCell {
    static let identifier = "SenderCell"
    @IBOutlet weak var senderMessageView: UIView!
    @IBOutlet weak var senderMessageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var isSeenImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    var messagesViewSizes = [CGSize]()
    func configure(patient:getPatientMessagesResponse,index:Int) {
        senderMessageLabel.text = patient.messsages?.data[index].message
        let timeInDateFormat = Date(timeIntervalSinceReferenceDate: TimeInterval(patient.messsages?.data[index].date ?? "0") ?? 0)
        let timeInString = timeInDateFormat.convertFromTimeStamp()
        timeLabel.text = timeInString
        if patient.messsages?.data[index].isSeen ?? false {
            isSeenImageView.tintColor = UIColor.SALAMTECH.blue!
        }else {
            isSeenImageView.tintColor = UIColor.SALAMTECH.darkGray!
        }
    }
}
