//
//  ChatVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/18/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class ChatVC: UIViewController {
    
    // MARK:-Variables
    var messagesResponse:getPatientMessagesResponse?
    var consultationID:Int!
    //MARK:-Outlets
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var patientImageView: UIImageView!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var patientCodeLabel: UILabel!
    @IBOutlet weak var messageTxtView: UITextView!
    @IBOutlet weak var typeMessageView: UIView!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
        getPatientMessages()
        //view
//        scrollToBottomOfChat()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        headerView.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 20)
        
    }
    
    //MARK:-Methods
    private func configuration() {
        configureTableView()
        adjustUITextViewHeight(arg: messageTxtView)
        setPatientData()
        configureTxtView()
        self.tableView.layoutIfNeeded()
    }
    
    private func setupUI() {
        navigationController?.navigationBar.isHidden = true
    }
    
    private func configureTxtView() {
        messageTxtView.delegate = self
    }
    
    private func setPatientData() {
        patientImageView.addImage(withImage: messagesResponse?.user?.image, andPlaceHolder: "profile")
        patientNameLabel.text = messagesResponse?.user?.name
        patientCodeLabel.text = "Code:\(messagesResponse?.user?.code ?? "")"
    }
    
    private func configureTableView() {
        let recieverNib = UINib(nibName: RecieverCell.identifier, bundle: nil)
        let senderNib = UINib(nibName: SenderCell.identifier, bundle: nil)
        tableView.register(senderNib, forCellReuseIdentifier: SenderCell.identifier)
        tableView.register(recieverNib, forCellReuseIdentifier: RecieverCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.transform = CGAffineTransform(scaleX: 1, y: -1)
    }
    
    func scrollToBottomOfChat() {
        if messagesResponse?.messsages?.data.count ?? 0 > 0 {
            let indexPath = IndexPath(row: messagesResponse?.messsages?.data.count ?? 0 - 1, section: 0)
            tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }
    
    private func adjustUITextViewHeight(arg : UITextView) {
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
    private func getPatientMessages() {
        //        showLoading()
        guard let userID = messagesResponse?.user?.id else {return}
        let parameters = ConsultationParameters(userID: userID, limit: 100)
        print("x: ", parameters)
        RM.request(service: HomeService.getPatientMessages(with: parameters)) {[weak self] (result:Result<getPatientMessagesResponse, RequestError>) in
            guard let self = self else {return}
            //            self.hideLoading()
            switch result {
            case .success(let response):
                self.messagesResponse = response
                self.tableView.reloadData()
                let indexPath = IndexPath(row: (self.messagesResponse?.messsages?.data.count)! - 1, section:0)
//                self.tableView.scrollToRow(at: indexPath, at:.top, animated:true)
//                self.tableView.reloadData()
            case .failure(let error):
                let _ = self.bottomAlert(title: error.message,
                                         message: error.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    
    private func sendMessage(message:String) {
        let parameters = SendMessageParameters(consultationID: consultationID, message: message)
        RM.request(service: HomeService.sendDoctorMessage(with: parameters)) {[weak self] (result:Result<TheDefaultResponse, RequestError>) in
            guard let self = self else {return}
            switch result {
            case .success:
                self.getPatientMessages()
            case .failure(let err):
                let _ = self.bottomAlert(title: err.message,
                                         message: err.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    //MARK:-Actions
    @IBAction func onTapBackBttn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension ChatVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesResponse?.messsages?.data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let messagesResponse = messagesResponse else {return UITableViewCell()}
        
        if messagesResponse.messsages?.data[indexPath.row].sender == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: RecieverCell.identifier, for: indexPath) as! RecieverCell
            cell.transform = CGAffineTransform(scaleX: 1, y: -1)
            cell.configure(patient: messagesResponse, index: indexPath.row)
            
            return cell
        }else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: SenderCell.identifier, for: indexPath) as! SenderCell
            cell.configure(patient: messagesResponse, index: indexPath.row)
            cell.transform = CGAffineTransform(scaleX: 1, y: -1)
            return cell
        }
    }
    
}

extension ChatVC:UITableViewDelegate {
    
}

extension ChatVC:UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
        textView.textColor = UIColor.SALAMTECH.blue
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.textColor = .lightGray
            textView.text = "Type your message here"
        }else {
            sendMessage(message: textView.text)
            textView.textColor = .lightGray
            textView.text = "Type your message here"
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n")
        {
            view.endEditing(true)
            return false
        }
        else
        {
            return true
        }
    }
    
}

