//
//  ProfileVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 10/5/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    //MARK:-Variables
    private lazy var generalInfoVC:GeneralInfoVC = {
        var viewController = GeneralInfoVC.make()
        return viewController
    }()
    private lazy var workClinicInfoVC:WorkClinicInfoVC = {
        var viewController = WorkClinicInfoVC.make()
        return viewController
    }()
    var state = ProfileState.generalInfo
    var profileData:ProfileModel?
    //MARK:-Outlets
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var HelloDoctorNameLbl: UILabel!
    @IBOutlet weak var generalnfoView: UIView!
    @IBOutlet weak var workClinicInfoView: UIView!
    @IBOutlet weak var workClinicInfoBttn: UIButton!
    @IBOutlet weak var generalInfoBttn: UIButton!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        headerView.roundCorners(corners: [.bottomRight,.bottomLeft], radius: 20)
    }
    //MARK:-Functions
    private func configuration() {
        addGeneralInfoVC()
        getProfileData(vc: nil)
    }
    private func setupUI() {
        
    }
    private func addGeneralInfoVC() {
        addChild(childController: generalInfoVC, to: generalnfoView)
    }
    private func addWorkClinicInfo() {
        addChild(childController: workClinicInfoVC, to: workClinicInfoView)
    }
    private func removeGeneralInfoVC() {
        removeChild(childViewController: generalInfoVC, from: generalnfoView)
    }
    private func removeWorkClinicInfoVC() {
        removeChild(childViewController: workClinicInfoVC, from: workClinicInfoView)
    }
    private func addChild(childController:UIViewController, to childContentView:UIView) {
        addChild(childController)
        childContentView.addSubview(childController.view)
        childController.didMove(toParent: self)
        childController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            childController.view.leadingAnchor.constraint(equalTo: childContentView.leadingAnchor),
            childController.view.trailingAnchor.constraint(equalTo: childContentView.trailingAnchor),
            childController.view.bottomAnchor.constraint(equalTo: childContentView.bottomAnchor),
            childController.view.topAnchor.constraint(equalTo: childContentView.topAnchor)
        ])
        childContentView.isHidden = false
    }
    private func removeChild(childViewController:UIViewController,from subView:UIView) {
        childViewController.willMove(toParent: nil)
        childViewController.view.removeFromSuperview()
        childViewController.removeFromParent()
        subView.isHidden = true
    }
    
    private func getProfileData(vc: String?) {
        showLoading()
        print("inside get profile data")
        RM.request(service: ProfileService.getProfileData) { [weak self] (result:Result<ProfileModel, RequestError>) in
            guard let self = self else {return}
            DispatchQueue.main.async {
                print("returned from getting data")
                self.hideLoading()
            }
           
            switch result {
            case .success(let response):
                self.profileData = response
                self.setProfileData(vc: vc)
            case .failure(let err):
                let _ = self.bottomAlert(title: err.message,
                                         message: err.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    
    private func setProfileData(vc: String?) {
        HelloDoctorNameLbl.text = "Hello, Dr. \(profileData?.doctor?.name ?? "")"
        generalInfoVC.setGeneralInfo(profileData: profileData ?? ProfileModel())
        if vc == "clinic info" {
            workClinicInfoVC.setWorkClinicInfo(profileData: profileData ?? ProfileModel())
        }
    }
    
    private func setClinicData() {
        HelloDoctorNameLbl.text = "Hello, Dr. \(profileData?.doctor?.name ?? "")"
        workClinicInfoVC.setWorkClinicInfo(profileData: profileData ?? ProfileModel())
    }
    
    //MARK:-Actions
    @IBAction func onTapGeneralInfo(_ sender: Any) {
        state = .generalInfo
        removeWorkClinicInfoVC()
        addGeneralInfoVC()
        generalInfoBttn.alpha = 1
        workClinicInfoBttn.alpha = 0.5
        generalInfoVC.setGeneralInfo(profileData: profileData ?? ProfileModel())
    }
    @IBAction func onTapWorkAndClinicInfo(_ sender: Any) {
        state = .workClinic
        removeGeneralInfoVC()
        addWorkClinicInfo()
        workClinicInfoBttn.alpha = 1
        generalInfoBttn.alpha = 0.5
        workClinicInfoVC.setWorkClinicInfo(profileData: profileData ?? ProfileModel())
    }
    @IBAction func onTapEditProfile(_ sender: Any) {
        switch state {
        case .generalInfo:
            let editGeneralInfo = EditGeneralInfoVC.make()
            editGeneralInfo.profileData = profileData
            editGeneralInfo.delegate = self
            present(editGeneralInfo, animated: true, completion: nil)
        case .workClinic:
            let editWorkClinic = EditWorkClinicInfoVC.make()
            editWorkClinic.profileData = profileData
            editWorkClinic.delegate = self
            present(editWorkClinic, animated: true, completion: nil)
        }
    }
    
    @IBAction func closeButtonIsPressed(_ sender: Any) {
        guard let window = view.window else {return}
        MainFlowPresenter().navigateToHomeNC(window: window)
    }
    
}
//MARK:-Extension
enum ProfileState {
    case generalInfo
    case workClinic
}

extension ProfileVC:StoryboardMakeable {
    static var storyboardName: String = Storyboard.profileSB.rawValue
    typealias StoryboardMakeableType = ProfileVC
}

extension ProfileVC:EditGeneralInfoDelegate {
    func onEditingCompletion(vc: String) {
        if vc == "general info" {
            getProfileData(vc: vc)
        } else if vc == "clinic info" {
            getProfileData(vc: vc)
        }
    }
}
