//
//  GeneralInfoVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 10/5/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class GeneralInfoVC: UIViewController {
    
    //MARK:-Variables
    private var profileData: ProfileModel?
    
    //MARK:-Outlets
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var doctorEmailLabel: UILabel!
    @IBOutlet weak var doctorPhoneLabel: UILabel!
    @IBOutlet weak var doctorBirthdateDayLabel: UILabel!
    @IBOutlet weak var doctorBirthdateMonthLabel: UILabel!
    @IBOutlet weak var doctorBirthdateYearLabel: UILabel!
    @IBOutlet weak var doctorGenderLabel: UILabel!
    @IBOutlet weak var doctorLevelOfSeniorityLabel: UILabel!
    @IBOutlet weak var doctorSpecializationLabel: UILabel!
    @IBOutlet weak var doctorSubSpecialityLabel: UILabel!
    @IBOutlet weak var certificatesTableView: SelfSizedTableView!
    @IBOutlet weak var servicesTableView: SelfSizedTableView!
    @IBOutlet weak var documentsTableView: SelfSizedTableView!
    @IBOutlet weak var totalNumberOfVisitsLabel: UILabel!
    @IBOutlet weak var totalNumberOfAppointmentsLabel: UILabel!
    @IBOutlet weak var validFromLabel: UILabel!
    @IBOutlet weak var untilLabel: UILabel!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    //MARK:-Functions
    private func configuration() {
        configureCertificatesTableView()
        configureServicesTableView()
        configureDocumentTableView()
    }
    
    private func setupUI() {
        
    }
    
    func setGeneralInfo(profileData:ProfileModel) {
        self.profileData = profileData
        UserDefaults.Account.set(value: profileData.doctor, key: .user)
        certificatesTableView.reloadData()
        servicesTableView.reloadData()
        documentsTableView.reloadData()
        doctorNameLabel.text = profileData.doctor?.name
        doctorEmailLabel.text = profileData.doctor?.email
        doctorPhoneLabel.text = profileData.doctor?.phone
//        doctorBirthdateDayLabel.text = String((profileData.doctor?.birthDate?.suffix(2))!)
        doctorBirthdateDayLabel.text = String((profileData.doctor?.birthDate?.suffix(2)) ?? "")
        guard let mySubString = profileData.doctor?.birthDate?.getSubString(startIndex: 5, endIndex: 7) else {return}
        doctorBirthdateMonthLabel.text = String(mySubString)
        doctorBirthdateYearLabel.text = String((profileData.doctor?.birthDate?.prefix(4))!)
        doctorGenderLabel.text = profileData.doctor?.gender == "1" ? "Male" : "Female"
        doctorLevelOfSeniorityLabel.text = profileData.doctor?.seniorityLevel
        doctorSpecializationLabel.text = profileData.doctor?.specialist?.name
        doctorSubSpecialityLabel.text = profileData.doctor?.subSpecialist?.first
        totalNumberOfVisitsLabel.text = String(profileData.doctor?.noOfVisits ?? 0)
//        totalNumberOfVisitsLabel.text = "\(profileData.doctor?.noOfVisits ?? 0)"
        totalNumberOfAppointmentsLabel.text = "\(profileData.doctor?.noOfAppointments ?? 0)"
//        validFromLabel.text = profileDa
    }
    
    private func configureCertificatesTableView() {
        let nib = UINib(nibName: EducationCell.identifier, bundle: nil)
        certificatesTableView.register(nib, forCellReuseIdentifier: EducationCell.identifier)
        certificatesTableView.delegate = self
        certificatesTableView.dataSource = self
    }
    
    private func configureServicesTableView() {
        let nib = UINib(nibName: ServicesCell.identifier, bundle: nil)
        servicesTableView.register(nib, forCellReuseIdentifier: ServicesCell.identifier)
        servicesTableView.delegate = self
        servicesTableView.dataSource = self
    }
    
    private func configureDocumentTableView() {
        let nib = UINib(nibName: DocumentCell.identifier, bundle: nil)
        documentsTableView.register(nib, forCellReuseIdentifier: DocumentCell.identifier)
        documentsTableView.dataSource = self
        documentsTableView.delegate = self
    }
    
    //MARK:-Actions
}
//MARK:-Extensions
extension GeneralInfoVC:StoryboardMakeable {
    static var storyboardName: String = Storyboard.generalInfoSB.rawValue
    typealias StoryboardMakeableType = GeneralInfoVC
}
extension GeneralInfoVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case certificatesTableView:
            return profileData?.doctor?.certifications?.count ?? 0
        case servicesTableView:
            return profileData?.doctor?.clinic?.services?.count ?? 0
        case documentsTableView:
            return profileData?.doctor?.documents?.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case certificatesTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: EducationCell.identifier, for: indexPath) as! EducationCell
            cell.clearImageView.isHidden = true
            cell.configure(certificateTitle: profileData?.doctor?.certifications?[indexPath.row].title ?? "", certificateBody: profileData?.doctor?.certifications?[indexPath.row].body ?? "")
            return cell
        case servicesTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: ServicesCell.identifier, for: indexPath) as! ServicesCell
            cell.clearImageView.isHidden = true
            cell.configure(serviceTitle: profileData?.doctor?.clinic?.services?[indexPath.row] ?? "")
            return cell
        case documentsTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: DocumentCell.identifier, for: indexPath) as! DocumentCell
            print(profileData?.doctor?.documents?.count ?? 0)
            cell.clearButton.isHidden = true
            cell.configure(imageLink: profileData?.doctor?.documents?[indexPath.row].link, imageName: profileData?.doctor?.documents?[indexPath.row].title ?? "", imageSize: profileData?.doctor?.documents?[indexPath.row].size ?? "")
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}
