//
//  WorkClinicInfoVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 10/5/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class WorkClinicInfoVC: UIViewController {
    //MARK:-Variables
    private var profileData:ProfileModel?
    //MARK:-Outlets
    @IBOutlet weak var clinicCityLabel: UILabel!
    @IBOutlet weak var clinicAreaLabel: UILabel!
    @IBOutlet weak var clinicBlockNoLabel: UILabel!
    @IBOutlet weak var clinicFloorNoLabel: UILabel!
    @IBOutlet weak var clinicAddressLabel: UILabel!
    @IBOutlet weak var clinicWorkingDaysLabel: UILabel!
    @IBOutlet weak var clinicWorkingFromHourLabel: UILabel!
    @IBOutlet weak var clinicWorkingToHourLabel: UILabel!
    @IBOutlet weak var patientPerHourLabel: UILabel!
    @IBOutlet weak var appointmentFeesLabel: UILabel!
    @IBOutlet weak var homeVisitCheckBoxView: UIView!
    @IBOutlet weak var homeVisitFeesLabel: UILabel!
    @IBOutlet weak var numberOfVisitsLabel: UILabel!
    @IBOutlet weak var noAppointments: UILabel!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
        if let profileData = self.profileData {
            setWorkClinicInfo(profileData: profileData)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }
    //MARK:-Functions
    private func configuration() {
        
    }
    private func setupUI() {
        
    }
    
    func convertTo12Hours(date24: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm:ss"
        if let date12 = dateFormatter.date(from: date24) {
            dateFormatter.dateFormat = "h:mm a"
            let date22 = dateFormatter.string(from: date12)
            print(date22)
//            print("output \(date24)")
            return date22
        } else {
            // oops, error while converting the string
        }
        return "0"
    }
    
    func setWorkClinicInfo(profileData:ProfileModel) {
        self.profileData = profileData
        clinicCityLabel.text = profileData.doctor?.city?.name ?? ""
        clinicAreaLabel.text = profileData.doctor?.area?.name ?? ""
        clinicBlockNoLabel.text = String(profileData.doctor?.blockNo ?? 0)
        clinicFloorNoLabel.text = String(profileData.doctor?.floorNo ?? 0)
        clinicAddressLabel.text = profileData.doctor?.address
        clinicWorkingDaysLabel.text = profileData.doctor?.workDays?.joined(separator: ",")
//        let toTimeInDateFormat = Date(timeIntervalSinceReferenceDate: TimeInterval(Double(profileData.doctor?.workTimeTo ?? "0") ?? 0))
//        let toTimeInString = toTimeInDateFormat.convertFromTimeStamp()
//        let toHour = "To \(toTimeInString)"
        if let workTimeToDate24 = profileData.doctor?.workTimeTo {
            let toTimeInString = convertTo12Hours(date24: workTimeToDate24)
            let toHour = "To \(toTimeInString)"
            clinicWorkingToHourLabel.text = toHour
        }
        
        if let workTimeFromDate24 = profileData.doctor?.workTimeFrom {
            let fromTimeInString = convertTo12Hours(date24: workTimeFromDate24)
            let fromHour = "From \(fromTimeInString)"
            clinicWorkingFromHourLabel.text = fromHour
        }
        
        patientPerHourLabel.text = "\(profileData.doctor?.patientHour ?? 0)"
        appointmentFeesLabel.text = "\(profileData.doctor?.fees ?? "0")"
        homeVisitCheckBoxView.backgroundColor = profileData.doctor?.homeVisit == true ? .green : .clear
        homeVisitFeesLabel.isHidden = profileData.doctor?.homeVisit == true ? false : true
        homeVisitFeesLabel.text = String(profileData.doctor?.homeVisitFees ?? 0)
        
        numberOfVisitsLabel.text = String(profileData.doctor?.noOfVisits ?? 0)
        noAppointments.text = String(profileData.doctor?.noOfAppointments ?? 0)
    }
    
    //MARK:-Actions
    @IBAction func backButtonIsPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
//MARK:-Extensions
extension WorkClinicInfoVC:StoryboardMakeable {
    static var storyboardName: String = Storyboard.workClinicInfoSB.rawValue
    
    typealias StoryboardMakeableType = WorkClinicInfoVC
    
    
}
