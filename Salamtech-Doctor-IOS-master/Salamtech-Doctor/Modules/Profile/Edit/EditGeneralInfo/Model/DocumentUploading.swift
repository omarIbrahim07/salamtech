//
//  DocumentUploading.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 10/26/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import UIKit
struct DocumentUploading {
    var id:Int? = nil
    var image:UIImage
    var name:String
    var isUploadedBefore:Bool
}
