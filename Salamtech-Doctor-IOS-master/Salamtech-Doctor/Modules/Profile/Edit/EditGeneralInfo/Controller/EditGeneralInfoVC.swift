//
//  EditGeneralInfoVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 10/5/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit
import DropDown

protocol EditGeneralInfoDelegate:class {
    func onEditingCompletion(vc: String)
}

class EditGeneralInfoVC: UIViewController {
    
    //MARK:-Variables
    var document = [DocumentUploading]()
    var selectedImageName = ""
    var doctorProfileImage:UIImage?
    var specialists = [Specialist]()
    var profileData: ProfileModel?
    weak var delegate:EditGeneralInfoDelegate?
    
    //MARK:-Outlets
    @IBOutlet weak var uploadDocumentView: UIView!
    @IBOutlet weak var doctorNameTxtField:UITextField!
    @IBOutlet weak var doctorEmailTxtField:UITextField!
    @IBOutlet weak var doctorPhoneTxtField:UITextField!
    @IBOutlet weak var doctorBirthdateBttn:UIButton!
    @IBOutlet weak var doctorLevelOfSeniorityBttn:UIButton!
    @IBOutlet weak var doctorSpecialityBttn:UIButton!
    @IBOutlet weak var doctorSubSpecialityTxtField:UITextField!
    @IBOutlet weak var doctorCertificateTitleTxtField:UITextField!
    @IBOutlet weak var doctorCertificateBodyTxtField:UITextField!
    @IBOutlet weak var doctorCertificatesTableView:SelfSizedTableView!
    @IBOutlet weak var doctorServiceTxtField:UITextField!
    @IBOutlet weak var doctorServicesTableView:SelfSizedTableView!
    @IBOutlet weak var doctorDocumentsTableView:SelfSizedTableView!
    @IBOutlet weak var maleGenderView:UIView!
    @IBOutlet weak var femaleGenderView:UIView!
    @IBOutlet weak var doctorLevelOfSeniorityView:UIView!
    @IBOutlet weak var doctorSpecializationView:UIView!
    @IBOutlet weak var doctorProfileImageView: UIImageView!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        uploadDocumentView.setDashedBorderLine(boundsColor: UIColor.SALAMTECH.skyBlue!, cornerRadius: 8)
    }
    //MARK:-Functions
    private func configuration() {
        configureDoctorCertificateTableView()
        configureDoctorServicesTableView()
        configureDoctorDocumentsTableView()
        getDoctorSpecialities()
        setData()
    }
    private func setupUI() {
        
    }
    private func configureDoctorCertificateTableView() {
        let nib = UINib(nibName: EducationCell.identifier, bundle: nil)
        doctorCertificatesTableView.register(nib, forCellReuseIdentifier: EducationCell.identifier)
        doctorCertificatesTableView.delegate = self
        doctorCertificatesTableView.dataSource = self
    }
    private func configureDoctorServicesTableView() {
        let nib = UINib(nibName: ServicesCell.identifier, bundle: nil)
        doctorServicesTableView.register(nib, forCellReuseIdentifier: ServicesCell.identifier)
        doctorServicesTableView.delegate = self
        doctorServicesTableView.dataSource = self
    }
    private func configureDoctorDocumentsTableView() {
        let nib = UINib(nibName: DocumentCell.identifier, bundle: nil)
        doctorDocumentsTableView.register(nib, forCellReuseIdentifier: DocumentCell.identifier)
        doctorDocumentsTableView.delegate = self
        doctorDocumentsTableView.dataSource = self
    }
    private func showDatePicker() {
        let datePicker = showDatePicker(mode: .date)
        datePicker.delegate = self
    }
    private func showLevelOfSeniority() {
//        editParameters.seniorityLevel = "Doctor"
        let dropDown = showDropDown(dataSource: ["Doctor","Consultant"], onView: doctorLevelOfSeniorityView)
        dropDown.selectionAction = { [weak self] (index:Int,item:String) in
            self?.doctorLevelOfSeniorityBttn.setTitle(item, for: .normal)
            self?.profileData?.doctor?.seniorityLevel = item
        }
    }
    private func showDoctorSpecialities() {
        let specialities = specialists.map{$0.name}
        let dropDown = showDropDown(dataSource: specialities, onView: doctorSpecializationView)
        dropDown.selectionAction = { [weak self] (index:Int,item:String) in
            self?.doctorSpecialityBttn.setTitle(item, for: .normal)
            self?.profileData?.doctor?.specialist?.id = self?.specialists[index].id ?? 0
        }
    }
    
    private func isValidParameters() -> Bool {
        guard doctorNameTxtField.text != "" else {
            let _ = bottomAlert(title: "Warning", message: "Please insert doctor name")
            return false
        }
        guard doctorEmailTxtField.text != "" else {
            let _ = bottomAlert(title: "Warning", message: "Please insert doctor email")
            return false
        }
        guard doctorPhoneTxtField.text != "" else {
            let _ = bottomAlert(title: "Warning", message: "Please insert doctor phone")
            return false
        }
        guard doctorBirthdateBttn.titleLabel?.text != "dd-MM-yyyy" else {
            let _ = bottomAlert(title: "Warning", message: "Please insert doctor birthdate")
            return false
        }
        guard doctorSubSpecialityTxtField.text != "" else {
            let _ = bottomAlert(title: "Warning", message: "Please insert doctor sub speciality")
            return false
        }
        return true
    }
    
    private func prepareParameters() -> ProfileModel {
        profileData?.doctor?.name = doctorNameTxtField.text
        profileData?.doctor?.email = doctorEmailTxtField.text
        profileData?.doctor?.phone = doctorPhoneTxtField.text
        let birthDate = prepareBirthdateFormat()
        profileData?.doctor?.birthDate = birthDate
        profileData?.doctor?.subSpecialist?[0] = doctorSubSpecialityTxtField.text ?? ""
        return profileData!
    }
    
    private func prepareBirthdateFormat() -> String {
        guard doctorBirthdateBttn.currentTitle != "dd-MM-yyyy" else {
            let _ = bottomAlert(title: "Warning", message: "Please choose doctor birthdate")
            return "dd-MM-yyyy"
        }
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        print(doctorBirthdateBttn.currentTitle)
        let birthdateInDate = df.date(from: doctorBirthdateBttn.currentTitle ?? "")
        df.dateFormat = "yyyy-MM-dd"
        let birthdateInString = df.string(from: birthdateInDate!)
        return birthdateInString
    }
    
    private func getDoctorSpecialities() {
        showLoading()
        let service = AuthService.specialists
        RM.request(service: service) { [weak self] (result:Result<SpecialistResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success(let response):
                self.specialists = response.specialists
            case .failure(let err):
                let _ = self.bottomAlert(title: err.message, message: err.joinedErrors, actionTitle: "Ok")
            }
        }
    }
    
    private func updateGeneralInfo() {
        if isValidParameters() {
            let parameters = prepareParameters()
            showLoading()
            let documentsToUpload = document.filter{$0.isUploadedBefore == false}
            RM.updateGeneralInfo(with: parameters, documents: documentsToUpload, profileImage: doctorProfileImage) { [weak self] (result:Result<TheDefaultResponse, RequestError>) in
                guard let self = self else {return}
                self.hideLoading()
                switch result {
                case .success:
                    print("Success")
                    self.dismiss(animated: true) {
                        self.delegate?.onEditingCompletion(vc: "general info")
                    }
                case .failure(let error):
                    let _ = self.bottomAlert(title: error.message, message: error.joinedErrors, actionTitle: "Ok")
                }
            }
        }
    }
    
    private func setData() {
        doctorNameTxtField.isEnabled = false
        doctorPhoneTxtField.isEnabled = false
        doctorSpecialityBttn.isEnabled = false
        doctorNameTxtField.text = profileData?.doctor?.name
        doctorEmailTxtField.text = profileData?.doctor?.email
        doctorPhoneTxtField.text = profileData?.doctor?.phone
        let birthDate = profileData?.doctor?.birthDate?.convertDateFormat(format: "dd-MM-yyyy") ?? ""
        doctorBirthdateBttn.setTitle(birthDate, for: .normal)
        doctorLevelOfSeniorityBttn.setTitle(profileData?.doctor?.seniorityLevel, for: .normal)
        doctorSpecialityBttn.setTitle(profileData?.doctor?.specialist?.name, for: .normal)
        doctorSubSpecialityTxtField.text = profileData?.doctor?.subSpecialist?[0]
        doctorProfileImageView.addImage(withImage: profileData?.doctor?.image, andPlaceHolder: "AddPicture")
        profileData?.doctor?.gender == "1" ? onTapMaleBttn():onTapFemaleBttn()
        let documentImagesUrl = profileData?.doctor?.documents?.map{$0.link ?? ""} ?? []
        let documentNames = profileData?.doctor?.documents?.map{$0.title ?? ""} ?? []
        
        for (index,imageUrl) in documentImagesUrl.enumerated() {
            downloadImage(downloadableImage: imageUrl) { [weak self] (image) in
                guard let image = image else {return}
                self?.document.append(DocumentUploading(id:self?.profileData?.doctor?.documents?[index].id ?? 0,image: image, name: documentNames[index], isUploadedBefore: true))
                self?.doctorDocumentsTableView.reloadData()
            }
        }
    }
    
    private func onTapMaleBttn(){
        profileData?.doctor?.gender = "1"
        femaleGenderView.backgroundColor = .white
        maleGenderView.backgroundColor = UIColor.SALAMTECH.skyBlue
    }
    
    private func onTapFemaleBttn() {
        profileData?.doctor?.gender = "2"
        femaleGenderView.backgroundColor = UIColor.SALAMTECH.skyBlue
        maleGenderView.backgroundColor = .white
    }
    
    private func deleteDocumentWithID(documentID:Int,completion:@escaping () -> ()) {
        showLoading()
        let parameters = DeleteDocumentParameters(documentID: documentID)
        RM.request(service: ProfileService.deleteDocument(para:parameters )) { [weak self] (result:Result<TheDefaultResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success:
                print("Successfully deleted the document")
                completion()
            case .failure(let err):
                let _ = self.bottomAlert(title: err.message, message: err.joinedErrors, actionTitle: "Ok")
            }
        }
    }
    
    //MARK:- Actions
    @IBAction func onTapBirthdateBttn(_ sender:UIButton) {
        showDatePicker()
    }
    @IBAction func onTapMaleBttn(_ sender:UIButton) {
        onTapMaleBttn()
    }
    @IBAction func onTapFemaleBttn(_ sender:UIButton) {
        onTapFemaleBttn()
    }
    @IBAction func onTapLevelOfSeniorityBttn(_ sender:UIButton) {
        showLevelOfSeniority()
    }
    @IBAction func onTapDoctorSpecialityBttn(_ sender:UIButton) {
        showDoctorSpecialities()
    }
    @IBAction func onTapAddCertificate(_ sender:UIButton) {
        guard doctorCertificateTitleTxtField.text != "" else {
            let _ = bottomAlert(title: "Warning", message: "Please insert certificate title")
            return
        }
        guard doctorCertificateBodyTxtField.text != "" else {
            let _ = bottomAlert(title: "Warning", message: "Please insert certificate body")
            return
        }
        profileData?.doctor?.certifications?.append(Certification(title: doctorCertificateTitleTxtField.text!, body: doctorCertificateBodyTxtField.text!))
        doctorCertificateTitleTxtField.text = nil
        doctorCertificateBodyTxtField.text = nil
        doctorCertificatesTableView.reloadData()
        
    }
    @IBAction func onTapAddServices(_ sender:UIButton) {
        guard doctorServiceTxtField.text != "" else {
            let _ = bottomAlert(title: "Warning", message: "Please insert service")
            return
        }
//        profileData?.doctor?.clinic?.services?.append(doctorServiceTxtField.text!)
        profileData?.doctor?.clinic?.services?.append(doctorServiceTxtField.text!)
        doctorServiceTxtField.text = nil
        doctorServicesTableView.reloadData()
    }
    
    @IBAction func onTapAddDocuments(_ sender:UIButton) {
        ImagePickerManager().pickImage(self) { [weak self] (image, imageName) in
            self?.document.append(DocumentUploading(image: image, name: imageName, isUploadedBefore: false))
            self?.doctorDocumentsTableView.isHidden = false
            self?.selectedImageName = imageName
            self?.doctorDocumentsTableView.reloadData()
        }
    }
    @IBAction func onTapChangeDoctorProfileImage(_ sender: Any) {
        ImagePickerManager().pickImage(self) { [weak self] (image, imageName) in
            self?.doctorProfileImageView.image = image
            self?.doctorProfileImage = image
        }
    }
    @IBAction func onTapSaveEdit(_ sender: Any) {
        updateGeneralInfo()
    }
    
    @IBAction func closeButtonIsPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
//MARK:-Extension
extension EditGeneralInfoVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case doctorCertificatesTableView:
            return profileData?.doctor?.certifications?.count ?? 0
        case doctorServicesTableView:
            return profileData?.doctor?.clinic?.services?.count ?? 0
        case doctorDocumentsTableView:
            return document.count
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case doctorCertificatesTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: EducationCell.identifier, for: indexPath) as! EducationCell
            let item = profileData?.doctor?.certifications?[indexPath.row]
            cell.eduTitleLabel.text = item?.title
            cell.eduBodyLabel.text = item?.body
            cell.clearButton.tag = indexPath.row
            cell.removeEducationDelegate = self
            return cell
        case doctorServicesTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: ServicesCell.identifier, for: indexPath) as! ServicesCell
            let service = profileData?.doctor?.clinic?.services?[indexPath.row]
            cell.serviceTitleLabel.text = service
            cell.clearButton.tag = indexPath.row
            cell.removeServiceDelegate = self
            return cell
        case doctorDocumentsTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: DocumentCell.identifier, for: indexPath) as! DocumentCell
            
            cell.configure(image: document[indexPath.row].image, imageName:document[indexPath.row].name)
            cell.delegate = self
            return cell
        default:
            return UITableViewCell()
        }
    }
}
extension EditGeneralInfoVC:UITableViewDelegate {
    
}
extension EditGeneralInfoVC:DatePickerViewDelegate {
    func onTapDoneBttn(date: Date, datePickerMode: UIDatePicker.Mode) {
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        let dateInString = df.string(from: date)
        doctorBirthdateBttn.setTitle(dateInString, for: .normal)
    }
}
extension EditGeneralInfoVC:DocumentCellDelegate {
    func deleteDocument(cell: DocumentCell) {
        guard let index = doctorDocumentsTableView.indexPath(for: cell) else {return}
        if document[index.row].isUploadedBefore == false {
            document.remove(at: index.row)
            doctorDocumentsTableView.beginUpdates()
            doctorDocumentsTableView.deleteRows(at: [index], with: .left)
            doctorDocumentsTableView.endUpdates()
            if document.isEmpty  {
                UIView.animate(withDuration: 0.3) { [weak self] in
                    self?.doctorDocumentsTableView.isHidden = true
                }
            }else {
                return
            }
        }else {
            guard let id = document[index.row].id else {return}
            print(id)
            deleteDocumentWithID(documentID: id) { [weak self] in
                self?.document.remove(at: index.row)
                self?.doctorDocumentsTableView.beginUpdates()
                self?.doctorDocumentsTableView.deleteRows(at: [index], with: .left)
                self?.doctorDocumentsTableView.endUpdates()
                if self?.document.isEmpty ?? false {
                    UIView.animate(withDuration: 0.3) { [weak self] in
                        self?.doctorDocumentsTableView.isHidden = true
                    }
                }else {
                    return
                }
            }
        }
    }
}
extension EditGeneralInfoVC:StoryboardMakeable {
    static var storyboardName: String = Storyboard.editGeneralInfoSB.rawValue
    typealias StoryboardMakeableType = EditGeneralInfoVC
}

extension EditGeneralInfoVC: RemoveServiceButtonDelegate {
    func removeServiceButtonIsPressed(serviceIndex: Int) {
        profileData?.doctor?.clinic?.services?.remove(at: serviceIndex)
        doctorServicesTableView.reloadData()
    }
}

extension EditGeneralInfoVC: RemoveEducationButtonDelegate {
    func removeEducationButtonIsPressed(educationIndex: Int) {
        profileData?.doctor?.certifications?.remove(at: educationIndex)
        doctorCertificatesTableView.reloadData()
    }
}
