//
//  EditWorkClinicInfoVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 10/5/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class EditWorkClinicInfoVC: UIViewController {
    
    //MARK:-Variables
    var weekDays = ["Saturday","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Clear selected days"]
    var selectedWorkingDays = [String]()
    var fromOperatingHoursBttnIsTapped = false
    var toOperatingHoursBttnIsTapped = false
    var editParameters = EditWorkClinicInfoParameters()
    var profileData:ProfileModel?
    weak var delegate:EditGeneralInfoDelegate?
    
    //MARK:-Outlets
    @IBOutlet weak var workingDaysView: UIView!
    @IBOutlet weak var workingDaysBttn: UIButton!
    @IBOutlet weak var fromWorkingHourBttn: UIButton!
    @IBOutlet weak var toWorkingHourBttn: UIButton!
    @IBOutlet weak var patientsPerHourTxtField: UITextField!
    @IBOutlet weak var appointmentFeesTxtField: UITextField!
    @IBOutlet weak var homeVisitView: UIView!
    @IBOutlet weak var homeVisitFeesView: UIView!
    @IBOutlet weak var homeVisitFeesTxtField: UITextField!
    @IBOutlet weak var selectWorkingDaysButton: UIButton!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
    }
    
    //MARK:-Functions
    private func configuration() {
        
    }
    
    private func setupUI() {
        editParameters.homeVisit = 0
        homeVisitFeesView.isHidden = (editParameters.homeVisit == 0) ? true:false
        setData()
    }
    
    private func setData() {
        self.selectedWorkingDays = []
        patientsPerHourTxtField.text = String(profileData?.doctor?.patientHour ?? 0)
        appointmentFeesTxtField.text = profileData?.doctor?.fees
        if profileData?.doctor?.workDays?.count ?? 0 > 0 {
            self.selectedWorkingDays = (profileData?.doctor?.workDays)!
        }
        selectWorkingDaysButton.setTitle(profileData?.doctor?.workDays?.joined(separator: ","), for: .normal)
        editParameters.address = profileData?.doctor?.address
        editParameters.areaId = profileData?.doctor?.areaID
        editParameters.cityID = profileData?.doctor?.cityID
        editParameters.longitude = profileData?.doctor?.longitude
        editParameters.latitude = profileData?.doctor?.latitude
        
        if profileData?.doctor?.homeVisit == true {
            homeVisitFeesView.isHidden = false
            homeVisitView.backgroundColor = UIColor.SALAMTECH.skyBlue!
            editParameters.homeVisit = 1
            editParameters.homeVisitFees = profileData?.doctor?.homeVisitFees
            if let homeVisitFees = self.profileData?.doctor?.homeVisitFees {
                self.homeVisitFeesTxtField.text = String(homeVisitFees)
            }
        } else {
            homeVisitView.backgroundColor = .white
            homeVisitFeesView.isHidden = true
            editParameters.homeVisit = 0
        }
        
        if let workTimeToDate24 = profileData?.doctor?.workTimeTo {
            let toTimeInString = convertTo12Hours(date24: workTimeToDate24)
            let toHour = toTimeInString
            toWorkingHourBttn.setTitle(toHour, for: .normal)
        }
        
        if let workTimeFromDate24 = profileData?.doctor?.workTimeFrom {
            let fromTimeInString = convertTo12Hours(date24: workTimeFromDate24)
            let fromHour = fromTimeInString
            fromWorkingHourBttn.setTitle(fromHour, for: .normal)
        }

//        doctorNameTxtField.text = profileData?.doctor?.name
//        doctorEmailTxtField.text = profileData?.doctor?.email
//        doctorPhoneTxtField.text = profileData?.doctor?.phone
//        let birthDate = profileData?.doctor?.birthDate?.convertDateFormat(format: "dd-MM-yyyy") ?? ""
//        doctorBirthdateBttn.setTitle(birthDate, for: .normal)
//        doctorLevelOfSeniorityBttn.setTitle(profileData?.doctor?.seniorityLevel, for: .normal)
//        doctorSpecialityBttn.setTitle(profileData?.doctor?.specialist?.name, for: .normal)
//        doctorSubSpecialityTxtField.text = profileData?.doctor?.subSpecialist?[0]
//        doctorProfileImageView.addImage(withImage: profileData?.doctor?.image, andPlaceHolder: "AddPicture")
//        profileData?.doctor?.gender == "1" ? onTapMaleBttn():onTapFemaleBttn()
//        let documentImagesUrl = profileData?.doctor?.documents?.map{$0.link ?? ""} ?? []
//        let documentNames = profileData?.doctor?.documents?.map{$0.title ?? ""} ?? []
//
//        for (index,imageUrl) in documentImagesUrl.enumerated() {
//            downloadImage(downloadableImage: imageUrl) { [weak self] (image) in
//                guard let image = image else {return}
//                self?.document.append(DocumentUploading(id:self?.profileData?.doctor?.documents?[index].id ?? 0,image: image, name: documentNames[index], isUploadedBefore: true))
//                self?.doctorDocumentsTableView.reloadData()
//            }
//        }
    }
    
    func convertTo12Hours(date24: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm:ss"
        if let date12 = dateFormatter.date(from: date24) {
            dateFormatter.dateFormat = "h:mm a"
            let date22 = dateFormatter.string(from: date12)
            print(date22)
//            print("output \(date24)")
            return date22
        } else {
            // oops, error while converting the string
        }
        return "0"
    }

    
    private func clearSelectedWorkingDays() {
        weekDays = ["Saturday","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Clear selected days"]
        selectedWorkingDays.removeAll()
        workingDaysBttn.setTitleColor(UIColor.lightGray, for: .normal)
        workingDaysBttn.setTitle("Select...", for: .normal)
    }
    
    private func setSelectedWorkingDays(item:String,index:Int) {
        let firstThreeLettersOfItem = String(item.prefix(3))
        selectedWorkingDays.append(firstThreeLettersOfItem)
        selectedWorkingDays.removeDuplicates()
        weekDays.remove(at: index)
        let sortedSelectedWorkingDays = sortSelectedWorkingDays()
        workingDaysBttn.setTitle(sortedSelectedWorkingDays.joined(separator: ","), for: .normal)
        workingDaysBttn.setTitleColor(UIColor.SALAMTECH.blue, for: .normal)
    }
    
    private func sortSelectedWorkingDays() -> [String] {
        let sortedSelectedDays = selectedWorkingDays.sorted { (s1, s2) -> Bool in
            let df = DateFormatter()
            df.dateFormat = "E"
            let firstDate = df.date(from: s1)!
            let secondDate = df.date(from: s2)!
            return firstDate < secondDate
        }
        return sortedSelectedDays
    }
    
    private func setupWorkingDaysDropDown() {
        let dropDown = showDropDown(dataSource: weekDays, onView:workingDaysBttn )
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            if item == "Clear selected days" {
                self?.clearSelectedWorkingDays()
            }else {
                self?.setSelectedWorkingDays(item: item,index:index)
            }
        }
    }
    
    private func showTimePickerView() {
        let timePicker = showDatePicker(mode: .time)
        timePicker.delegate = self
    }
    
    private func prepareParameters() -> EditWorkClinicInfoParameters {
        if isValidWorkingHours() {
            editParameters.workTimeFrom = convertTimeFormat(time: fromWorkingHourBttn.currentTitle!)
            editParameters.workTimeTo = convertTimeFormat(time: toWorkingHourBttn.currentTitle!)
        }else {
            let _ = bottomAlert(title: "Warning", message: "Please insert valid working hours")
        }
        editParameters.workDays = self.selectedWorkingDays
        editParameters.patientHour = Int(patientsPerHourTxtField.text ?? "")
        editParameters.fees = Int(appointmentFeesTxtField.text ?? "")
        editParameters.homeVisit = (homeVisitFeesView.isHidden == true) ? 0:1
        editParameters.homeVisitFees = Int(homeVisitFeesTxtField.text ?? "")
        return editParameters
    }
    
    private func isValidParameters() -> Bool {
        guard selectedWorkingDays.count != 0 else {
            let _ = bottomAlert(title: "Warning", message: "Please select working days")
            return false
        }
        guard isValidWorkingHours() == true else {
            let _ = bottomAlert(title: "Warning", message: "Please insert valid working hours")
            return false
        }
        guard patientsPerHourTxtField.text != "" else {
            let _ = bottomAlert(title: "Warning", message: "Please insert number of patients per hour")
            return false
        }
        guard appointmentFeesTxtField.text != ""  else {
            let _ = bottomAlert(title: "Warning", message: "Please insert appointment fees")
            return false
        }
        if homeVisitFeesView.isHidden {
            return true
        }else {
            guard homeVisitFeesTxtField.text != "" else {
                let _ = bottomAlert(title: "Warning", message: "Please insert home visit appointment fees")
                return false
            }
            return true
        }
    }
    
    private func isValidWorkingHours() -> Bool {
        let df = DateFormatter()
        df.dateFormat = "h:mm a"
        guard let fromHourInDate = df.date(from: fromWorkingHourBttn.currentTitle ?? "") else {return false}
        guard let toHourInDate = df.date(from: toWorkingHourBttn.currentTitle ?? "") else {return false}
        if toHourInDate < fromHourInDate || fromWorkingHourBttn.currentTitle == "From 00:00 AM" || toWorkingHourBttn.currentTitle == "To 00:00 PM"{
            return false
        }else {
            return true
        }
    }
    
    private func updateWorkClinicInfo() {
        if isValidParameters() {
           let parameters = prepareParameters()
            showLoading()
            RM.updateWorkClinicInfo(with: parameters) { [weak self] (result:Result<TheDefaultResponse, RequestError>) in
                guard let self = self else {return}
                self.hideLoading()
                switch result {
                case .success:
                    print("Success")
                    self.dismiss(animated: true) {
                        self.delegate?.onEditingCompletion(vc: "clinic info")
                    }
                case .failure(let error):
                    let _ = self.bottomAlert(title: error.message,
                                             message: error.joinedErrors,
                                             actionTitle: "Ok")
                }
            }
        }
    }
    
    //MARK:-Actions
    @IBAction func onTapWorkingDays(_ sender: Any) {
        setupWorkingDaysDropDown()
    }
    @IBAction func onTapFromWorkingHour(_ sender: Any) {
        fromOperatingHoursBttnIsTapped = true
        toOperatingHoursBttnIsTapped = false
        showTimePickerView()
    }
    @IBAction func onTapToWorkingHour(_ sender: Any) {
        fromOperatingHoursBttnIsTapped = false
        toOperatingHoursBttnIsTapped = true
        showTimePickerView()
    }
    @IBAction func onTapHomeVisit(_ sender: Any) {
        if editParameters.homeVisit == 0 {
            homeVisitFeesView.isHidden = false
            homeVisitView.backgroundColor = UIColor.SALAMTECH.skyBlue!
            editParameters.homeVisit = 1
        }else {
            homeVisitView.backgroundColor = .white
            homeVisitFeesView.isHidden = true
            editParameters.homeVisit = 0
        }
    }
    @IBAction func onTapSaveEdit(_ sender: Any) {
        updateWorkClinicInfo()
    }
    
    @IBAction func closeButtonIsPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension EditWorkClinicInfoVC:DatePickerViewDelegate {
    func onTapDoneBttn(date:Date,datePickerMode:UIDatePicker.Mode){
        switch datePickerMode {
        case .date:
            return
        case .time:
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = DateFormatter.Style.short
            dateFormatter.timeZone = TimeZone.current
            let time = dateFormatter.string(from: date)
            if fromOperatingHoursBttnIsTapped {
                editParameters.workTimeFrom = convertTimeFormat(time: time)
                fromWorkingHourBttn.setTitle(time, for: .normal)
            }else {
                editParameters.workTimeTo = convertTimeFormat(time: time)
                toWorkingHourBttn.setTitle(time, for: .normal)
            }
        default:
            return
        }
    }
}
extension EditWorkClinicInfoVC:StoryboardMakeable {
    static var storyboardName: String = Storyboard.editWorkClinicInfoSB.rawValue
    typealias StoryboardMakeableType = EditWorkClinicInfoVC
}
