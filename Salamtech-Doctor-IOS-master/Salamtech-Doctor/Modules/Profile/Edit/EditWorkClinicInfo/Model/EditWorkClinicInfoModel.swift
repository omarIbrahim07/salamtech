//
//  EditWorkClinicInfoModel.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 10/6/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation

struct EditWorkClinicInfoParameters:Codable {
    var workDays = [String]()
    var workTimeFrom,workTimeTo, address:String?
    var fees,patientHour,homeVisit,homeVisitFees, areaId, cityID: Int?
    var longitude, latitude: Double?
    
    enum CodingKeys:String,CodingKey {
        case workDays = "work_days"
        case workTimeFrom = "work_time_from"
        case workTimeTo = "work_time_to"
        case fees
        case patientHour = "patient_hour"
        case homeVisit = "home_visit"
        case homeVisitFees = "home_visit_fees"
        case areaId = "area_id"
        case cityID = "city_id"
        case longitude = "longitude"
        case latitude = "latitude"
    }
}
