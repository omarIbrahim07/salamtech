//
//  ProfileModel.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 10/26/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation

struct ProfileModel: Codable {
    var doctor: Doctor?
    var cities: [City]?
    var specialists: [Specialist]?

    enum CodingKeys: String, CodingKey {
        case doctor, cities, specialists
    }
}
struct UpdateProfileGeneralInfoParameters {
    let name,email,phone:String
}
struct City: Codable {
    let id: Int?
    let name: String
    let areas: [Area]?
}
struct Area: Codable {
    let id: Int?
    let name: String
}
