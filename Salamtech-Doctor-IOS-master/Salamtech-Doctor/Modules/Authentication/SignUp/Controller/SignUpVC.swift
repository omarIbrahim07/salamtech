//
//  SignUpVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/3/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit
import FlagPhoneNumber
class SignUpVC: UIViewController {
    
    //MARK:-Variables
    var dialCode = ""
    var phoneIsValid: Bool?
    var verificationCode: Int?
    
    //MARK:-Outlets
    @IBOutlet weak var flagNumberTxtField: FPNTextField!
    @IBOutlet weak var fullNameTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var mobileTxtField: UITextField!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        delegateCountryCode()
    }
    
    //MARK:-Functions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CodeVerificationSegue" {
            if let vc = segue.destination as? CodeVerificationVC {
                vc.email = emailTxtField.text ?? ""
                vc.password = passwordTxtField.text ?? ""
                vc.phone = mobileTxtField.text ?? ""
                vc.fullName = fullNameTxtField.text ?? ""
                vc.verifiedCode = sender as? NSNumber ?? 0
                vc.checkCode = self.verificationCode ?? 0000
                vc.delegate = self
            }
        }
    }
    
    private func isValidEmail() -> Bool {
        if !(emailTxtField.text?.isValidEmail())! {
            let _ = bottomAlert(title: "Warning", message: "Email should be like this format example@example.com")
            return false
        }else {
            //check on email api
            return true
        }
    }
    
    private func isValidPassword() -> Bool {
        if !(passwordTxtField.text?.isValidPassword())! {
            let _ = bottomAlert(title: "Warning", message: "Password should be at least 8 character long")
            return false
        } else {
            return true
        }
    }
    
    private func isValidName() -> Bool {
        if fullNameTxtField.text?.isEmpty ?? true {
            let _ = bottomAlert(title: "Warning", message: "Please enter your full name")
            return false
        } else {
            return true
        }
    }
    
    private func isValidPhone() -> Bool {
        if mobileTxtField.text?.isEmpty ?? true {
            let _ = bottomAlert(title: "Warning", message: "Please enter your mobile number")
            return false
        }else {
            return true
        }
    }
    
    private func areValidCredentials() -> Bool {
        if isValidPassword() && isValidEmail() && isValidName() && isValidPhone() {
            return true
        }else {
            return false
        }
    }
    
    private func SaveToUserDefaults(response:LoginResponse) {
        guard let doctor = response.doctor else { return }
        UserDefaults.Account.set(value: doctor, key: .user)
        UserDefaults.Account.set(doctor.token ?? "", forKey: .token)
//        UserDefaults.Settings.set(true, forKey: .isUserLoggedIn)
        UserDefaults.Settings.set(true, forKey: .isAppLaunchedBefore)
        
        if doctor.setupProfileStatus == "1" {
            UserDefaults.Account.set("1", forKey: .setupProfileStatus)
            guard let window = self.view.window else {return}
            MainFlowPresenter().navigateToSetupProfileDoctor(window: window)
        }
    }
    
    private func signUp() {
        //        guard let window = self.view.window else {return}
        //        MainFlowPresenter().navigateToSetupProfileDoctor(window: window)
        showLoading()
        let parameters = SignUpParameters(name: fullNameTxtField.text!, email: emailTxtField.text!, phone: "0" + mobileTxtField.text!, password: passwordTxtField.text!, code: String(self.verificationCode!))
        RM.request(service: AuthService.signUp(with: parameters)) { [weak self] (result:Result<LoginResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success(let response):
                print(response)
                self.SaveToUserDefaults(response: response)
                //                guard let window = self.view.window else {return}
                //                MainFlowPresenter().navigateToSetupProfile(window: window)
//                MainFlowPresenter().navigateToSetupProfileDoctor(window: window)
            case .failure(let err):
                let _ = self.bottomAlert(title: err.message,
                                         message: err.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    
    private func checkOnEmailAndPhone() {
        //        self.performSegue(withIdentifier: "CodeVerificationSegue", sender: "4444")
        
        
        guard let phone: String = mobileTxtField.text, phone.count == 10 else {
            let _ = bottomAlert(title: "Warning", message: "Please enter valid phone number")
            return
        }
        
        if areValidCredentials() {
            let parameters = CheckOnMailAndPhoneModel(phone: "0" + mobileTxtField.text!, email: emailTxtField.text!)
            showLoading()
            RM.request(service: AuthService.checkOnMailAndPhone(with: parameters), completion: {[weak self] (result:Result<VerificationResponse, RequestError>) in
                guard let self = self else {return}
                self.hideLoading()
                switch result {
                case .success(let response):
                    print(response.code)
                    self.verificationCode = response.code
                    
                    self.performSegue(withIdentifier: "CodeVerificationSegue", sender: response.code)
                case .failure( let err):
                    let _ = self.bottomAlert(title: err.message,
                                             message: err.joinedErrors,
                                             actionTitle: "Ok")
                }
            })
        }
    }
    
    //MARK:-Actions
    @IBAction func onTapNext(_ sender: Any) {
        checkOnEmailAndPhone()
    }
}
//MARK:-Extensions
extension SignUpVC:StoryboardMakeable{
    static var storyboardName: String = Storyboard.signUpSB.rawValue
    typealias StoryboardMakeableType = SignUpVC
}
extension SignUpVC:CodeVerificationProtocol {
    func didEndVerification() {
        dismiss(animated: true) {
            self.signUp()
        }
    }
    
    
}
extension SignUpVC: FPNTextFieldDelegate {
    
    func delegateCountryCode() {
        mobileTxtField.delegate = self
        flagNumberTxtField.inputView = FPNCountryPicker()
        flagNumberTxtField.setFlag(countryCode: .EG)
        flagNumberTxtField.hasPhoneNumberExample = false
        flagNumberTxtField.placeholder = ""
        flagNumberTxtField.textColor = UIColor.SALAMTECH.blue
        flagNumberTxtField.delegate = self
    }
    
    func fpnDisplayCountryList() {
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code)
        self.dialCode = dialCode
    }
            
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            // Do something...
            self.phoneIsValid = true
        } else {
            // Do something...
            self.phoneIsValid = false
        }
    }

        
//        if isValid {
//            // Do something...
//            //            textField.getFormattedPhoneNumber(format: .E164)           // Output "+33600000001"
//            //            textField.getFormattedPhoneNumber(format: .International)  // Output "+33 6 00 00 00 01"
//            //            textField.getFormattedPhoneNumber(format: .National)       // Output "06 00 00 00 01"
//            //            textField.getFormattedPhoneNumber(format: .RFC3966)        // Output "tel:+33-6-00-00-00-01"
//            //            textField.getRawPhoneNumber()                               // Output "600000001"
//        } else {
//            // Do something...
//        }
//    }
    
    
}

extension SignUpVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For numers
        if textField == mobileTxtField {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
}
