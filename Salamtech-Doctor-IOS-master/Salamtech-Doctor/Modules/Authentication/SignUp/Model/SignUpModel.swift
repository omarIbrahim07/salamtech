//
//  SignUpModel.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/6/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
struct SignUpParameters:Codable {
    let name:String
    let email:String
    let phone:String
    let password:String
    let code: String
}

struct SignUpResponse:Codable {
//    let doctor:Doctor?
    let doctor:SignUpDoctorData?
}
struct SignUpDoctorData:Codable {
    let name:String?
    let email:String?
    let phone:String?
    let id:Int?
    let token:String?
}
struct CheckOnMailAndPhoneModel:Codable {
    let phone:String
    let email:String
}
