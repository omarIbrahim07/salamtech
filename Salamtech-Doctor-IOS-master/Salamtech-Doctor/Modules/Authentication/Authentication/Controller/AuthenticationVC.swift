//
//  AuthenticationVC.swift
//  salamtech
//
//  Created by hesham ghalaab on 5/17/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import UIKit

class AuthenticationVC: UIViewController {

    // MARK: Outlets
    
    // MARK: Properties
    
    // MARK: Override Functions
    // viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configuration()
        setupUI()
        print(navigationController)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        isNavigationBarShadowHidden(true)
    }
    
    // MARK: Methods
    // configuration: to configure any protocols
    private func configuration(){
        
    }
    
    // setupUI: to setup data or make a custom design
    private func setupUI(){
        
    }
    
    // MARK: Actions

    
}

extension AuthenticationVC: StoryboardMakeable{
    static var storyboardName: String = Storyboard.authenticationSB.rawValue
    typealias StoryboardMakeableType = AuthenticationVC
}
