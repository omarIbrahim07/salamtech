//
//  AuthenticationNC.swift
//  salamtech
//
//  Created by hesham ghalaab on 5/17/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import UIKit

class AuthenticationNC: UINavigationController {

    // MARK: Outlets
    
    // MARK: Properties
    
    // MARK: Override Functions
    // viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configuration()
        setupUI()
    }
    
    // MARK: Methods
    // configuration: to configure any protocols
    private func configuration(){
        
    }
    
    // setupUI: to setup data or make a custom design
    private func setupUI(){
        let image = UIImage(named: "back_arrow")
        self.navigationBar.backIndicatorImage = image
        self.navigationBar.backIndicatorTransitionMaskImage = image
        self.navigationBar.tintColor = UIColor.SALAMTECH.skyBlue
    }
    
    // MARK: Actions

}

extension AuthenticationNC: StoryboardMakeable{
    static var storyboardName: String = Storyboard.authenticationSB.rawValue
    typealias StoryboardMakeableType = AuthenticationNC
}
