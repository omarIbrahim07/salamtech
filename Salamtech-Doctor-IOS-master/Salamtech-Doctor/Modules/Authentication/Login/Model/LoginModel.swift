//
//  LoginModel.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/3/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
struct LoginParameters:Codable {
    let phone:String
    let password:String
}

struct LoginResponse:Codable {
    let doctor:Doctor?
}

struct Doctor:Codable {
    var id, patientHour, homeVisitFees, rate, cityID, areaID, floorNo, blockNo: Int?
    var name, email, phone,image,birthDate,seniorityLevel,address,token,workTimeFrom, workTimeTo, setupProfileStatus, gender, fees, streetName: String?
    var subSpecialist: [String]?
    var latitude,longitude: Double?
    var workDays: [String]?
    var homeVisit: Bool?
    var services: [String]?
    var profileFinish: Bool?
    var clinic:Clinic?
    var specialist:Specialist?
    var certifications:[Certification]?
    var documents:[Document]?
    var city:City?
    var area:Area?
    var noOfVisits: Int?
    var noOfAppointments:Int?
    
//    "sub_specialist": [
//        "Dentistry 22"
//    ],
//    "seniority_level": "Consulting Doctor",
//    "floor_no": null,
//    "block_no": null,
//    "address": null,
//    "latitude": null,
//    "longitude": null,
//    "work_days": null,
//    "work_time_from": null,
//    "work_time_to": null,
//    "fees": 0,
//    "patient_hour": 1,
//    "home_visit": false,
//    "home_visit_fees": 0,
//    "services": null,
//    "rate": 0,
//    "views": 0,
//    "profile_finish": true,
//    "status": false,
//    "setup_profile_status": "2",
//    "specialist_id": 3,
//    "city_id": null,
//    "area_id": null,
//    "clinic_id": null,
//    "clinic_branch_id": null,
//    "created_at": "2021-03-03",
//    "updated_at": "2021-03-03",
//    "token": "ltjLhStM33zxt4qF8MgDocRvEgRprfPmeh2yTiyon6fR1vC5cv4SfkUWygJ4Yz59Rn6Z48ZeHRRXF37KDbg4me9wwinE1Iclxq2J"


    enum CodingKeys: String, CodingKey {
        case id, name, email, phone, image,fees,address, latitude, longitude,gender,token,services, rate,city,area,specialist,certifications,documents
        case setupProfileStatus = "setup_profile_status"
        case birthDate = "birth_date"
        case subSpecialist = "sub_specialist"
        case seniorityLevel = "seniority_level"
        case floorNo = "floor_no"
        case blockNo = "block_no"
        case workDays = "work_days"
        case workTimeFrom = "work_time_from"
        case workTimeTo = "work_time_to"
        case patientHour = "patient_hour"
        case homeVisit = "home_visit"
        case homeVisitFees = "home_visit_fees"
        case profileFinish = "profile_finish"
        case noOfAppointments = "no_appointments"
        case noOfVisits = "no_visits"
        case cityID = "city_id"
        case areaID = "area_id"
        case streetName = "street_name"
        case clinic = "doctor_clinic"
    }
}

struct Clinic:Codable {
    let id: Int?
    let name,email,phone,image,branche: String?
    let branchesNo: Int?
    var services: [String]?
    let amenities: [String]?
    let websiteURL: String?
//    let profileFinish: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id, name, email, phone, image
        case branchesNo = "branches_no"
        case services, amenities
        case websiteURL = "website_url"
//        case profileFinish = "profile_finish"
        case branche
    }
}
