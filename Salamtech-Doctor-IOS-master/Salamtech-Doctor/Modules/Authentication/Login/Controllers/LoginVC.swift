//
//  LoginVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/17/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import UIKit
import MOLH
class LoginVC: UIViewController {
    //MARK:-Variables
    var currentLang = "en"
    //MARK:-Outlets
    @IBOutlet weak var phoneTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var emailImageView: UIImageView!
    @IBOutlet weak var passwordImageView: UIImageView!
    @IBOutlet weak var rememberMeImageView: UIImageView!
    @IBOutlet weak var rememberMeLabel: UILabel!
    @IBOutlet weak var forgetYourPasswordBttn: UIButton!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    //MARK:-Methods
    private func configuration() {
        configureTxtFields()
    }
    private func setupUI(){
        phoneTxtField.placeholder = "Mobile Number".localized
        passwordTxtField.placeholder = "Login_Password_Placeholder".localized
//        emailTxtField.text = "yasmien@gmail.com"
//        passwordTxtField.text = "123456789"
        rememberMeLabel.text = "Remember_Me".localized
        forgetYourPasswordBttn.setTitle("Forget_Your_Password".localized, for: .normal)
    }
    private func configureTxtFields() {
        phoneTxtField.addTarget(self, action: #selector(emailHasChanged), for: .editingChanged)
        passwordTxtField.addTarget(self, action: #selector(passwordHasChanged), for: .editingChanged)
    }
    
//    private func isValidEmail() ->Bool {
//        if !(emailTxtField.text?.isValidEmail())! {
//            let _ = bottomAlert(title: "Warning", message: "Email should be like this format example@example.com")
//            return false
//        }else {
//            return true
//        }
//    }
    
    private func isValidPhone() ->Bool {
        guard let phone: String = phoneTxtField.text, phone.count == 11 else {
            let _ = bottomAlert(title: "Warning", message: "Please enter valid phone number")
            return false
        }
        return true
    }
    
    private func isValidPassword() -> Bool {
        if !(passwordTxtField.text?.isValidPassword())! {
            let _ = bottomAlert(title: "Warning", message: "Password should be at least 8 character long")
            return false
        }else {
            return true
        }
    }
    private func areValidCredentials() -> Bool {
        if isValidPassword() && isValidPhone() {
            return true
        }else {
            return false
        }
    }
    private func restartApplication(hideNavBar:Bool) {
        let viewController = LoginVC.make()
        let nav = UINavigationController.init(rootViewController: viewController)
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        if hideNavBar {
            nav.navigationBar.isHidden = true
        }else {
            nav.navigationBar.isHidden = false
        }
        window?.rootViewController = nav
    }
    @objc private func emailHasChanged() {
//        if emailTxtField.text == "" {
//            emailImageView.image = UIImage(named: "email")
//        }else {
//            emailImageView.image = UIImage(named: "emailWritten")
//        }
    }
    @objc private func passwordHasChanged() {
        if passwordTxtField.text == "" {
            passwordImageView.image = UIImage(named: "password")
        }else {
            passwordImageView.image = UIImage(named: "passwordWritten")
        }
        
    }
    //MARK:-Actions
    @IBAction func onTapNext(_ sender: Any) {
        guard let window = view.window else {return}
        if areValidCredentials() {
            let parameters = LoginParameters(phone: phoneTxtField.text!, password: passwordTxtField.text!)
            showLoading()
            RM.request(service: AuthService.login(with: parameters)) { [weak self] (result:Result<LoginResponse, RequestError>) in
                guard let self = self else {return}
                self.hideLoading()
                switch result {
                case .success(let response):
                    print(response)
                    guard let doctor = response.doctor else {return}
                    UserDefaults.Account.set(value: response.doctor, key: .user)
                    UserDefaults.Account.set(doctor.token ?? "", forKey: .token)
                    UserDefaults.Settings.set(true, forKey: .isUserLoggedIn)
                    MainFlowPresenter().navigateToAccessability(window: window)
                case .failure(let err):
                    let _ = self.bottomAlert(title: err.message,
                                             message: err.joinedErrors,
                                             actionTitle: "Ok")
                }
            }
        }else {
            return
        }
    }
    @IBAction func onTapForgetPassword(_ sender: Any) {
        
    }
}
extension LoginVC:StoryboardMakeable{
    static var storyboardName: String = Storyboard.loginSB.rawValue
    typealias StoryboardMakeableType = LoginVC
}
