//
//  LoginNC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/20/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import UIKit

class LoginNC: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
    }
    //MARK:-Methods
    private func configuration() {
        
    }
    private func setupUI() {
        self.navigationBar.isHidden = true
    }
    
    
}
extension LoginNC:StoryboardMakeable{
    static var storyboardName: String = Storyboard.loginSB.rawValue
    typealias StoryboardMakeableType = LoginNC
    
    
}
