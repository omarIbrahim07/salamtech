//
//  ViewController.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/17/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        perform(#selector(checkWhereToGo), with: nil, afterDelay: 1)
        UserDefaults.Settings.set(true, forKey: UserDefaults.Settings.Key.isEnglish)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
     
    }
    
    @objc func checkWhereToGo() {
        guard let window = self.view.window else { return }
        let isUserLoggedIn = UserDefaults.Settings.bool(forKey: .isUserLoggedIn)
        switch isUserLoggedIn{
        case true:
            MainFlowPresenter().navigateToHomeNC(window: window)
        case false:
            checkIfAppLauncedBefore()
        }
    }
    private func checkIfAppLauncedBefore() {
        guard let window = self.view.window else { return }
        
        print(UserDefaults.Settings.bool(forKey: .isAppLaunchedBefore))
        print(UserDefaults.Account.string(forKey: .setupProfileStatus))
        
        if UserDefaults.Settings.bool(forKey: .isAppLaunchedBefore) && (UserDefaults.Account.string(forKey: .setupProfileStatus) == "1") {
            MainFlowPresenter().navigateToSetupProfileDoctor(window: window)
//            MainFlowPresenter().navigateToSetupProfile(window: window)
        } else if UserDefaults.Settings.bool(forKey: .isAppLaunchedBefore) && (UserDefaults.Account.string(forKey: .setupProfileStatus) == "2") {
            MainFlowPresenter().navigateToSetupProfileClinic(window: window)
        } else if UserDefaults.Settings.bool(forKey: .isAppLaunchedBefore) && (UserDefaults.Account.string(forKey: .setupProfileStatus) == "3") {
            MainFlowPresenter().navigateToSetupDrProfileUploadDocumentsVC(window: window)
        } else if UserDefaults.Settings.bool(forKey: .isAppLaunchedBefore) && (UserDefaults.Account.string(forKey: .setupProfileStatus) == nil) {
            MainFlowPresenter().navigateToAuthenticationNC(window: window)
        } else if UserDefaults.Settings.bool(forKey: .isAppLaunchedBefore) == false {
            MainFlowPresenter().navigateToOnBoardingNC(window: window)
        }

//        if UserDefaults.Settings.bool(forKey: .isAppLaunchedBefore) {
//            MainFlowPresenter().navigateToAuthenticationNC(window: window)
//        } else {
//            MainFlowPresenter().navigateToOnBoardingNC(window: window)
//        }
    }
}

//MARK:-Extension
extension SplashVC:StoryboardMakeable {
    static var storyboardName: String = Storyboard.splashSB.rawValue
    typealias StoryboardMakeableType = SplashVC
}
