//
//  AppointmentDetailsVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/18/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import UIKit
protocol IAppointmentDetails:class {
    func goToMessages(userID:Int,consultationID:Int)
    func appointmentIsCanceledByDoctor(indexPath:IndexPath)
}
class AppointmentDetailsVC:UIViewController {
    //MARK:-Variables
    weak var delegate:IAppointmentDetails?
    var patientAppointmentDetails:Appointments?
    var selectedPatientIndexPath:IndexPath!
    var patientID:Int?
    var consultationID:Int?
    var isAppointmentCanceled = false
    //MARK:-Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var patientImageView: UIImageView!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var patientCodeLabel: UILabel!
    @IBOutlet weak var doctorSpecialityImageView: UIImageView!
    @IBOutlet weak var patientAppointmentTime: UILabel!
    @IBOutlet weak var patientAppointmentDate: UILabel!
    @IBOutlet weak var patientAddress: UILabel!
    @IBOutlet weak var cancelAppointmentBttn: UIButton!
    
    //MARK:-ViewLifeCycle and overridden functions
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        containerView.roundCorners(corners: [.topLeft,.topRight], radius: 20)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PatientProfileSegue"{
            if let vc = segue.destination as? PatientProfileVC{
                if let sender = sender as? PatientEMRResponse {
                    vc.delegate = self
                    vc.profileData = sender
                    setPatientProfileView(viewController: vc, data: sender)
                }
            }
        }
    }
    //MARK:-Methods
    private func configuration() {
        setAppointmentData()
    }
    private func setupUI() {
        if isAppointmentCanceled {
            cancelAppointmentBttn.isEnabled = false
            cancelAppointmentBttn.setTitle("Appointment is cancelled", for: .normal)
        }else {
            cancelAppointmentBttn.isEnabled = true
            cancelAppointmentBttn.setTitle("Cancel appointment", for: .normal)
        }
    }
    private func setPatientProfileView(viewController:PatientProfileVC,data:PatientEMRResponse) {
        checkIfReportExist(viewController: viewController, data: data)
        checkIfDocumentsExist(viewController: viewController, data: data)
        checkIfMedicinesExist(viewController: viewController, data: data)
    }
    private func checkIfReportExist(viewController:PatientProfileVC,data:PatientEMRResponse) {
        if data.emr?.report == nil {
            viewController.reportState = .unfilled
        }else{
            viewController.reportState = .filled
        }
    }
    private func checkIfDocumentsExist(viewController:PatientProfileVC,data:PatientEMRResponse) {
        if data.emr?.documents.count == 0 {
            viewController.documentState = .unfilled
        }else {
            viewController.documentState = .filled
        }
    }
    private func checkIfMedicinesExist(viewController:PatientProfileVC,data:PatientEMRResponse) {
        if data.emr?.medecines.count == 0 {
            viewController.medicineState = .unfilled
        }else {
            viewController.medicineState = .filled
        }
    }
    private func setDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateInDateFormat = dateFormatter.date(from:patientAppointmentDetails?.date ?? "")
        dateFormatter.dateFormat = "d EEEE, MMMM yyyy"
        let date = dateFormatter.string(from: dateInDateFormat!)
        return date
    }
    private func setAppointmentData() {
        patientImageView.addImage(withImage:patientAppointmentDetails?.patient?.image , andPlaceHolder: "profile")
        patientNameLabel.text = patientAppointmentDetails?.patient?.name
        patientCodeLabel.text = patientAppointmentDetails?.patient?.code
//        let patientAppointmentTimeInDate = Date(timeIntervalSinceReferenceDate: TimeInterval(patientAppointmentDetails?.time ?? 0))
//        let patientAppointmentTimeInString = patientAppointmentTimeInDate.convertFromTimeStamp()
//        patientAppointmentTime.text = patientAppointmentTimeInString
        
        if let appointmentTimeFromDate24 = patientAppointmentDetails?.doctor?.workTimeFrom, let appointmentTimeToDate24 = patientAppointmentDetails?.doctor?.workTimeTo {
            let appointmentFromTimeInString = convertTo12Hours(date24: appointmentTimeFromDate24)
            let appointmentToTimeInString = convertTo12Hours(date24: appointmentTimeToDate24)
            let appointmentTime = appointmentFromTimeInString + " - " + appointmentToTimeInString
            patientAppointmentTime.text = appointmentTime
        }

        patientAppointmentDate.text = setDate()
        if patientAppointmentDetails?.type == 1 {
            let blockNo = patientAppointmentDetails?.address?.blockNo ?? 0
            let address = patientAppointmentDetails?.address?.address ?? ""
//            let floorNo = patientAppointmentDetails?.address?.floorNo ?? 0
//            patientAddress.text = "\(blockNo),\(address),floorNo:\(floorNo)"
            patientAddress.text = "\(blockNo),\(address)"
        }else {
            
        }
    }
    
    func convertTo12Hours(date24: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm:ss"
        if let date12 = dateFormatter.date(from: date24) {
            dateFormatter.dateFormat = "h:mm a"
            let date22 = dateFormatter.string(from: date12)
            print(date22)
//            print("output \(date24)")
            return date22
        } else {
            // oops, error while converting the string
        }
        return "0"
    }

    private func cancelAppointment() {
        showLoading()
        guard let appointmentID = patientAppointmentDetails?.id else {
            let _ = bottomAlert(title: "Alert", message: "Something went wrong,please try again later")
            print("there is no Appointment ID")
            return
        }
        let parameters = CancelAppointmentParameter(AppointmentID: appointmentID)
        RM.request(service: HomeService.cancelAppointment(with: parameters)) {[weak self] (result:Result<TheDefaultResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success:
                self.dismiss(animated: true) {
                    let _ = self.bottomAlert(title: "Alert", message: "Appointment is cancelled")
                    self.delegate?.appointmentIsCanceledByDoctor(indexPath: self.selectedPatientIndexPath)
                }
            case .failure(let err):
                let _ = self.bottomAlert(title: err.message,
                                         message: err.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    private func createConsultationRoom() {
        showLoading()
        guard let patientID = patientAppointmentDetails?.patient?.id else {
            let _ = bottomAlert(title: "Alert", message: "Something went wrong,please try again later")
            print("there is no patient ID")
            return
        }
        self.patientID = patientID
        print(patientID)
        let parameters = ConsultationParameters(userID: patientID, limit: 100)
        RM.request(service: HomeService.createChatRoom(with: parameters)) {[weak self] (result:Result<ConsultationsResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success(let response):
                self.dismiss(animated: true) {
                    for item in response.consulations.data {
                        if item.user?.id == parameters.userID {
                            self.consultationID = item.id
                        }
                    }
                    self.delegate?.goToMessages(userID: parameters.userID, consultationID: self.consultationID ?? 0)
                }
            case .failure(let err):
                let _ = self.bottomAlert(title: err.message,
                                         message: err.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    private func getEMRData() {
        showLoading()
        guard let patientID = patientAppointmentDetails?.patient?.id else {
            let _ = bottomAlert(title: "Alert", message: "Something went wrong,please try again later")
            print("there is no patient ID")
            return
        }
        let parameters = EMRParameters(userID: patientID)
        RM.request(service: HomeService.getPatientEMR(with: parameters)) { [weak self] (result:Result<PatientEMRResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success(let response):
                print(response)
                self.performSegue(withIdentifier: "PatientProfileSegue", sender: response)
            case .failure(let err):
                let _ = self.bottomAlert(title: err.message,
                                         message: err.joinedErrors,
                                         actionTitle: "Ok")
            }
        }
    }
    //MARK:-Actions
    @IBAction func onTapPatientProfile(_ sender: Any) {
        getEMRData()
    }
    @IBAction func onTapMessage(_ sender: Any) {
        self.createConsultationRoom()
    }
    @IBAction func onTapCancelAppointment(_ sender: Any) {
        cancelAppointment()
    }
    
    @IBAction func backButtonIsPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
extension AppointmentDetailsVC:IPatientProfile {
    func messageIsTapped(patientID:Int) {
        dismiss(animated: true) {
            self.delegate?.goToMessages(userID: patientID, consultationID: self.consultationID ?? 0)
        }
    }
    
    
}
