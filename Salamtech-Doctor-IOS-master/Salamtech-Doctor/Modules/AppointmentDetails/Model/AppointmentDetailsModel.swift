//
//  AppointmentDetailsModel.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/14/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
struct CancelAppointmentParameter:Codable {
    let AppointmentID:Int
    
    enum CodingKeys:String,CodingKey {
        case AppointmentID = "appointment_id"
    }
}

struct EMRParameters:Codable {
    let userID:Int
    
    enum CodingKeys:String,CodingKey {
        case userID = "user_id"
    }
}
struct PatientEMRResponse: Codable {
    let emr: Emr?
}

// MARK: - Emr
struct Emr: Codable {
    var id,noAppointments: Int?
    var report: String?
    var patient: Patient?
    var documents: [Document]
    var medecines: [Medecine]

    enum CodingKeys: String, CodingKey {
        case id, report
        case noAppointments = "no_appointments"
        case documents, medecines
        case patient = "user"
    }
}

// MARK: - Document
struct Document: Codable {
    var id: Int?
    var title,link,size: String?
}

// MARK: - Medecine
struct Medecine: Codable {
    var id: Int?
    var title, body: String?
}

// MARK: - User
struct User: Codable {
    let id: Int?
    let name, code: String?
    let image: String?
}
