//
//  AccessabilityVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/17/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class EnableLocationVC:UIViewController {
    //MARK:-Variables
    let locationManager = CLLocationManager()
    //MARK:-Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var accessabilityButton: UIButton!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
        print(navigationController)
    }
    //MARK:-Methods
    private func configuration() {
        
    }
    private func setupUI() {
        
    }
    private func saveLocationToUserDefaults(latitude:Double,longitude:Double) {
        UserDefaults.Account.set(latitude, forKey: .locationLat)
        UserDefaults.Account.set(longitude, forKey: .locationLong)
    }
    private func enableLocationServices() {
           locationManager.requestWhenInUseAuthorization();
           if CLLocationManager.locationServicesEnabled() {
               locationManager.delegate = self
               locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
               locationManager.startUpdatingLocation()
           }
           else{
               print("Location service disabled");
           }
       }
    private func navigateToEnablePushNotifications() {
        guard let window = view.window else {return}
        MainFlowPresenter().navigateToEnableNotifications(window: window)
    }
    //MARK:-Actions
    @IBAction func onTapEnableButton(_ sender: Any) {
        print("Hello")
        enableLocationServices()
    }
    @IBAction func onTapDontAllow(_ sender: Any) {
        guard let window = view.window else {return}
        MainFlowPresenter().navigateToEnableNotifications(window: window)
    }
    
}
extension EnableLocationVC:StoryboardMakeable{
    static var storyboardName: String = Storyboard.accessabilitySB.rawValue
    typealias StoryboardMakeableType = EnableLocationVC
}
extension EnableLocationVC:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = manager.location?.coordinate
        let long = Double(location?.longitude ?? CLLocationDegrees())
        let lat = Double(location?.latitude ?? CLLocationDegrees())
        saveLocationToUserDefaults(latitude: long, longitude: lat)
        print(long,lat)
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            navigateToEnablePushNotifications()
        }else {
            //warn user
            navigateToEnablePushNotifications()
        }
    }
}
