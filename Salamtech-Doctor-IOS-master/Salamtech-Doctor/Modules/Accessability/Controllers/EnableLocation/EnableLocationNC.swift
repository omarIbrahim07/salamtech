//
//  AccessabilityNC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/11/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class EnableLocationNC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
extension EnableLocationNC:StoryboardMakeable{
    static var storyboardName: String = Storyboard.accessabilitySB.rawValue
    typealias StoryboardMakeableType = EnableLocationNC
}
