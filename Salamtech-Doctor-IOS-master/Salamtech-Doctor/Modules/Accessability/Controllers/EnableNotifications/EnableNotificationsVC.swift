//
//  EnableNotificationsVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/11/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit
import UserNotifications
class EnableNotificationsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    private func configuration() {
        
    }
    private func setupUI() {
        
    }
    private func registerForRemoteNotification() {
        guard let window = view.window else {return}
        let center  = UNUserNotificationCenter.current()
        center.delegate = self as? UNUserNotificationCenterDelegate
        center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
            if error == nil{
                DispatchQueue.main.async {
                    MainFlowPresenter().navigateToHomeNC(window: window)
                }
            }else {
                
            }
        }
    }
    @IBAction func onTapEnableBttn(_ sender: Any) {
        registerForRemoteNotification()
    }
    
    
}
extension EnableNotificationsVC:StoryboardMakeable{
    static var storyboardName: String = Storyboard.accessabilitySB.rawValue
    typealias StoryboardMakeableType = EnableNotificationsVC
}
