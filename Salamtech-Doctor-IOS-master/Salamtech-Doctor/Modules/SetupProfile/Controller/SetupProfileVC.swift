//
//  SetupProfileVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/6/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import UIKit
import DropDown

class SetupProfileVC: UIViewController {
    //MARK:-Variables
    var certifications = [Certification]()
    var services = [String]()
    var selectedWorkingDays = [String]()
    var weekDays = ["Saturday","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Clear selected days"]
    
    var isHomeVisitEnabled: Bool = false
    var fromOperatingHoursBttnIsTapped = false
    var toOperatingHoursBttnIsTapped = false
    var seniorityLevel:String?
    var fromTime:String?
    var toTime:String?
    var documentsImages = [UIImage]()
    
    var specialists = [Specialist]()
    var selectedSpecialist: Specialist?
    
    var selectedGender = Gender.none
    enum Gender: Int {
        case male = 1
        case female = 2
        case none = 0
    }
    var selectedProfileImage: UIImage?
    
    
    //MARK:-Outlets
    @IBOutlet weak var birthdateLabel: UILabel!
    @IBOutlet weak var femaleBttn: UIButton!
    @IBOutlet weak var maleBttn: UIButton!
    @IBOutlet weak var appointmentFeesTxtField: UITextField!
    @IBOutlet weak var homeVisitView: UIView!
    @IBOutlet weak var uploadCertifcatesView: UIView!
    @IBOutlet weak var pickSpecializationBttn: UIButton!
    @IBOutlet weak var levelOfSeniorityBttn: UIButton!
    @IBOutlet weak var workingDaysBttn: UIButton!
    @IBOutlet weak var educationTableView: SelfSizedTableView!
    @IBOutlet weak var servicesTableView: SelfSizedTableView!
    @IBOutlet weak var profilePhotoImageView: UIImageView!
    @IBOutlet weak var selectFromOperatingHoursBttn: UIButton!
    @IBOutlet weak var selectToOperatingHoursBttn: UIButton!
    @IBOutlet weak var documentsTableView: UITableView!
    
    @IBOutlet weak var subSpecialisationField: UITextField!
    @IBOutlet weak var homeVisitFeesField: UITextField!
    @IBOutlet weak var numberOfPatientField: UITextField!
    @IBOutlet weak var certificationTitleField: UITextField!
    @IBOutlet weak var certificationBodyField: UITextField!
    @IBOutlet weak var serviceField: UITextField!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setupDashedBorderLine()
    }
    //MARK:-Methods
    private func configuration() {
        configureServicesTableView()
        configureEducationTableView()
        configureDocumentsTableView()
        configureTxtFields()
    }
    private func setupUI() {
        handleGenderUI(with: selectedGender)
    }
    
    private func setupDashedBorderLine() {
        uploadCertifcatesView.setDashedBorderLine(boundsColor: UIColor.SALAMTECH.skyBlue!, cornerRadius: 5)
    }
    
    private func configureTxtFields() {
        
    }
    private func configureDocumentsTableView() {
        let nib = UINib(nibName: DocumentCell.identifier, bundle: nil)
        documentsTableView.register(nib, forCellReuseIdentifier: DocumentCell.identifier)
        documentsTableView.delegate = self
        documentsTableView.dataSource = self
    }
    private func configureEducationTableView() {
        let nib = UINib(nibName: EducationCell.identifier, bundle: nil)
        educationTableView.register(nib, forCellReuseIdentifier: EducationCell.identifier)
        educationTableView.delegate = self
        educationTableView.dataSource = self
    }
    private func configureServicesTableView() {
        let nib = UINib(nibName: ServicesCell.identifier, bundle: nil)
        servicesTableView.register(nib, forCellReuseIdentifier: ServicesCell.identifier)
        servicesTableView.delegate = self
        servicesTableView.dataSource = self
    }
    private func setupPickSpecializationDropDown() {
        let dataSource = specialists.map { $0.name }
        let dropDown = showDropDown(dataSource: dataSource, onView: pickSpecializationBttn)
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            self?.selectedSpecialist = self?.specialists[index]
            self?.pickSpecializationBttn.setTitle(item, for: .normal)
        }
    }
    
    private func setupLevelOfSeniorityDropDown() {
        let dropDown = showDropDown(dataSource: ["Doctor","Consulting Doctor","Professional Doctor"], onView:levelOfSeniorityBttn )
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            self?.levelOfSeniorityBttn.setTitle(item, for: .normal)
            self?.seniorityLevel = item
        }
    }
    private func clearSelectedWorkingDays() {
        weekDays = ["Saturday","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Clear selected days"]
        selectedWorkingDays.removeAll()
        workingDaysBttn.setTitleColor(UIColor.lightGray, for: .normal)
        workingDaysBttn.setTitle("Select...", for: .normal)
    }
    private func setSelectedWorkingDays(item:String,index:Int) {
        let firstThreeLettersOfItem = String(item.prefix(3))
        selectedWorkingDays.append(firstThreeLettersOfItem)
        selectedWorkingDays.removeDuplicates()
        weekDays.remove(at: index)
        let sortedSelectedWorkingDays = sortSelectedWorkingDays()
        workingDaysBttn.setTitle(sortedSelectedWorkingDays.joined(separator: ","), for: .normal)
        workingDaysBttn.setTitleColor(UIColor.SALAMTECH.blue, for: .normal)
    }
    private func sortSelectedWorkingDays() -> [String] {
        let sortedSelectedDays = selectedWorkingDays.sorted { (s1, s2) -> Bool in
            let df = DateFormatter()
            df.dateFormat = "E"
            let firstDate = df.date(from: s1)!
            let secondDate = df.date(from: s2)!
            return firstDate < secondDate
        }
        return sortedSelectedDays
    }
    private func setupWorkingDaysDropDown() {
        let dropDown = showDropDown(dataSource: weekDays, onView:workingDaysBttn )
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            if item == "Clear selected days" {
                self?.clearSelectedWorkingDays()
            }else {
                self?.setSelectedWorkingDays(item: item,index:index)
            }
        }
    }
    
    private func showTimePickerView() {
        let timePicker = showDatePicker(mode: .time)
        timePicker.delegate = self
    }
    private func showDatePickerView() {
        let datePicker = showDatePicker(mode: .date)
        datePicker.delegate = self
    }
 

    private func convertBirthdateFormat(date:String) -> String {
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        let birthdateInDate = df.date(from: date)
        df.dateFormat = "yyyy-MM-dd"
        let birthdateInString = df.string(from: birthdateInDate!)
        return birthdateInString
    }
    private func setupProfile() {
        guard let birthDate = birthdateLabel.text , birthDate != "DD-MM-YYYY" else {
            let _ = bottomAlert(title: "Warning", message: "Please insert your birthdate")
            return
            
        }
        let birthdateInFormat = convertBirthdateFormat(date: birthDate)
        
        guard let seniorityLevel = seniorityLevel else {
            let _ = bottomAlert(title: "Warning", message: "Please select your seniority level")
            return
        }
        
        guard let fromTime = fromTime, let toTime = toTime else {
            let _ = bottomAlert(title: "Warning", message: "Please select working hours")
            return
        }
        
        guard let supSpecialist = subSpecialisationField.text, !supSpecialist.isEmpty else {
            let _ = bottomAlert(title: "Warning", message: "Please add sup Specialist")
            return
        }
        guard selectedGender != .none else {
            let _ = bottomAlert(title: "Warning", message: "Please select your gender")
            return
        }
        
        guard let specialist = selectedSpecialist else {
            let _ = bottomAlert(title: "Warning", message: "Please select specialist")
            return
        }
        
        guard let appointmentFees = appointmentFeesTxtField.text, !appointmentFees.isEmpty, let fees = Int(appointmentFees) else {
            let _ = bottomAlert(title: "Warning", message: "Please add appointment Fees")
            return
        }
        
        guard let numberOfPatients = numberOfPatientField.text, !numberOfPatients.isEmpty, let patientHour = Int(numberOfPatients) else {
            let _ = bottomAlert(title: "Warning", message: "Please add number of patients per hour.")
            return
        }
        
        var homeVisit: Int = 0
        var homeVisitFees: Int? = nil
        if isHomeVisitEnabled{
            guard let _homeVisitFees = homeVisitFeesField.text , !_homeVisitFees.isEmpty else{
                let _ = bottomAlert(title: "Warning", message: "Please add home visit fees.")
                return
            }
            homeVisit = 1
            homeVisitFees = Int(_homeVisitFees)
        }
        
        guard let profileImage = selectedProfileImage else {
            let _ = bottomAlert(title: "Warning", message: "Please add your profile picture.")
            return
        }
        
        let profileInfo = SetupProfileInfo(birthDate: birthdateInFormat, seniorityLevel: seniorityLevel, workTimeFrom: fromTime, workTimeTo: toTime, supSpecialist: supSpecialist, gender: selectedGender.rawValue, specialistID: specialist.id, fees: fees, patientHour: patientHour, homeVisit: homeVisit, homeVisitFees: homeVisitFees, services: services, workDays: selectedWorkingDays, certifications: certifications)
        
        print(profileInfo)
        showLoading()
        RM.setupProfile(with: profileInfo, image: profileImage, documents: documentsImages) { [weak self] (result:Result<TheDefaultResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success(let response):
                print(response)
//                let doctor:Doctor = response
//                UserDefaults.Account.set(doctor, forKey: .user)
                guard let window = self.view.window else {return}
                MainFlowPresenter().navigateToHomeNC(window: window)
            case .failure(let err):
                let _ = self.bottomAlert(title: err.message, message: err.joinedErrors, actionTitle: "Ok")
                
            }
        }
    }
    
    //MARK:-Actions
    @IBAction func onTapBirthDate(_ sender: Any) {
        showDatePickerView()
    }
    
    @IBAction func onTapFemale(_ sender: Any) {
        handleGenderUI(with: .female)
    }
    
    @IBAction func onTapMale(_ sender: Any) {
        handleGenderUI(with: .male)
    }
    
    private func handleGenderUI(with gender: Gender){
        self.selectedGender = gender
        
        switch gender {
        case .male:
            handleGenderButton(button: maleBttn, isActive: true)
            handleGenderButton(button: femaleBttn, isActive: false)
        case .female:
            handleGenderButton(button: maleBttn, isActive: false)
            handleGenderButton(button: femaleBttn, isActive: true)
        case .none:
            handleGenderButton(button: maleBttn, isActive: false)
            handleGenderButton(button: femaleBttn, isActive: false)
        }
    }
    
    private func handleGenderButton(button: UIButton, isActive: Bool){
        if isActive{
            button.backgroundColor = UIColor.SALAMTECH.skyBlue
            button.setTitleColor(UIColor.SALAMTECH.blue, for: .normal)
        }else{
            button.backgroundColor = .white
            button.setTitleColor(UIColor.lightGray.withAlphaComponent(0.6), for: .normal)
        }
        button.shadowRadius = 6
        button.shadowColor = .lightGray
        button.shadowOffset = .zero
        button.shadowOpacity = 0.3
    }

    @IBAction func onTapPickerSpecialization(_ sender: Any) {
        if specialists.count == 0{
            showLoading()
            let service = AuthService.specialists
            RM.request(service: service) { [weak self] (result:Result<SpecialistResponse, RequestError>) in
                guard let self = self else {return}
                self.hideLoading()
                switch result {
                case .success(let response):
                    self.specialists = response.specialists
                    self.setupPickSpecializationDropDown()
                case .failure(let err):
                    let _ = self.bottomAlert(title: err.message, message: err.joinedErrors, actionTitle: "Ok")
                }
            }
        }else{
            setupPickSpecializationDropDown()
        }
    }
    
    @IBAction func onTapLevelOfSeniority(_ sender: Any) {
        setupLevelOfSeniorityDropDown()
    }
    
    @IBAction func onTapSelectWorkingDays(_ sender: Any) {
        setupWorkingDaysDropDown()
    }
    
    @IBAction func onTapHomeVisit(_ sender: Any) {
        isHomeVisitEnabled.toggle()
        homeVisitView.backgroundColor = isHomeVisitEnabled ? UIColor.SALAMTECH.skyBlue: UIColor.white
    }
    
    @IBAction func onTapAddEducationAndCertificates(_ sender: Any) {
        guard let title = certificationTitleField.text , !title.isEmpty else {
            self.alert(title: "please add title", message: "")
            return
        }
        guard let body = certificationBodyField.text , !body.isEmpty else {
            self.alert(title: "please add body", message: "")
            return
        }
        certifications.append(Certification(title: title, body: body))
        certificationTitleField.text = nil
        certificationBodyField.text = nil
        educationTableView.reloadData()
    }
    @IBAction func onTapAddServices(_ sender: Any) {
        guard let service = serviceField.text , !service.isEmpty else {
            self.alert(title: "please add service", message: "")
            return
        }
        services.append(service)
        serviceField.text = nil
        servicesTableView.reloadData()
    }
    
    @IBAction func onTapAddProfilePhoto(_ sender: Any) {
        ImagePickerManager().pickImage(self) { [weak self] (image,ImageName)  in
            self?.selectedProfileImage = image
            self?.profilePhotoImageView.image = image
        }
    }
    
    @IBAction func onTapNext(_ sender: Any) {
        setupProfile()
    }
    var selectedImageName = String()
    @IBAction func onTapUploadDocuments(_ sender: Any) {
        ImagePickerManager().pickImage(self) { [weak self] (image,imageName)  in
            self?.documentsImages.append(image)
            self?.documentsTableView.isHidden = false
            self?.selectedImageName = imageName
            self?.documentsTableView.reloadData()
        }
    }
    
    
    @IBAction func onTapSelectFromOperatingHours(_ sender: Any) {
        fromOperatingHoursBttnIsTapped = true
        toOperatingHoursBttnIsTapped = false
        showTimePickerView()
    }
    
    @IBAction func onTapSelectToOperatingHours(_ sender: Any) {
        fromOperatingHoursBttnIsTapped = false
        toOperatingHoursBttnIsTapped = true
        showTimePickerView()
    }
    
    
}

//MARK:-Extension
extension SetupProfileVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case educationTableView:
            return certifications.count
        case servicesTableView:
            return services.count
        case documentsTableView:
            return documentsImages.count
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case educationTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: EducationCell.identifier, for: indexPath) as! EducationCell
            let item = certifications[indexPath.row]
            cell.eduTitleLabel.text = item.title
            cell.eduBodyLabel.text = item.body
            return cell
        case servicesTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: ServicesCell.identifier, for: indexPath) as! ServicesCell
            let service = services[indexPath.row]
            cell.serviceTitleLabel.text = service
            return cell
        case documentsTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: DocumentCell.identifier, for: indexPath) as! DocumentCell
            cell.configure(image: documentsImages[indexPath.row], imageName:selectedImageName)
            cell.delegate = self
            return cell
        default:
            return UITableViewCell()
        }
    }
}
extension SetupProfileVC:DatePickerViewDelegate {
    func onTapDoneBttn(date:Date,datePickerMode:UIDatePicker.Mode){
        switch datePickerMode {
        case .date:
            let df = DateFormatter()
            df.dateFormat = "dd-MM-yyyy"
            let birthdateInString = df.string(from: date)
            birthdateLabel.text = birthdateInString
            birthdateLabel.textColor = UIColor.SALAMTECH.blue!
        case .time:
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = DateFormatter.Style.short
            dateFormatter.timeZone = TimeZone.current
            let time = dateFormatter.string(from: date)
            
            if fromOperatingHoursBttnIsTapped {
                fromTime = convertTimeFormat(time: time)
                selectFromOperatingHoursBttn.setTitle(time, for: .normal)
            }else {
                toTime = convertTimeFormat(time: time)
                selectToOperatingHoursBttn.setTitle(time, for: .normal)
            }
        default:
            return
        }
    }
}
extension SetupProfileVC:DocumentCellDelegate {
    func deleteDocument(cell: DocumentCell) {
        guard let index = documentsTableView.indexPath(for: cell) else {return}
        documentsImages.remove(at: index.row)
        documentsTableView.beginUpdates()
        documentsTableView.deleteRows(at: [index], with: .left)
        documentsTableView.endUpdates()
        if documentsImages.isEmpty {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.documentsTableView.isHidden = true
            }
        }else {
            return
        }
    }
    
    
}
extension SetupProfileVC:StoryboardMakeable{
    static var storyboardName: String = Storyboard.setupProfileSB.rawValue
    typealias StoryboardMakeableType = SetupProfileVC
}
