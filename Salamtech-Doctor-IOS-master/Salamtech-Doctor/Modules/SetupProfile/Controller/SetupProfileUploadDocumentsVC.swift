//
//  SetupProfileUploadDocumentsVCViewController.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 2/9/21.
//  Copyright © 2021 Ahmed. All rights reserved.
//

import UIKit


class SetupProfileUploadDocumentsVC: UIViewController {

    //MARK:-Variables
    var previousClinicInfo: SetupClinicProfileInfo?
    var branchImages = [UIImage]()
    var clinicLogo = UIImage()
    
    var documentsImages = [UIImage]()
    
    //MARK:-Outlets
    @IBOutlet weak var documentsTableView: UITableView!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserDefaults.Account.string(forKey: .setupProfileStatus))
        configuration()
     
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
      
    }
    //MARK:-Methods
    private func configuration() {
       
        configureDocumentsTableView()
    }
  
    private func configureDocumentsTableView() {
        let nib = UINib(nibName: DocumentCell.identifier, bundle: nil)
        documentsTableView.register(nib, forCellReuseIdentifier: DocumentCell.identifier)
        documentsTableView.delegate = self
        documentsTableView.dataSource = self
    }
    
    
    //MARK:-Actions
    @IBAction func onTapNext(_ sender: Any) {
        if documentsImages.count != 0 {
            setupClinicProfile()
        } else {
            let _ = bottomAlert(title: "Warning", message: "Please add required documents")
        }
    }
    
    
    private func setupClinicProfile() {
                
        showLoading()
        
        guard let clinicInfo = previousClinicInfo else {return}
        
        RM.setupClinicProfile(with: clinicInfo, image: clinicLogo, clinicPhotos: branchImages, documents: documentsImages) {[weak self] (result:Result<TheDefaultResponse, RequestError>) in
            
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success(let response):
                print(response)
//                let doctor:Doctor = response
//                UserDefaults.Account.set(doctor, forKey: .user)
                guard let window = self.view.window else { return }
                MainFlowPresenter().navigateToHomeNC(window: window)
            case .failure(let err):
                let _ = self.bottomAlert(title: err.message, message: err.joinedErrors, actionTitle: "Ok")
                
            }
        }
    }
    
//    private func setupProfileUploadDocuments() {
//
//        showLoading()
//
//        guard let clinicInfo = previousClinicInfo else {return}
//
//        RM.setupClinicProfile(with: clinicInfo, image: clinicLogo, clinicPhotos: branchImages, documents: documentsImages) {[weak self] (result:Result<TheDefaultResponse, RequestError>) in
//
//            guard let self = self else {return}
//            self.hideLoading()
//            switch result {
//            case .success(let response):
//                print(response)
////                let doctor:Doctor = response
////                UserDefaults.Account.set(doctor, forKey: .user)
//                guard let window = self.view.window else { return }
//                MainFlowPresenter().navigateToHomeNC(window: window)
//            case .failure(let err):
//                let _ = self.bottomAlert(title: err.message, message: err.joinedErrors, actionTitle: "Ok")
//
//            }
//        }
//    }
    
    private func SaveToUserDefaults(response:LoginResponse) {
        guard let doctor = response.doctor else { return }
        UserDefaults.Account.set(value: doctor, key: .user)
//        UserDefaults.Account.set(doctor.token ?? "", forKey: .token)
        UserDefaults.Settings.set(true, forKey: .isUserLoggedIn)
        
        if doctor.setupProfileStatus == "4" {
            UserDefaults.Account.set("4", forKey: .setupProfileStatus)
            guard let window = self.view.window else { return }
            MainFlowPresenter().navigateToHomeNC(window: window)
        }
    }
    
    private func setupProfileUploadDocuments() {
                
        showLoading()
                
        RM.setupProfileUploadDocuments(with: documentsImages) {[weak self] (result:Result<LoginResponse, RequestError>) in
            
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success(let response):
                print(response)
                self.SaveToUserDefaults(response: response)
//                let doctor:Doctor = response
//                UserDefaults.Account.set(doctor, forKey: .user)
                
            case .failure(let err):
                let _ = self.bottomAlert(title: err.message, message: err.joinedErrors, actionTitle: "Ok")
                
            }
        }
    }
    
    var selectedImageName = String()
    
    @IBAction func onTapUploadDocuments(_ sender: Any) {
        ImagePickerManager().pickImage(self) { [weak self] (image,imageName)  in
            self?.documentsImages.append(image)
            self?.documentsTableView.isHidden = false
            self?.selectedImageName = imageName
            self?.documentsTableView.reloadData()
        }
    }
    
    @IBAction func signUpButtonIsPressed(_ sender: Any) {
        if documentsImages.count != 0 {
            self.setupProfileUploadDocuments()
        } else {
            let _ = bottomAlert(title: "Warning", message: "Please add required documents")
        }
    }
    
  
    
}

//MARK:-Extension
extension SetupProfileUploadDocumentsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case documentsTableView:
            return  documentsImages.count
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case documentsTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: DocumentCell.identifier, for: indexPath) as! DocumentCell
            cell.configure(image: documentsImages[indexPath.row], imageName:selectedImageName)
            cell.delegate = self
            return cell
        default:
            return UITableViewCell()
        }
    }
}

extension SetupProfileUploadDocumentsVC: DocumentCellDelegate {
    func deleteDocument(cell: DocumentCell) {
        guard let index = documentsTableView.indexPath(for: cell) else {return}
        documentsImages.remove(at: index.row)
        documentsTableView.beginUpdates()
        documentsTableView.deleteRows(at: [index], with: .left)
        documentsTableView.endUpdates()
        if documentsImages.isEmpty {
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.documentsTableView.isHidden = true
            }
        }else {
            return
        }
    }
}


extension SetupProfileUploadDocumentsVC:StoryboardMakeable{
    static var storyboardName: String = Storyboard.setupProfileSB.rawValue
    typealias StoryboardMakeableType = SetupProfileUploadDocumentsVC
}
