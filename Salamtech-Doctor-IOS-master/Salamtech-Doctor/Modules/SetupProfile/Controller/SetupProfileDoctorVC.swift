//
//  SetupProfileDoctorVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 2/8/21.
//  Copyright © 2021 Ahmed. All rights reserved.
//

import UIKit
import DropDown

class SetupProfileDoctorVC: UIViewController {
    //MARK:-Variables
    var certifications = [Certification]()
    
    var seniorityLevel:String?
    var specialists = [Specialist]()
    var selectedSpecialist: Specialist?
    
    var selectedGender = Gender.none
    enum Gender: Int {
        case male = 1
        case female = 2
        case none = 0
    }
    var selectedProfileImage: UIImage?
    
    //MARK:-Outlets
    @IBOutlet weak var birthdateLabel: UILabel!
    @IBOutlet weak var femaleBttn: UIButton!
    @IBOutlet weak var maleBttn: UIButton!
    @IBOutlet weak var pickSpecializationBttn: UIButton!
    @IBOutlet weak var levelOfSeniorityBttn: UIButton!
    @IBOutlet weak var educationTableView: SelfSizedTableView!
    @IBOutlet weak var profilePhotoImageView: UIImageView!
    
    @IBOutlet weak var subSpecialisationField: UITextField!
    @IBOutlet weak var certificationTitleField: UITextField!
    @IBOutlet weak var certificationBodyField: UITextField!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserDefaults.Account.string(forKey: .setupProfileStatus))
        configuration()
        setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //        setupDashedBorderLine()
    }
    //MARK:-Methods
    private func configuration() {
        configureEducationTableView()
        configureTxtFields()
    }
    private func setupUI() {
        handleGenderUI(with: selectedGender)
    }
    
    //    private func setupDashedBorderLine() {
    //        uploadCertifcatesView.setDashedBorderLine(boundsColor: UIColor.SALAMTECH.skyBlue!, cornerRadius: 5)
    //    }
    
    private func configureTxtFields() {
        
    }
    
    private func configureEducationTableView() {
        let nib = UINib(nibName: EducationCell.identifier, bundle: nil)
        educationTableView.register(nib, forCellReuseIdentifier: EducationCell.identifier)
        educationTableView.delegate = self
        educationTableView.dataSource = self
    }
    
    private func setupPickSpecializationDropDown() {
        let dataSource = specialists.map { $0.name }
        let dropDown = showDropDown(dataSource: dataSource, onView: pickSpecializationBttn)
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            self?.selectedSpecialist = self?.specialists[index]
            self?.pickSpecializationBttn.setTitle(item, for: .normal)
        }
    }
    
    private func setupLevelOfSeniorityDropDown() {
        let dropDown = showDropDown(dataSource: ["Doctor","Consulting Doctor","Professional Doctor"], onView:levelOfSeniorityBttn )
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            self?.levelOfSeniorityBttn.setTitle(item, for: .normal)
            self?.seniorityLevel = item
        }
    }
    
    
    private func showTimePickerView() {
        let timePicker = showDatePicker(mode: .time)
        timePicker.delegate = self
    }
    private func showDatePickerView() {
        let datePicker = showDatePicker(mode: .date)
        datePicker.delegate = self
    }
    
    
    private func convertBirthdateFormat(date:String) -> String {
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        let birthdateInDate = df.date(from: date)
        df.dateFormat = "yyyy-MM-dd"
        let birthdateInString = df.string(from: birthdateInDate!)
        return birthdateInString
    }
    
    private func SaveToUserDefaults(response:LoginResponse) {
        guard let doctor = response.doctor else { return }
        UserDefaults.Account.set(value: doctor, key: .user)
//        UserDefaults.Account.set(doctor.token ?? "", forKey: .token)
//        UserDefaults.Settings.set(true, forKey: .isUserLoggedIn)
        
        if doctor.setupProfileStatus == "2" {
            UserDefaults.Account.set("2" , forKey: .setupProfileStatus)
            guard let window = self.view.window else {return}
            MainFlowPresenter().navigateToSetupProfileClinic(window: window)
        }
    }
    
    private func setupProfile() {
        
        guard let birthDate = birthdateLabel.text , birthDate != "DD-MM-YYYY" else {
            let _ = bottomAlert(title: "Warning", message: "Please insert your birthdate")
            return
            
        }
        let birthdateInFormat = convertBirthdateFormat(date: birthDate)
        
        guard let seniorityLevel = seniorityLevel else {
            let _ = bottomAlert(title: "Warning", message: "Please select your seniority level")
            return
        }
        
        guard let supSpecialist = subSpecialisationField.text, !supSpecialist.isEmpty else {
            let _ = bottomAlert(title: "Warning", message: "Please add sup Specialist")
            return
        }
        
        guard selectedGender != .none else {
            let _ = bottomAlert(title: "Warning", message: "Please select your gender")
            return
        }
        
        guard let specialist = selectedSpecialist else {
            let _ = bottomAlert(title: "Warning", message: "Please select specialist")
            return
        }
        
        guard let profileImage = selectedProfileImage else {
            let _ = bottomAlert(title: "Warning", message: "Please add your profile picture.")
            return
        }
        
        
        if self.certifications.count <= 0 {
            let _ = bottomAlert(title: "Warning", message: "Please add at least one certificate.")
            return
        }
                
        let profileInfo = SetupDoctorProfileInfo(birthDate: birthdateInFormat, seniorityLevel: seniorityLevel, supSpecialist: supSpecialist, gender: selectedGender.rawValue, specialistID: specialist.id, certifications: certifications)
        
        print(profileInfo)
        
        showLoading()
        RM.setupDoctorProfile(with: profileInfo, image: profileImage) {[weak self] (result:Result<LoginResponse, RequestError>) in
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success(let response):
                print(response)
                self.SaveToUserDefaults(response: response)
                //                let doctor:Doctor = response
                //                UserDefaults.Account.set(doctor, forKey: .user)
                //                guard let window = self.view.window else {return}
                //                MainFlowPresenter().navigateToHomeNC(window: window)
//                guard let window = self.view.window else {return}
//                MainFlowPresenter().navigateToSetupProfileClinic(window: window)
            case .failure(let err):
                let _ = self.bottomAlert(title: err.message, message: err.joinedErrors, actionTitle: "Ok")
                
            }
        }
        //                RM.setupProfile(with: profileInfo, image: profileImage, documents: documentsImages) { [weak self] (result:Result<TheDefaultResponse, RequestError>) in
        //                    guard let self = self else {return}
        //                    self.hideLoading()
        //                    switch result {
        //                    case .success(let response):
        //                        print(response)
        //        //                let doctor:Doctor = response
        //        //                UserDefaults.Account.set(doctor, forKey: .user)
        //                        guard let window = self.view.window else {return}
        //                        MainFlowPresenter().navigateToHomeNC(window: window)
        //                    case .failure(let err):
        //                        let _ = self.bottomAlert(title: err.message, message: err.joinedErrors, actionTitle: "Ok")
        //
        //                    }
        //                }
    }
    
    //MARK:-Actions
    @IBAction func onTapBirthDate(_ sender: Any) {
        showDatePickerView()
    }
    
    @IBAction func onTapFemale(_ sender: Any) {
        handleGenderUI(with: .female)
    }
    
    @IBAction func onTapMale(_ sender: Any) {
        handleGenderUI(with: .male)
    }
    
    private func handleGenderUI(with gender: Gender){
        self.selectedGender = gender
        
        switch gender {
        case .male:
            handleGenderButton(button: maleBttn, isActive: true)
            handleGenderButton(button: femaleBttn, isActive: false)
        case .female:
            handleGenderButton(button: maleBttn, isActive: false)
            handleGenderButton(button: femaleBttn, isActive: true)
        case .none:
            handleGenderButton(button: maleBttn, isActive: false)
            handleGenderButton(button: femaleBttn, isActive: false)
        }
    }
    
    private func handleGenderButton(button: UIButton, isActive: Bool){
        if isActive{
            button.backgroundColor = UIColor.SALAMTECH.skyBlue
            button.setTitleColor(UIColor.SALAMTECH.blue, for: .normal)
        }else{
            button.backgroundColor = .white
            button.setTitleColor(UIColor.lightGray.withAlphaComponent(0.6), for: .normal)
        }
        button.shadowRadius = 6
        button.shadowColor = .lightGray
        button.shadowOffset = .zero
        button.shadowOpacity = 0.3
    }
    
    @IBAction func onTapPickerSpecialization(_ sender: Any) {
        if specialists.count == 0{
            showLoading()
            let service = AuthService.specialists
            RM.request(service: service) { [weak self] (result:Result<SpecialistResponse, RequestError>) in
                guard let self = self else {return}
                self.hideLoading()
                switch result {
                case .success(let response):
                    self.specialists = response.specialists
                    self.setupPickSpecializationDropDown()
                case .failure(let err):
                    let _ = self.bottomAlert(title: err.message, message: err.joinedErrors, actionTitle: "Ok")
                }
            }
        }else{
            setupPickSpecializationDropDown()
        }
    }
    
    @IBAction func onTapLevelOfSeniority(_ sender: Any) {
        setupLevelOfSeniorityDropDown()
    }
    
    
    
    @IBAction func onTapAddEducationAndCertificates(_ sender: Any) {
        guard let title = certificationTitleField.text , !title.isEmpty else {
            self.alert(title: "please add title", message: "")
            return
        }
        guard let body = certificationBodyField.text , !body.isEmpty else {
            self.alert(title: "please add body", message: "")
            return
        }
        certifications.append(Certification(title: title, body: body))
        certificationTitleField.text = nil
        certificationBodyField.text = nil
        educationTableView.reloadData()
    }
    
    @IBAction func onTapAddProfilePhoto(_ sender: Any) {
        ImagePickerManager().pickImage(self) { [weak self] (image,ImageName)  in
            self?.selectedProfileImage = image
            self?.profilePhotoImageView.image = image
        }
    }
    
    @IBAction func onTapNext(_ sender: Any) {
        setupProfile()
    }
    
}

//MARK:-Extension
extension SetupProfileDoctorVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case educationTableView:
            return certifications.count
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case educationTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: EducationCell.identifier, for: indexPath) as! EducationCell
            let item = certifications[indexPath.row]
            cell.eduTitleLabel.text = item.title
            cell.eduBodyLabel.text = item.body
            cell.clearButton.tag = indexPath.row
            cell.removeEducationDelegate = self
            return cell
        default:
            return UITableViewCell()
        }
    }
}
extension SetupProfileDoctorVC:DatePickerViewDelegate {
    
    func onTapDoneBttn(date:Date,datePickerMode:UIDatePicker.Mode){
        switch datePickerMode {
        case .date:
            let df = DateFormatter()
            df.dateFormat = "dd-MM-yyyy"
            let birthdateInString = df.string(from: date)
            birthdateLabel.text = birthdateInString
            birthdateLabel.textColor = UIColor.SALAMTECH.blue!
        //                case .time:
        //                    let dateFormatter = DateFormatter()
        //                    dateFormatter.timeStyle = DateFormatter.Style.short
        //                    dateFormatter.timeZone = TimeZone.current
        //                    let time = dateFormatter.string(from: date)
        //
        //                    if fromOperatingHoursBttnIsTapped {
        //                        fromTime = convertTimeFormat(time: time)
        //                        selectFromOperatingHoursBttn.setTitle(time, for: .normal)
        //                    }else {
        //                        toTime = convertTimeFormat(time: time)
        //                        selectToOperatingHoursBttn.setTitle(time, for: .normal)
        //                    }
        default:
            return
        }
    }
}

extension SetupProfileDoctorVC:StoryboardMakeable{
    static var storyboardName: String = Storyboard.setupProfileSB.rawValue
    typealias StoryboardMakeableType = SetupProfileDoctorVC
}

extension SetupProfileDoctorVC: RemoveEducationButtonDelegate {
    func removeEducationButtonIsPressed(educationIndex: Int) {
        certifications.remove(at: educationIndex)
        educationTableView.reloadData()
    }
}
