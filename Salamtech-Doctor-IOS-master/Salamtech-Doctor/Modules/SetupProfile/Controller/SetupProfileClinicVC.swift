//
//  SetupProfileClinicVC.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 2/9/21.
//  Copyright © 2021 Ahmed. All rights reserved.
//

import UIKit
import BSImagePicker
import Photos

class SetupProfileClinicVC: UIViewController{
    
    //MARK:-Variables
    var services = [String]()
    var selectedWorkingDays = [String]()
    var weekDays = ["Saturday","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Clear selected days"]
    
    var isHomeVisitEnabled: Bool = false
    var fromOperatingHoursBttnIsTapped = false
    var toOperatingHoursBttnIsTapped = false
    var fromTime:String?
    var toTime:String?
    
    var cities = [City]()
    var selectedCity: City?
    var areas = [Area]()
    var selectedArea: Area?
    
    var selectedClinicLogo: UIImage?
    
    var mainBranchImages = [UIImage]()
    
    var validtedClinicInfo: SetupClinicProfileInfo?
    
    //MARK:-Outlets
    
    @IBOutlet weak var clinicNameTF: UITextField!
    @IBOutlet weak var websiteTF: UITextField!
    @IBOutlet weak var branchesNumberTF: UITextField!
    @IBOutlet weak var clinicStreetNameTextField: UITextField!
    @IBOutlet weak var clinicGoogleMapLinkTextField: UITextField!
    
    @IBOutlet weak var appointmentFeesTxtField: UITextField!
    @IBOutlet weak var homeVisitView: UIView!
    @IBOutlet weak var workingDaysBttn: UIButton!
    @IBOutlet weak var servicesTableView: SelfSizedTableView!
    @IBOutlet weak var clinicLogoImageView: UIImageView!
    @IBOutlet weak var selectFromOperatingHoursBttn: UIButton!
    @IBOutlet weak var selectToOperatingHoursBttn: UIButton!
    
    @IBOutlet weak var homeVisitFeesField: UITextField!
    @IBOutlet weak var numberOfPatientField: UITextField!
    @IBOutlet weak var serviceField: UITextField!
    
    @IBOutlet weak var mainBranchPhotosCollectionView: UICollectionView!
    @IBOutlet weak var pickCityButton: UIButton!
    @IBOutlet weak var pickAreaButton: UIButton!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserDefaults.Account.string(forKey: .setupProfileStatus))
        configuration()
        setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    //MARK:-Methods
    private func configuration() {
        configureServicesTableView()
        configureTxtFields()
    }
    private func setupUI() {
    }
    
    private func configureTxtFields() {
        homeVisitFeesField.delegate = self
        numberOfPatientField.delegate = self
        appointmentFeesTxtField.delegate = self
        branchesNumberTF.delegate = self
    }
    
    private func configureServicesTableView() {
        let nib = UINib(nibName: ServicesCell.identifier, bundle: nil)
        servicesTableView.register(nib, forCellReuseIdentifier: ServicesCell.identifier)
        servicesTableView.delegate = self
        servicesTableView.dataSource = self
    }
    
    
    private func clearSelectedWorkingDays() {
        weekDays = ["Saturday","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Clear selected days"]
        selectedWorkingDays.removeAll()
        workingDaysBttn.setTitleColor(UIColor.lightGray, for: .normal)
        workingDaysBttn.setTitle("Select...", for: .normal)
    }
    private func setSelectedWorkingDays(item:String,index:Int) {
        let firstThreeLettersOfItem = String(item.prefix(3))
        selectedWorkingDays.append(firstThreeLettersOfItem)
        selectedWorkingDays.removeDuplicates()
        weekDays.remove(at: index)
        let sortedSelectedWorkingDays = sortSelectedWorkingDays()
        workingDaysBttn.setTitle(sortedSelectedWorkingDays.joined(separator: ","), for: .normal)
        workingDaysBttn.setTitleColor(UIColor.SALAMTECH.blue, for: .normal)
    }
    private func sortSelectedWorkingDays() -> [String] {
        let sortedSelectedDays = selectedWorkingDays.sorted { (s1, s2) -> Bool in
            let df = DateFormatter()
            df.dateFormat = "E"
            let firstDate = df.date(from: s1)!
            let secondDate = df.date(from: s2)!
            return firstDate < secondDate
        }
        return sortedSelectedDays
    }
    private func setupWorkingDaysDropDown() {
        let dropDown = showDropDown(dataSource: weekDays, onView:workingDaysBttn )
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            if item == "Clear selected days" {
                self?.clearSelectedWorkingDays()
            }else {
                self?.setSelectedWorkingDays(item: item,index:index)
            }
        }
    }
    
    private func showTimePickerView() {
        let timePicker = showDatePicker(mode: .time)
        timePicker.delegate = self
    }
    private func showDatePickerView() {
        let datePicker = showDatePicker(mode: .date)
        datePicker.delegate = self
    }
    
    
    private func convertBirthdateFormat(date:String) -> String {
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        let birthdateInDate = df.date(from: date)
        df.dateFormat = "yyyy-MM-dd"
        let birthdateInString = df.string(from: birthdateInDate!)
        return birthdateInString
    }
    
    
    private func validateDataBeforeGoingToUploadDocumentsScreen() -> Bool {
        
        guard let clinicName = clinicNameTF.text, !clinicName.isEmpty else {
            let _ = bottomAlert(title: "Warning", message: "Please add clinic name")
            return false
        }
        
        guard let website = websiteTF.text else {
            let _ = bottomAlert(title: "Warning", message: "Please add website url")
            return false
        }
        
        guard let numberOfPatients = numberOfPatientField.text, !numberOfPatients.isEmpty, let patientHour = Int(numberOfPatients) else {
            let _ = bottomAlert(title: "Warning", message: "Please add number of patients per hour.")
            return false
        }
        
        guard let appointmentFees = appointmentFeesTxtField.text, !appointmentFees.isEmpty, let fees = Int(appointmentFees) else {
            let _ = bottomAlert(title: "Warning", message: "Please add appointment Fees")
            return false
        }
        
        var homeVisit: Int = 0
        var homeVisitFees: Int? = nil
        if isHomeVisitEnabled {
            guard let _homeVisitFees = homeVisitFeesField.text , !_homeVisitFees.isEmpty else{
                let _ = bottomAlert(title: "Warning", message: "Please add home visit fees.")
                return false
            }
            homeVisit = 1
            homeVisitFees = Int(_homeVisitFees)
        }
        
        if services.count <= 0 {
            let _ = bottomAlert(title: "Warning", message: "Please add at least one service.")
            return false
        }
        
        guard let branchesNumber = branchesNumberTF.text, !branchesNumber.isEmpty else {
            let _ = bottomAlert(title: "Warning", message: "Please add number of branches")
            return false
        }
        
        guard let cityID = selectedCity?.id, cityID > 0 else {
            let _ = bottomAlert(title: "Warning", message: "Please choose clinic city.")
            return false
        }
        
        guard let areaID = selectedArea?.id, areaID > 0 else {
            let _ = bottomAlert(title: "Warning", message: "Please choose clinic area.")
            return false
        }

        guard let clinicStreetName = clinicStreetNameTextField.text, !clinicStreetName.isEmpty else {
            let _ = bottomAlert(title: "Warning", message: "Please add your clinic street name.")
            return false
        }
        
        guard let clinicGoogleMapLink = clinicGoogleMapLinkTextField.text, !clinicGoogleMapLink.isEmpty else {
            let _ = bottomAlert(title: "Warning", message: "Please add your clinic google map link.")
            return false
        }
        
        if selectedWorkingDays.count <= 0 {
            let _ = bottomAlert(title: "Warning", message: "Please add working days.")
            return false
        }
        
        guard let fromTime = fromTime, let toTime = toTime else {
            let _ = bottomAlert(title: "Warning", message: "Please select working hours")
            return false
        }
                                
        guard let clinicLogo = selectedClinicLogo else {
            let _ = bottomAlert(title: "Warning", message: "Please add your clinic logo.")
            return false
        }
        
//        guard let address = mainBranchAddressTF.text else {
//            let _ = bottomAlert(title: "Warning", message: "Please add your clinic address.")
//            return false
//        }
                
        if mainBranchImages.count == 0 {
            let _ = bottomAlert(title: "Warning", message: "Please add your branch images.")
            return false
        }
        
//        let profileInfo = SetupClinicProfileInfo(name: clinicName, website: website, patientHour: patientHour, fees: fees, homeVisit: homeVisit, homeVisitFees: homeVisitFees ?? 0, services: services, branchesNumber: Int(branchesNumber) ?? 0, cityID: cityID, areaID: areaID, address: "", streetName: clinicStreetName, googleMapLink: clinicGoogleMapLink, workTimeFrom: fromTime, workTimeTo: toTime, workDays: selectedWorkingDays)
        let profileInfo = SetupClinicProfileInfo(name: clinicName, website: website, patientHour: patientHour, fees: fees, homeVisit: homeVisit, homeVisitFees: homeVisitFees ?? 0, services: services, branchesNumber: Int(branchesNumber) ?? 0, cityID: cityID, areaID: areaID, address: "", streetName: clinicStreetName, workTimeFrom: fromTime, workTimeTo: toTime, workDays: selectedWorkingDays)

        print(profileInfo)
        
        self.validtedClinicInfo = profileInfo
        return true
        
    }
    
    private func setupClinicProfile() {
                
        showLoading()
        
        guard let clinicInfo = validtedClinicInfo else {return}
        
        RM.setupClinicProfileOnly(with: clinicInfo, image: self.selectedClinicLogo, clinicPhotos: self.mainBranchImages) {[weak self] (result:Result<LoginResponse, RequestError>) in
            
            guard let self = self else {return}
            self.hideLoading()
            switch result {
            case .success(let response):
                print(response)
//                let doctor:Doctor = response
//                UserDefaults.Account.set(doctor, forKey: .user)
                self.SaveToUserDefaults(response: response)
            case .failure(let err):
                let _ = self.bottomAlert(title: err.message, message: err.joinedErrors, actionTitle: "Ok")
                
            }
        }
    }
    
    private func SaveToUserDefaults(response:LoginResponse) {
        guard let doctor = response.doctor else { return }
        UserDefaults.Account.set(value: doctor, key: .user)
//        UserDefaults.Account.set(doctor.token ?? "", forKey: .token)
//        UserDefaults.Settings.set(true, forKey: .isUserLoggedIn)
        
        if doctor.setupProfileStatus == "3" {
            UserDefaults.Account.set("3", forKey: .setupProfileStatus)
            guard let window = self.view.window else { return }
            MainFlowPresenter().navigateToSetupDrProfileUploadDocumentsVC(window: window)
        }
    }
    
    //MARK:-Actions
    
    private func handleGenderButton(button: UIButton, isActive: Bool) {
        if isActive{
            button.backgroundColor = UIColor.SALAMTECH.skyBlue
            button.setTitleColor(UIColor.SALAMTECH.blue, for: .normal)
        }else{
            button.backgroundColor = .white
            button.setTitleColor(UIColor.lightGray.withAlphaComponent(0.6), for: .normal)
        }
        button.shadowRadius = 6
        button.shadowColor = .lightGray
        button.shadowOffset = .zero
        button.shadowOpacity = 0.3
    }
    
    private func setupPickCityDropDown() {
        let dataSource = cities.map { $0.name }
        let dropDown = showDropDown(dataSource: dataSource, onView: pickCityButton)
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            
            self?.selectedCity = self?.cities[index]
            if self?.selectedCity?.name != self?.pickCityButton.titleLabel?.text {
                self?.pickCityButton.setTitle(item, for: .normal)
                
                // init areas
                if self?.selectedCity?.areas?.count ?? 0 > 0 {
                    self?.areas = (self?.selectedCity?.areas)!
                    self?.selectedArea = nil
                    self?.pickAreaButton.setTitle("Select Clinic Area", for: .normal)
                }
            }
        }
    }
    
    private func setupPickAreaDropDown() {
        let dataSource = areas.map { $0.name }
        let dropDown = showDropDown(dataSource: dataSource, onView: pickAreaButton)
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            self?.selectedArea = self?.areas[index]
            self?.pickAreaButton.setTitle(item, for: .normal)
        }
    }
    
    @IBAction func onTapSelectWorkingDays(_ sender: Any) {
        setupWorkingDaysDropDown()
    }
    
    @IBAction func onTapHomeVisit(_ sender: Any) {
        isHomeVisitEnabled.toggle()
        homeVisitView.backgroundColor = isHomeVisitEnabled ? UIColor.SALAMTECH.skyBlue: UIColor.white
    }
    
    @IBAction func onTapAddServices(_ sender: Any) {
        guard let service = serviceField.text , !service.isEmpty else {
            self.alert(title: "please add service", message: "")
            return
        }
        services.append(service)
        serviceField.text = nil
        servicesTableView.reloadData()
    }
    
    @IBAction func onTapAddClinicLogo(_ sender: Any) {
        ImagePickerManager().pickImage(self) { [weak self] (image,ImageName)  in
            self?.selectedClinicLogo = image
            self?.clinicLogoImageView.image = image
        }
    }
    
    @IBAction func onTapNext(_ sender: Any) {
        
        if validateDataBeforeGoingToUploadDocumentsScreen() {
                        
            guard let info = validtedClinicInfo, let logo = selectedClinicLogo else {return}
            self.setupClinicProfile()
            guard let window = self.view.window else {return}
//            MainFlowPresenter().navigateToSetupProfileUploadDocumentsVC(window: window, info: info, branchPhotos: mainBranchImages, clinicLogo: logo)
            
            
        }
    }
    
    @IBAction func onTapSelectFromOperatingHours(_ sender: Any) {
        fromOperatingHoursBttnIsTapped = true
        toOperatingHoursBttnIsTapped = false
        showTimePickerView()
    }
    
    @IBAction func onTapSelectToOperatingHours(_ sender: Any) {
        fromOperatingHoursBttnIsTapped = false
        toOperatingHoursBttnIsTapped = true
        showTimePickerView()
    }
    
    @IBAction func onAddBranchPhotos(_ sender: UIButton) {
        let imagePicker = ImagePickerController()
        imagePicker.settings.selection.max = 10
        imagePicker.settings.theme.selectionStyle = .numbered
        imagePicker.settings.fetch.assets.supportedMediaTypes = [.image]
        imagePicker.settings.selection.unselectOnReachingMax = false
        
        
        self.presentImagePicker(imagePicker, select: { (asset) in
            print("Selected: \(asset)")
            
        }, deselect: { (asset) in
            print("Deselected: \(asset)")
        }, cancel: { (assets) in
            print("Canceled with selections: \(assets)")
        }, finish: { (assets) in
            print("Finished with selections: \(assets)")
            self.mainBranchImages.removeAll()
            for asset in assets {
                PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: nil) { (image, info) in
                    // Do something with image
                    
                    self.mainBranchImages.append(image ?? UIImage())
                }
                
            }
            
            self.mainBranchPhotosCollectionView.reloadData()
            
            print(self.mainBranchImages.count)
            
            
        }, completion: {
            
        })
    }
    
    @IBAction func onTapPickerCity(_ sender: Any) {
        if cities.count == 0 {
            showLoading()
            let service = AuthService.cities
            RM.request(service: service) { [weak self] (result:Result<CityResponse, RequestError>) in
                guard let self = self else {return}
                self.hideLoading()
                switch result {
                case .success(let response):
                    self.cities = response.cities
                    self.setupPickCityDropDown()
                case .failure(let err):
                    let _ = self.bottomAlert(title: err.message, message: err.joinedErrors, actionTitle: "Ok")
                }
            }
        } else {
            setupPickCityDropDown()
        }
    }
    
    @IBAction func onTapPickerArea(_ sender: Any) {
        if let selectedClinicCity = self.selectedCity {
            if selectedClinicCity.areas?.count ?? 0 > 0 {
                self.setupPickAreaDropDown()
            } else {
                let _ = self.bottomAlert(title: "Clinic Area", message: "There is no areas for this city", actionTitle: "Ok")
            }
        } else {
            let _ = self.bottomAlert(title: "Clinic Area", message: "Please pick up city first", actionTitle: "Ok")
        }
    }
    
}

//MARK:-Extension
extension SetupProfileClinicVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case servicesTableView:
            return services.count
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case servicesTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: ServicesCell.identifier, for: indexPath) as! ServicesCell
            let service = services[indexPath.row]
            cell.serviceTitleLabel.text = service
            cell.removeServiceDelegate = self
            cell.clearButton.tag = indexPath.row
            return cell
        default:
            return UITableViewCell()
        }
    }
}
extension SetupProfileClinicVC:DatePickerViewDelegate {
    
    func onTapDoneBttn(date:Date,datePickerMode:UIDatePicker.Mode){
        switch datePickerMode {
        //        case .date:
        //            let df = DateFormatter()
        //            df.dateFormat = "dd-MM-yyyy"
        //            let birthdateInString = df.string(from: date)
        //            birthdateLabel.text = birthdateInString
        //            birthdateLabel.textColor = UIColor.SALAMTECH.blue!
        case .time:
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = DateFormatter.Style.short
            dateFormatter.timeZone = TimeZone.current
            let time = dateFormatter.string(from: date)
            
            if fromOperatingHoursBttnIsTapped {
                fromTime = convertTimeFormat(time: time)
                selectFromOperatingHoursBttn.setTitle(time, for: .normal)
            }else {
                toTime = convertTimeFormat(time: time)
                selectToOperatingHoursBttn.setTitle(time, for: .normal)
            }
        default:
            return
        }
    }
}
//extension SetupProfileClinicVC:DocumentCellDelegate {
//
////    func deleteDocument(cell: DocumentCell) {
////        guard let index = documentsTableView.indexPath(for: cell) else {return}
////        documentsImages.remove(at: index.row)
////        documentsTableView.beginUpdates()
////        documentsTableView.deleteRows(at: [index], with: .left)
////        documentsTableView.endUpdates()
////        if documentsImages.isEmpty {
////            UIView.animate(withDuration: 0.3) { [weak self] in
////                self?.documentsTableView.isHidden = true
////            }
////        }else {
////            return
////        }
////    }
//
//
//}
extension SetupProfileClinicVC:StoryboardMakeable{
    static var storyboardName: String = Storyboard.setupProfileSB.rawValue
    typealias StoryboardMakeableType = SetupProfileClinicVC
}


extension SetupProfileClinicVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mainBranchImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = mainBranchPhotosCollectionView.dequeueReusableCell(withReuseIdentifier: "BranchPhotoCell", for: indexPath) as? BranchPhotoCell {
            cell.selectedImage.image = self.mainBranchImages[indexPath.row]
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //
        return CGSize(width: 100, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    
    
}

extension SetupProfileClinicVC: RemoveServiceButtonDelegate {
    func removeServiceButtonIsPressed(serviceIndex: Int) {
        services.remove(at: serviceIndex)
        servicesTableView.reloadData()
    }
}

extension SetupProfileClinicVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For numers
        if textField == branchesNumberTF || textField == homeVisitFeesField || textField == numberOfPatientField || textField == appointmentFeesTxtField {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
}
