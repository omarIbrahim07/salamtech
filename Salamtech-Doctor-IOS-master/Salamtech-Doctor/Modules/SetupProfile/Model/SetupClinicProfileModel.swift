//
//  SetupClinicProfileModel.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 2/10/21.
//  Copyright © 2021 Ahmed. All rights reserved.
//

import Foundation

struct SetupClinicProfileInfo:Codable {
    
    let name: String
    let website: String
    let patientHour: Int
    let fees: Int
    let homeVisit: Int
    let homeVisitFees: Int?
    let services: [String]
    let branchesNumber: Int
    
    let cityID: Int
    let areaID: Int
    let address: String?
    let streetName: String
//    let googleMapLink: String
    let workTimeFrom: String
    let workTimeTo: String
    let workDays: [String]
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case website = "website_url"
        case patientHour = "patient_hour"
        case homeVisit = "home_visit"
        case homeVisitFees = "home_visit_fees"
        case branchesNumber = "branches_no"
        case cityID = "city_id"
        case areaID = "area_id"
        case address = "address"
        case workDays = "work_days"
        case workTimeFrom = "work_time_from"
        case workTimeTo = "work_time_to"
        case services, fees
        case streetName = "street_name"
//        case googleMapLink = "google_map_link"
    }
}
