//
//  SetupDoctorProfileModel.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 2/10/21.
//  Copyright © 2021 Ahmed. All rights reserved.
//

import Foundation

struct SetupDoctorProfileInfo:Codable {
    
    let birthDate: String
    let seniorityLevel: String
    let supSpecialist: String
    let gender: Int
    let specialistID: Int
    let certifications: [Certification]
    
    enum CodingKeys: String, CodingKey {
        case birthDate = "birth_date"
        case gender,certifications
        case specialistID = "specialist_id"
        case supSpecialist = "sub_specialist"
        case seniorityLevel = "seniority_level"
    }
}

struct Certification:Codable {
    var title:String
    var body:String
}


