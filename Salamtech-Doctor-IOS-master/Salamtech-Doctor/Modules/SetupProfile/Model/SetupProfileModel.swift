//
//  SetupProfileModel.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/7/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation

struct SetupProfileInfo:Codable {
    
    let birthDate: String
    let seniorityLevel: String
    let workTimeFrom: String
    let workTimeTo: String
    let supSpecialist: String
    
    let gender: Int
    let specialistID: Int
    let fees: Int
    let patientHour: Int
    let homeVisit: Int
    let homeVisitFees: Int?
    
    let services: [String]
    let workDays: [String]
    let certifications: [Certification]
    
    enum CodingKeys: String, CodingKey {
        case birthDate = "birth_date"
        case services,fees,gender,certifications
        case specialistID = "specialist_id"
        case supSpecialist = "sub_specialist"
        case seniorityLevel = "seniority_level"
        case workDays = "work_days"
        case workTimeFrom = "work_time_from"
        case workTimeTo = "work_time_to"
        case patientHour = "patient_hour"
        case homeVisit = "home_visit"
        case homeVisitFees = "home_visit_fees"
        
    }
}

//struct Certification:Codable {
//    var title:String
//    var body:String
//}
