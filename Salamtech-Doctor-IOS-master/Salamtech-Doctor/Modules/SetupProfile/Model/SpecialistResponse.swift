//
//  SpecialistResponse.swift
//  Salamtech-Doctor
//
//  Created by hesham ghalaab on 9/24/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation

struct SpecialistResponse: Codable {
    let specialists: [Specialist]
}

struct Specialist: Codable {
    var id: Int
    var name: String
    var image: String?
}
