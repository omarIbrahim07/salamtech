//
//  SetupProfileUploadDocuments.swift
//  Salamtech-Doctor
//
//  Created by Omar Ibrahim on 10/03/2021.
//  Copyright © 2021 Ahmed. All rights reserved.
//

import Foundation

struct SetupProfileUploadDocuments: Codable {
    
    let birthDate: String
    
    enum CodingKeys: String, CodingKey {
        case birthDate = "birth_date"
    }
}
