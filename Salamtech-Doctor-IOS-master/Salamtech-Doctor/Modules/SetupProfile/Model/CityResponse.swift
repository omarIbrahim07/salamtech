//
//  CityResponse.swift
//  Salamtech-Doctor
//
//  Created by Omar Ibrahim on 13/03/2021.
//  Copyright © 2021 Ahmed. All rights reserved.
//

import Foundation

struct CityResponse: Codable {
    let cities: [City]
}

