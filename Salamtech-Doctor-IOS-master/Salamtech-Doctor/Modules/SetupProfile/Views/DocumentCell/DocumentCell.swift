//
//  DocumentCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 10/1/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit
import SDWebImage
protocol DocumentCellDelegate:class {
    func deleteDocument(cell:DocumentCell)
}
class DocumentCell: UITableViewCell {
    //MARK:-Variables
    
    static let identifier = "DocumentCell"
    weak var delegate:DocumentCellDelegate?
    var images = [UIImage]()
    //MARK:-Outlets
    
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var documentImageView: UIImageView!
    @IBOutlet weak var documentImageSizeLabel: UILabel!
    @IBOutlet weak var documentName: UILabel!
    //MARK:-Functions
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func configure(image:UIImage? = UIImage() ,imageLink:String? = "",imageName:String,imageSize:String = ""){
        if let link = imageLink, link != "" {
            print(link)
            documentImageView.addImage(withImage: imageLink, andPlaceHolder: "")
            documentName.text = imageName
            documentImageSizeLabel.text = imageSize
        }else {
            documentImageView.image = image
            documentImageSizeLabel.text = calculateDocumentSize(image: image ?? UIImage())
            documentName.text = imageName
        }
    }
    private func calculateDocumentSize(image:UIImage) -> String {
        let data = image.jpegData(compressionQuality: 1.0)!
        let size = Float(Double((data.count)/1024/1024))
        let sizeWithTwoDecimalsInMB = Double(round(10 * size) / 10)
        if sizeWithTwoDecimalsInMB < 1 {
            let sizeInKB = ((data.count)/1024)
            return "\(sizeInKB)KB"
        }else {
            return "\(sizeWithTwoDecimalsInMB)MB"
        }

    }
    //MARK:-Actions
    @IBAction func onTapDelete(_ sender: Any) {
        delegate?.deleteDocument(cell: self)
    }
    @IBAction func onTapShow(_ sender: Any) {
        
    }
}
