//
//  CertificatesCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/6/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit
protocol ServiceCellDelegate:class {
    func addingService(cell:ServicesCell)
}

protocol RemoveServiceButtonDelegate {
    func removeServiceButtonIsPressed(serviceIndex: Int)
}

class ServicesCell: UITableViewCell {
    static let identifier = "ServicesCell"
    weak var delegate:ServiceCellDelegate?
    var removeServiceDelegate: RemoveServiceButtonDelegate?

    @IBOutlet weak var clearImageView: UIImageView!
    @IBOutlet weak var serviceTitleLabel: UILabel!
    @IBOutlet weak var clearButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func removeServiceButtonIsPressed(serviceIndex: Int) {
        if let delegateValue = removeServiceDelegate {
            delegateValue.removeServiceButtonIsPressed(serviceIndex: serviceIndex)
        }
    }

    func configure(serviceTitle:String) {
        self.serviceTitleLabel.text = serviceTitle
    }
    
    @IBAction func clearServiceButtonIsPressed(_ sender: Any) {
        print("Clear is pressed")
        removeServiceButtonIsPressed(serviceIndex: clearButton.tag)
    }

}
