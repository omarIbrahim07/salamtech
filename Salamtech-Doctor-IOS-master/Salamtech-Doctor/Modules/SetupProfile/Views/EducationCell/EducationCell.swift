//
//  EducationCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/6/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

protocol RemoveEducationButtonDelegate {
    func removeEducationButtonIsPressed(educationIndex: Int)
}

class EducationCell: UITableViewCell {
    
    static let identifier = "EducationCell"
    
    var removeEducationDelegate: RemoveEducationButtonDelegate?
    
    @IBOutlet weak var clearImageView: UIImageView!
    @IBOutlet weak var eduTitleLabel: UILabel!
    @IBOutlet weak var eduBodyLabel: UILabel!
    @IBOutlet weak var clearButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configure(certificateTitle:String,certificateBody:String) {
        self.eduTitleLabel.text = certificateTitle
        self.eduBodyLabel.text = certificateBody
    }
    
    func removeEducationButtonIsPressed(educationIndex: Int) {
        if let delegateValue = removeEducationDelegate {
            delegateValue.removeEducationButtonIsPressed(educationIndex: educationIndex)
        }
    }

    @IBAction func clearButtonIsPressed(_ sender: Any) {
        print("Clear is pressed")
        removeEducationButtonIsPressed(educationIndex: clearButton.tag)
    }
    
}
