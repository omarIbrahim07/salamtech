//
//  MenuItem.swift
//  salamtech
//
//  Created by hesham ghalaab on 5/18/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import UIKit

struct MenuItem {
    
    let title: String
    let image: UIImage?
    let menuType: MenuType
    
    enum MenuType {
        case wallet
        case favroites
        case emergency
        case helpCenter
        case setting
        case signOut
        case spacer
    }
    
    static func dummyData() -> [MenuItem]{
        var menu = [MenuItem]()
        
        let item1 = MenuItem(title: "Wallet", image: UIImage(named: "wallet"),
                             menuType: .wallet)
        let item2 = MenuItem(title: "Favroites", image: UIImage(named: "favorite"),
                             menuType: .favroites)
        let item3 = MenuItem(title: "Help Center", image: UIImage(named: "HelpCenter"),
                             menuType: .helpCenter)
        let item4 = MenuItem(title: "Setting", image: UIImage(named: "Settings"),
                             menuType: .setting)
        
        let item5 = MenuItem(title: "Emergency", image: UIImage(named: "Emergency"),
                             menuType: .emergency)
        
        let item6 = MenuItem(title: "Sign out", image: UIImage(named: "SignOut"),
                             menuType: .signOut)
        
        let spacer = MenuItem(title: "", image: nil, menuType: .spacer)
        
        menu = [spacer, item1, item2, item3, item4, spacer, item5, spacer , item6]
        
        return menu
    }
}

