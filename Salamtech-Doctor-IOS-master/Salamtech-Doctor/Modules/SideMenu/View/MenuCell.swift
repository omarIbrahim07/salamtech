//
//  MenuCell.swift
//  salamtech
//
//  Created by hesham ghalaab on 5/18/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    static let identifier = "MenuCell"
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
