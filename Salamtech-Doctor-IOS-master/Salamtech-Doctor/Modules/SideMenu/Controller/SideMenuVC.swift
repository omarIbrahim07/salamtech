//
//  SideMenuVC.swift
//  salamtech
//
//  Created by hesham ghalaab on 5/17/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import UIKit

class SideMenuVC: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: HView!
    @IBOutlet weak var leadingContainerConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingContainerConstraint: NSLayoutConstraint!
    @IBOutlet weak var topContainerConstraint: NSLayoutConstraint!
    @IBOutlet weak var shadowView: UIView!
    
    
    @IBOutlet weak var firstShadowView: UIView!
    @IBOutlet weak var leadingFirstShadowConstraint: NSLayoutConstraint!
    @IBOutlet weak var topFirstShadowconstraint: NSLayoutConstraint!
    @IBOutlet weak var secondShadowView: UIView!
    @IBOutlet weak var leadingSecondShadowConstraint: NSLayoutConstraint!
    @IBOutlet weak var topSecondShadowConstraint: NSLayoutConstraint!
    @IBOutlet weak var thirdShadowView: UIView!
    @IBOutlet weak var leadingThirdShadowConstraint: NSLayoutConstraint!
    @IBOutlet weak var topThirdShadowConstraint: NSLayoutConstraint!
    
    // MARK: Properties
    enum MenuStatus {
        case opened
        case closed
    }
    var menuStatus = MenuStatus.closed
    var menuItems = MenuItem.dummyData()
    var tapGestureRecognizer: UITapGestureRecognizer?
    var panGestureRecognizer: UIPanGestureRecognizer?
    
    // MARK: Override Functions
    // viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(openMenuNotification(_:)), name: .openMenu, object: nil)
        
        configuration()
        setupUI()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .openMenu, object: nil)
    }
    
    @objc private func openMenuNotification(_ notification: NSNotification){
        toggleMenu()
    }
    
    private func toggleMenu(){
        switch menuStatus{
        case .opened:
            closeMenu()
            removeDismissGesture()
            menuStatus = .closed
        case .closed:
            openMenu()
            addGestureToDismiss()
            menuStatus = .opened
        }
    }
    
    private func openMenu(){
        let value = view.frame.width - 80
        leadingContainerConstraint.constant = value
        trailingContainerConstraint.constant = -value
        leadingFirstShadowConstraint.constant = value - 24
        leadingSecondShadowConstraint.constant = value - (24 * 2)
        leadingThirdShadowConstraint.constant = value - (24 * 3)
        
        let topValue: CGFloat = view.safeAreaInsets.top + 38
        topContainerConstraint.constant = topValue
        topFirstShadowconstraint.constant = topValue + 24
        topSecondShadowConstraint.constant = topValue + (24 * 2)
        topThirdShadowConstraint.constant = topValue + (24 * 3)
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
            self.containerView.layer.cornerRadius = 40
            self.containerView.layer.masksToBounds = true
            self.addShadow()
        }
    }
    private func closeMenu(){
        leadingContainerConstraint.constant = 0
        trailingContainerConstraint.constant = 0
        leadingFirstShadowConstraint.constant = 0
        leadingSecondShadowConstraint.constant = 0
        leadingThirdShadowConstraint.constant = 0
        
        topContainerConstraint.constant = 0
        topFirstShadowconstraint.constant = 0
        topSecondShadowConstraint.constant = 0
        topThirdShadowConstraint.constant = 0
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
            self.containerView.layer.cornerRadius = 0
            self.removeShadow()
        }) { (_) in
            //self.containerView.layer.masksToBounds = false
            self.shadowView.layer.shadowColor = UIColor.clear.cgColor
            self.firstShadowView.layer.shadowColor = UIColor.clear.cgColor
            self.secondShadowView.layer.shadowColor = UIColor.clear.cgColor
            self.thirdShadowView.layer.shadowColor = UIColor.clear.cgColor
        }
    }
    
    private func addShadow(){
        add(shadow: shadowView)
        add(shadow: firstShadowView)
        add(shadow: secondShadowView)
        add(shadow: thirdShadowView)
    }
    
    private func add(shadow theView: UIView){
        theView.layer.masksToBounds  = false
        theView.layer.shadowColor = UIColor.lightGray.cgColor
        theView.layer.shadowOffset = CGSize(width: -5, height: 5)
        theView.layer.shadowOpacity = 0.1
        theView.layer.shadowRadius = 10
        theView.layer.cornerRadius = 40
    }
    
    private func removeShadow(){
        shadowView.layer.cornerRadius = 0
        firstShadowView.layer.cornerRadius = 0
        secondShadowView.layer.cornerRadius = 0
        thirdShadowView.layer.cornerRadius = 0
    }
    
    // MARK: Methods
    // configuration: to configure any protocols
    private func configuration(){
        tableView.delegate = self
        tableView.dataSource = self
        let nib = UINib(nibName: MenuCell.identifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: MenuCell.identifier)
        
        let spacerNib = UINib(nibName: MenuSpacerCell.identifier, bundle: nil)
        tableView.register(spacerNib, forCellReuseIdentifier: MenuSpacerCell.identifier)
        
    }
    
    // setupUI: to setup data or make a custom design
    private func setupUI(){
        
    }
    
    // MARK: Actions
    
}

extension SideMenuVC: StoryboardMakeable{
    static var storyboardName: String = Storyboard.sideMenuSB.rawValue
    typealias StoryboardMakeableType = SideMenuVC
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension SideMenuVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = menuItems[indexPath.row]
        guard item.menuType != .spacer else {
            let cell = tableView.dequeueReusableCell(withIdentifier: MenuSpacerCell.identifier, for: indexPath) as! MenuSpacerCell
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MenuCell.identifier, for: indexPath) as! MenuCell
        cell.itemLabel.text = item.title
        cell.itemImageView.image = item.image
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = menuItems[indexPath.row]
        print(item.menuType)
        
        switch item.menuType {
        case .wallet:
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "walletSegue", sender: nil)
            }
        case .favroites:
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "favoritesSegue", sender: nil)
            }
        case .helpCenter:
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "helpCenterSegue", sender: nil)
            }
        case .setting:
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "settingsSegue", sender: nil)
            }
        case .emergency:
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "emergencySegue", sender: nil)
            }
        case .signOut:
          guard let window = view.window else {return}
            UserDefaults.Settings.set(false, forKey: .isUserLoggedIn)
            UserDefaults.Account.clear(keys: [.token, .user])
            UserDefaults.Settings.set(true, forKey: .isAppLaunchedBefore)
            MainFlowPresenter().navigateToAuthenticationNC(window: window)
        case .spacer: break
        }
    }
}

extension SideMenuVC: UIGestureRecognizerDelegate{
    
    func addGestureToDismiss(){
        self.containerView.isUserInteractionEnabled = false
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissMenu))
        tapGestureRecognizer?.delegate = self
        view.addGestureRecognizer(tapGestureRecognizer!)
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(dismissMenu))
        panGestureRecognizer?.delegate = self
        view.addGestureRecognizer(panGestureRecognizer!)
    }
    
    func removeDismissGesture(){
        self.containerView.isUserInteractionEnabled = true
        if let gesture = tapGestureRecognizer{
            view.removeGestureRecognizer(gesture)
            tapGestureRecognizer = nil
        }
        
        if let gesture = panGestureRecognizer{
            view.removeGestureRecognizer(gesture)
            panGestureRecognizer = nil
        }
    }
    
    @objc private func dismissMenu(){
        toggleMenu()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        guard let view = touch.view, !view.isDescendant(of: tableView) else { return false}
        return true
    }
}
