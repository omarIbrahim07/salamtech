//
//  BurgerSideMenu.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/19/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import UIKit
protocol IBurgerSideMenu:class {
    func menuIsDismissed()
}
class BurgerSideMenuVC:UIViewController{
    //MARK:-Variables
    weak var dismissingDelegate:IBurgerSideMenu?
//    var menuItems = ["Profile","Patients","Messages","Settings"]
    var menuItems = ["Profile","Consultations"]
    //MARK:-Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: SelfSizedTableView!
    @IBOutlet weak var doctorImageView: UIImageView!
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var doctorSubSpecialityLabel: UILabel!
    @IBOutlet weak var doctorSpecialityLogo: UIImageView!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        setupUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        containerView.roundCorners(corners: [.topRight,.bottomRight], radius: 20)
        //        tableView.maxHeight = CGFloat.infinity
    }
    //MARK:-Methods
    private func configuration() {
        configureTableView()
        setDoctorData()
    }
    private func setupUI() {
        self.navigationController?.navigationBar.isHidden = true
    }
    private func configureTableView() {
        let nib = UINib(nibName: BurgerMenuCell.identifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: BurgerMenuCell.identifier)
        tableView.estimatedRowHeight = 65
        tableView.delegate = self
        tableView.dataSource = self
    }
    private func setDoctorData() {
        var user:Doctor?
        user = UserDefaults.Account.get(forKey: .user)
        doctorImageView.addImage(withImage: user?.image, andPlaceHolder: "")
        doctorName.text = "Dr.\(user?.name ?? "")"
        doctorSubSpecialityLabel.text = user?.subSpecialist?[0]
        //        doctorSpecialityLogo.addImage(withImage: user., andPlaceHolder: "")
    }
    //MARK:-Actions
    @IBAction func onPanGesture(_ sender: Any) {
        dismiss(animated: true) {
            self.dismissingDelegate?.menuIsDismissed()
        }
        print("Pan dismiss")
    }
    @IBAction func onTapSignOut(_ sender: Any) {
        guard let window = view.window else {return}
        UserDefaults.Settings.set(false, forKey: .isUserLoggedIn)
        UserDefaults.Account.clear(keys: [.token, .user, .setupProfileStatus])
        UserDefaults.Settings.set(true, forKey: .isAppLaunchedBefore)
//        MainFlowPresenter().navigateToAuthenticationNC(window: window)
        dismiss(animated: true) {
            self.dismissingDelegate?.menuIsDismissed()
            MainFlowPresenter().navigateToLoginNC(window: window)
        }
    }
    
}
extension BurgerSideMenuVC:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BurgerMenuCell.identifier, for: indexPath) as! BurgerMenuCell
        cell.configure(menuItemTitle: menuItems[indexPath.row])
        return cell
    }
    
    
}
extension BurgerSideMenuVC:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 1 {
            performSegue(withIdentifier: "MessagesSegue", sender: nil)
        }else if indexPath.row == 0 {
            performSegue(withIdentifier: "ProfileSegue", sender: nil)
        }
    }
}
extension BurgerSideMenuVC: StoryboardMakeable{
    static var storyboardName: String = Storyboard.burgerSideMenuSB.rawValue
    typealias StoryboardMakeableType = BurgerSideMenuVC
}
