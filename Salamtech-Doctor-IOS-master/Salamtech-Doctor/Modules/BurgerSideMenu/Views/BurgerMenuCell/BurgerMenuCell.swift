//
//  BurgerMenuCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/19/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class BurgerMenuCell: UITableViewCell {
    static let identifier = "BurgerMenuCell"
    
    @IBOutlet weak var menuItemLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configure(menuItemTitle:String) {
        menuItemLabel.text = menuItemTitle
    }
}
