//
//  MedicalReportViewController.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/18/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class MedicalReportVC: UIViewController {
    //MARK:-Variables
    
    //MARK:-Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var uploadView: UIView!
    
    //MARK:-ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        containerView.roundCorners(corners: [.topRight,.topLeft], radius: 20)
        uploadView.setDashedBorderLine(boundsColor: UIColor.SALAMTECH.skyBlue!, cornerRadius: 5)
        
    }
    //MARK:-Methods
    private func configuration() {
        
    }
    private func setupUI() {
        
    }

    //MARK:-Actions
    @IBAction func onTapClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func onTapUploadDocuments(_ sender: Any) {
    }
    @IBAction func onTapAdd(_ sender: Any) {
    }
}
