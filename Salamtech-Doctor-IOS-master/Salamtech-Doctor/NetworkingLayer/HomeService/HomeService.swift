//
//  HomeService.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/13/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import Alamofire
enum HomeService {
    case getAppointments(with:AppointmentsParameters)
    case getPatientConsultations
    case getPatientMessages(with:ConsultationParameters)
    case sendDoctorMessage(with:SendMessageParameters)
    case cancelAppointment(with:CancelAppointmentParameter)
    case createChatRoom(with:ConsultationParameters)
    case getPatientEMR(with:EMRParameters)
    case deleteDocument(with:DeleteDocumentParameters)
}

extension HomeService: Service{
    
    var path: String {
        switch self {
        case .getAppointments: return "appointments"
        case .getPatientConsultations: return "consultations"
        case .getPatientMessages: return "messages"
        case .sendDoctorMessage: return "sendMessage"
        case .cancelAppointment: return "cancelAppointments"
        case .createChatRoom:return "consultations"
        case .getPatientEMR: return "EMR"
        case .deleteDocument: return "deleteEMRDocument"
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .createChatRoom(let para):return para.asDictionary
        case .getAppointments(let para): return para.asDictionary
        case .getPatientConsultations: return nil
        case .getPatientMessages(let para): return para.asDictionary
        case .sendDoctorMessage(let para): return para.asDictionary
        case .cancelAppointment(let para): return para.asDictionary
        case .getPatientEMR(let para): return para.asDictionary
        case .deleteDocument(let para): return para.asDictionary
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getAppointments,.getPatientMessages,.sendDoctorMessage,.cancelAppointment,.getPatientEMR: return .post
        case .getPatientConsultations,.createChatRoom: return .get
        case .deleteDocument: return .delete
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .getAppointments,.getPatientConsultations,.getPatientMessages,.sendDoctorMessage,.cancelAppointment,.createChatRoom,.getPatientEMR,.deleteDocument: return RequestComponent.headerComponent([.authorization])
        }
    }
}
