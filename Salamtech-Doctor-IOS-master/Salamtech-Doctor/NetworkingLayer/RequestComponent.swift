//
//  RequestComponent.swift
//  salamtech
//
//  Created by hesham ghalaab on 6/29/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import Foundation
import Alamofire

class RequestComponent{
    
    enum Component {
        case accept
        case authorization
        //case lang
    }
    
    class func headerComponent(_ component: [Component]) -> HTTPHeaders{
        var header = HTTPHeaders()
        
        for singleComponent in component{
            switch singleComponent{
            case .accept:
                header.add(name: "Accept", value: "application/json")
            case .authorization:
                if let token = UserDefaults.Account.object(forKey: .token) as? String {
                    header.add(name: "Authorization", value: token)
                }else{
                    print("trying to add a token in the header but their is no token")
                }
                
            //case .lang:
                //header["lang"] = LanguageManger.shared.currentLanguage.rawValue
            }
        }
        return header
    }
}

