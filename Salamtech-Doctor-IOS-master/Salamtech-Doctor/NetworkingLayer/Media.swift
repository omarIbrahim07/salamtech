//
//  Media.swift
//  salamtech
//
//  Created by Ahmed on 9/6/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import Foundation
struct Media {
   
  let key: String
  let fileName: String
  let data: Data
  let mimeType: String
   
  init?(withData data: Data, forKey key: String, mimeType: String, fileName: String) {
    self.data = data
    self.key = key
    self.mimeType = mimeType
    self.fileName = fileName
  }
}
