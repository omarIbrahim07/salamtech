//
//  DefaultResponse.swift
//  salamtech
//
//  Created by hesham ghalaab on 6/29/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import Foundation

struct DefaultResponse: Codable {
    let message: String?
    let errors: Dictionary<String, [String]>?
}


struct TheDefaultResponse: Codable {
    let response: DefaultResponse
    
    enum CodingKeys: String, CodingKey {
        case response = "default_response"
    }
    
}
