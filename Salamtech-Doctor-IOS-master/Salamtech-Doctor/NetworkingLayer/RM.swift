//
//  RM.swift
//  salamtech
//
//  Created by hesham ghalaab on 6/29/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//
import Foundation
import Alamofire

class RM {
    
    typealias Completion<T> = (Result<T, RequestError>) -> ()
    
    class func request<T: Decodable>(service: Service, completion: @escaping Completion<T>) {
        guard let url = service.url else {return}
        print(url)
        print(service.parameters)
        AF.request(url, method: service.method, parameters: service.parameters, encoding: URLEncoding.default, headers: service.headers)
            .validate()
            .responseJSON(completionHandler: { (response) in
                //                print(service.parameters)
                switch response.result{
                case .success(_):
                    self.decodeResponse(response: response, completion: completion)
                case .failure(let error):
                    let responseError = error.localizedDescription
                    print("😡 Failure statusCode: ",response.response?.statusCode ?? 0)
                    self.handleFailure(response: response, responseError: responseError, completion: completion)
                }
            })
    }
        
    private class func decodeResponse<T: Decodable>(response: AFDataResponse<Any>, completion: @escaping Completion<T>) {
        
        guard let responseData = response.data else {
            DispatchQueue.main.async {
                completion(.failure(RequestError(.emptyData)))
            }
            return
        }
        
        do{
            let data = try JSONDecoder().decode(T.self, from: responseData)
            DispatchQueue.main.async {
                completion(.success(data))
            }
        }catch let DecodingError.typeMismatch(type, context)  {
            print("############# DECODING_ERROR Type mismatch #############")
            print("Type '\(type)' mismatch:", context.debugDescription)
            print("codingPath:", context.codingPath)
            DispatchQueue.main.async {
                completion(.failure(RequestError(.decodingError)))
            }
        } catch{
            print("----------------")
            print("error in DECODING with:")
            print(error)
            DispatchQueue.main.async {
                completion(.failure(RequestError(.decodingError)))
            }
        }
    }
    
    private class func handleFailure<T: Decodable>(response: AFDataResponse<Any>, responseError: String, completion: @escaping Completion<T>) {
        
        guard let responseData = response.data else {
            DispatchQueue.main.async {
                print(responseError)
                completion(.failure(RequestError(responseError, errors: [:])))
            }
            return
        }
        do{
            let data = try JSONDecoder().decode(TheDefaultResponse.self, from: responseData)
            let message = data.response.message ?? ""
            let errors = data.response.errors ?? Dictionary<String, [String]>()
            let requestError = RequestError(message, errors: errors)
            
            DispatchQueue.main.async {
                print(requestError)
                completion(.failure(requestError))
            }
            
        } catch{
            do {
                if let json = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any]{
                    print("Error InDecoding Error with json:")
                    let defaultResponse = json["default_response"] as? [String: Any]
                    
                    print(json)
                }
            }
            catch{
            }
            DispatchQueue.main.async {
                print(responseError)
                
                completion(.failure(RequestError(responseError, errors: [:])))
            }
        }
    }
    
    class func updateEMR(with parameters:UpdateEMRParameters,documents:[DocumentUploadingDetails] = [],completion: @escaping (Result<TheDefaultResponse, RequestError>) -> () ) {
        guard let url = URL(string: "https://www.salam-tech.com/admin/public/doctors/updateEMR") else {
            print("Somthing went wrong in the url")
            return
        }
        let headers = RequestComponent.headerComponent([.authorization])
        let request = AF.upload(multipartFormData: { (form) in
            form.append("\(parameters.emrID)".data(using: .utf8)!, withName: "emr_id")
            form.append("\(parameters.report)".data(using: .utf8)!, withName: "report")
            for (index,item) in parameters.medicine.enumerated() {
                form.append("\(item.title ?? "")".data(using: .utf8)!, withName: "medecines[\(index)][title]")
                form.append("\(item.body ?? "")".data(using: .utf8)!, withName: "medecines[\(index)][body]")
            }
            for (index,item) in documents.enumerated()  {
                if let data = item.image?.jpegData(compressionQuality: 0.5) {
                    if let mediaImage = Media(withData: data, forKey: "documents[\(index)]", mimeType: "image/jpg", fileName: "\(documents[index].title).jpg") {
                        form.append(mediaImage.data, withName: mediaImage.key, fileName: mediaImage.fileName, mimeType: mediaImage.mimeType)
                    }
                }
            }
        }, to: url, usingThreshold: MultipartFormData.encodingMemoryThreshold, method: .post, headers: headers)
        request.uploadProgress{ progress in
            print("progress: ",Float(progress.fractionCompleted))
        }
        request.responseJSON { response in
            print("responseJSON: ",response)
            switch response.result{
            case .success(_):
                self.decodeResponse(response: response, completion: completion)
            case .failure(let error):
                let responseError = error.localizedDescription
                print("😡 Failure statusCode: ",response.response?.statusCode ?? 0)
                self.handleFailure(response: response, responseError: responseError, completion: completion)
            }
        }
    }
    
    
    class func setupDoctorProfile(with parameters: SetupDoctorProfileInfo, image: UIImage, completion: @escaping (Result<LoginResponse, RequestError>) -> ()) {
        
        guard let url = URL(string: "https://www.salam-tech.com/admin/public/doctors/setUpProfileFirstStep") else {
            print("Somthing went wrong in the url")
            return
        }
        
        let headers = RequestComponent.headerComponent([.accept, .authorization])
        print("url: \(url)")
        
        let request = AF.upload(multipartFormData: { (form) in
            
            form.append("\(parameters.birthDate)".data(using: .utf8)!, withName: "birth_date")
            form.append("\(parameters.gender)".data(using: .utf8)!, withName: "gender")
            form.append("\(parameters.specialistID)".data(using: .utf8)!, withName: "specialist_id")
            form.append("\(parameters.supSpecialist)".data(using: .utf8)!, withName: "sub_specialist")
            form.append("\(parameters.seniorityLevel)".data(using: .utf8)!, withName: "seniority_level")
            
            for (index,item) in parameters.certifications.enumerated() {
                form.append("\(item.title)".data(using: .utf8)!, withName: "certifications[\(index)][title]")
                form.append("\(item.body)".data(using: .utf8)!, withName: "certifications[\(index)][body]")
            }
            
            if let data = image.jpegData(compressionQuality: 0.5) {
                let random = "\(UUID().uuidString)"
                if let mediaImage = Media(withData: data, forKey: "image", mimeType: "image/jpg", fileName: "\(random).jpg") {
                    form.append(mediaImage.data, withName: mediaImage.key, fileName: mediaImage.fileName, mimeType: mediaImage.mimeType)
                }
            }
            
        }, to: url, usingThreshold: MultipartFormData.encodingMemoryThreshold, method: .post, headers: headers)
        
        request.uploadProgress { (progress) in
            print("first step progress: \(Float(progress.fractionCompleted))")
        }
        
        request.responseJSON { (response) in
            switch response.result {
            case .success(_):
                self.decodeResponse(response: response, completion: completion)
            case .failure(let error):
                let responseError = error.localizedDescription
                print("😡 Failure statusCode: ",response.response?.statusCode ?? 0)
                self.handleFailure(response: response, responseError: responseError, completion: completion)
            }
        }
    }
    
    // Upload clinic info & upload documents at the same time
    class func setupClinicProfile(with parameters: SetupClinicProfileInfo, image: UIImage, clinicPhotos: [UIImage],documents: [UIImage], completion: @escaping (Result<TheDefaultResponse, RequestError>) -> () ) {
        
        guard let url = URL(string: "https://www.salam-tech.com/admin/public/doctors/setUpProfileSecondStep") else {
            print("Somthing went wrong in the url")
            return
        }
        
        let headers = RequestComponent.headerComponent([.accept, .authorization])
        print("url: \(url)")
        
        let request = AF.upload(multipartFormData: { (form) in
            
            form.append("\(parameters.name)".data(using: .utf8)!, withName: "name")
            form.append("\(parameters.website)".data(using: .utf8)!, withName: "website_url")
            form.append("\(parameters.patientHour)".data(using: .utf8)!, withName: "patient_hour")
            form.append("\(parameters.fees)".data(using: .utf8)!, withName: "fees")
            form.append("\(parameters.homeVisit)".data(using: .utf8)!, withName: "home_visit")
            if let homeVisitFees = parameters.homeVisitFees{
                form.append("\(homeVisitFees)".data(using: .utf8)!, withName: "home_visit_fees")
            }
            
            for (index,item) in parameters.services.enumerated() {
                form.append("\(item)".data(using: .utf8)!, withName: "services[\(index)]")
            }
            
            form.append("\(parameters.branchesNumber)".data(using: .utf8)!, withName: "branches_no")
            form.append("\(parameters.address)".data(using: .utf8)!, withName: "address")
            form.append("\(parameters.workTimeTo)".data(using: .utf8)!, withName: "work_time_to")
            form.append("\(parameters.workTimeFrom)".data(using: .utf8)!, withName: "work_time_from")
         
            
            for (index,item) in parameters.workDays.enumerated() {
                form.append("\(item)".data(using: .utf8)!, withName: "work_days[\(index)]")
            }
            
          
            
            if let data = image.jpegData(compressionQuality: 0.5) {
                let random = "\(UUID().uuidString)"
                if let mediaImage = Media(withData: data, forKey: "image", mimeType: "image/jpg", fileName: "\(random).jpg") {
                    form.append(mediaImage.data, withName: mediaImage.key, fileName: mediaImage.fileName, mimeType: mediaImage.mimeType)
                }
            }
            for (index,item) in documents.enumerated() {
                if let data = item.jpegData(compressionQuality: 0.5) {
                    let random = "\(UUID().uuidString)"
                    if let mediaImage = Media(withData: data, forKey: "documents[\(index)]", mimeType: "image/jpg", fileName: "\(random).jpg") {
                        form.append(mediaImage.data, withName: mediaImage.key, fileName: mediaImage.fileName, mimeType: mediaImage.mimeType)
                    }
                }
            }
            
            for (index,item) in clinicPhotos.enumerated() {
                if let data = item.jpegData(compressionQuality: 0.5) {
                    let random = "\(UUID().uuidString)"
                    if let mediaImage = Media(withData: data, forKey: "images[\(index)]", mimeType: "image/jpg", fileName: "\(random).jpg") {
                        form.append(mediaImage.data, withName: mediaImage.key, fileName: mediaImage.fileName, mimeType: mediaImage.mimeType)
                    }
                }
            }
            
            
            
        }, to: url, usingThreshold: MultipartFormData.encodingMemoryThreshold, method: .post, headers: headers)
        
        request.uploadProgress { (progress) in
            print("second step progress: \(Float(progress.fractionCompleted))")
        }
        
        request.responseJSON { (response) in
            switch response.result {
            case .success(_):
                self.decodeResponse(response: response, completion: completion)
            case .failure(let error):
                let responseError = error.localizedDescription
                print("😡 Failure statusCode: ",response.response?.statusCode ?? 0)
                self.handleFailure(response: response, responseError: responseError, completion: completion)
            }
        }
    }
    

    class func setupClinicProfileOnly(with parameters: SetupClinicProfileInfo, image: UIImage?, clinicPhotos: [UIImage], completion: @escaping (Result<LoginResponse, RequestError>) -> () ) {

        guard let url = URL(string: "https://www.salam-tech.com/admin/public/doctors/setUpProfileSecondStep") else {
            print("Somthing went wrong in the url")
            return
        }
        
        let headers = RequestComponent.headerComponent([.accept, .authorization])
        print("url: \(url)")
        
        let request = AF.upload(multipartFormData: { (form) in
            
            form.append("\(parameters.name)".data(using: .utf8)!, withName: "name")
            form.append("\(parameters.website)".data(using: .utf8)!, withName: "website_url")
            form.append("\(parameters.patientHour)".data(using: .utf8)!, withName: "patient_hour")
            form.append("\(parameters.fees)".data(using: .utf8)!, withName: "fees")
            form.append("\(parameters.homeVisit)".data(using: .utf8)!, withName: "home_visit")
            if let homeVisitFees = parameters.homeVisitFees{
                form.append("\(homeVisitFees)".data(using: .utf8)!, withName: "home_visit_fees")
            }
            
            for (index,item) in parameters.services.enumerated() {
                form.append("\(item)".data(using: .utf8)!, withName: "services[\(index)]")
            }
            
            form.append("\(parameters.branchesNumber)".data(using: .utf8)!, withName: "branches_no")
            form.append("\(parameters.address)".data(using: .utf8)!, withName: "address")
            form.append("\(parameters.workTimeTo)".data(using: .utf8)!, withName: "work_time_to")
            form.append("\(parameters.workTimeFrom)".data(using: .utf8)!, withName: "work_time_from")
            form.append("\(parameters.streetName)".data(using: .utf8)!, withName: "street_name")
//            form.append("\(parameters.googleMapLink)".data(using: .utf8)!, withName: "google_map_link")
            form.append("\(parameters.cityID)".data(using: .utf8)!, withName: "city_id")
            form.append("\(parameters.areaID)".data(using: .utf8)!, withName: "area_id")
            
            for (index,item) in parameters.workDays.enumerated() {
                form.append("\(item)".data(using: .utf8)!, withName: "work_days[\(index)]")
            }
            
            if let data = image?.jpegData(compressionQuality: 0.5) {
                let random = "\(UUID().uuidString)"
                if let mediaImage = Media(withData: data, forKey: "image", mimeType: "image/jpg", fileName: "\(random).jpg") {
                    form.append(mediaImage.data, withName: mediaImage.key, fileName: mediaImage.fileName, mimeType: mediaImage.mimeType)
                }
            }
            
            for (index,item) in clinicPhotos.enumerated() {
                if let data = item.jpegData(compressionQuality: 0.5) {
                    let random = "\(UUID().uuidString)"
                    if let mediaImage = Media(withData: data, forKey: "images[\(index)]", mimeType: "image/jpg", fileName: "\(random).jpg") {
                        form.append(mediaImage.data, withName: mediaImage.key, fileName: mediaImage.fileName, mimeType: mediaImage.mimeType)
                    }
                }
            }
            
            
            
        }, to: url, usingThreshold: MultipartFormData.encodingMemoryThreshold, method: .post, headers: headers)
        
        request.uploadProgress { (progress) in
            print("second step progress: \(Float(progress.fractionCompleted))")
        }
        
        request.responseJSON { (response) in
            switch response.result {
            case .success(_):
                self.decodeResponse(response: response, completion: completion)
            case .failure(let error):
                let responseError = error.localizedDescription
                print("😡 Failure statusCode: ",response.response?.statusCode ?? 0)
                self.handleFailure(response: response, responseError: responseError, completion: completion)
            }
        }
    }
    
    class func setupProfileUploadDocuments(with documents: [UIImage], completion: @escaping (Result<LoginResponse, RequestError>) -> () ) {
        
        guard let url = URL(string: "https://www.salam-tech.com/admin/public/doctors/upload-documents") else {
            print("Somthing went wrong in the url")
            return
        }
        
        let headers = RequestComponent.headerComponent([.accept, .authorization])
        print("url: \(url)")
        
        let request = AF.upload(multipartFormData: { (form) in
            
            for (index,item) in documents.enumerated() {
                if let data = item.jpegData(compressionQuality: 0.5) {
                    let random = "\(UUID().uuidString)"
                    if let mediaImage = Media(withData: data, forKey: "documents[\(index)]", mimeType: "image/jpg", fileName: "\(random).jpg") {
                        form.append(mediaImage.data, withName: mediaImage.key, fileName: mediaImage.fileName, mimeType: mediaImage.mimeType)
                    }
                }
            }
            
        }, to: url, usingThreshold: MultipartFormData.encodingMemoryThreshold, method: .post, headers: headers)
        
        request.uploadProgress { (progress) in
            print("second step progress: \(Float(progress.fractionCompleted))")
        }
        
        request.responseJSON { (response) in
            switch response.result {
            case .success(_):
                self.decodeResponse(response: response, completion: completion)
            case .failure(let error):
                let responseError = error.localizedDescription
                print("😡 Failure statusCode: ",response.response?.statusCode ?? 0)
                self.handleFailure(response: response, responseError: responseError, completion: completion)
            }
        }
    }

    
    class func setupProfileUploadDocuments(with parameters: SetupClinicProfileInfo, image: UIImage?, clinicPhotos: [UIImage], completion: @escaping (Result<LoginResponse, RequestError>) -> () ) {

        guard let url = URL(string: "https://www.salam-tech.com/admin/public/doctors/setUpProfileSecondStep") else {
            print("Somthing went wrong in the url")
            return
        }
        
        let headers = RequestComponent.headerComponent([.accept, .authorization])
        print("url: \(url)")
        
        let request = AF.upload(multipartFormData: { (form) in
            
            form.append("\(parameters.name)".data(using: .utf8)!, withName: "name")
            form.append("\(parameters.website)".data(using: .utf8)!, withName: "website_url")
            form.append("\(parameters.patientHour)".data(using: .utf8)!, withName: "patient_hour")
            form.append("\(parameters.fees)".data(using: .utf8)!, withName: "fees")
            form.append("\(parameters.homeVisit)".data(using: .utf8)!, withName: "home_visit")
            if let homeVisitFees = parameters.homeVisitFees{
                form.append("\(homeVisitFees)".data(using: .utf8)!, withName: "home_visit_fees")
            }
            
            for (index,item) in parameters.services.enumerated() {
                form.append("\(item)".data(using: .utf8)!, withName: "services[\(index)]")
            }
            
            form.append("\(parameters.branchesNumber)".data(using: .utf8)!, withName: "branches_no")
            form.append("\(parameters.address)".data(using: .utf8)!, withName: "address")
            form.append("\(parameters.workTimeTo)".data(using: .utf8)!, withName: "work_time_to")
            form.append("\(parameters.workTimeFrom)".data(using: .utf8)!, withName: "work_time_from")
         
            
            for (index,item) in parameters.workDays.enumerated() {
                form.append("\(item)".data(using: .utf8)!, withName: "work_days[\(index)]")
            }
            
            if let data = image?.jpegData(compressionQuality: 0.5) {
                let random = "\(UUID().uuidString)"
                if let mediaImage = Media(withData: data, forKey: "image", mimeType: "image/jpg", fileName: "\(random).jpg") {
                    form.append(mediaImage.data, withName: mediaImage.key, fileName: mediaImage.fileName, mimeType: mediaImage.mimeType)
                }
            }
            
            for (index,item) in clinicPhotos.enumerated() {
                if let data = item.jpegData(compressionQuality: 0.5) {
                    let random = "\(UUID().uuidString)"
                    if let mediaImage = Media(withData: data, forKey: "images[\(index)]", mimeType: "image/jpg", fileName: "\(random).jpg") {
                        form.append(mediaImage.data, withName: mediaImage.key, fileName: mediaImage.fileName, mimeType: mediaImage.mimeType)
                    }
                }
            }
            
            
            
        }, to: url, usingThreshold: MultipartFormData.encodingMemoryThreshold, method: .post, headers: headers)
        
        request.uploadProgress { (progress) in
            print("second step progress: \(Float(progress.fractionCompleted))")
        }
        
        request.responseJSON { (response) in
            switch response.result {
            case .success(_):
                self.decodeResponse(response: response, completion: completion)
            case .failure(let error):
                let responseError = error.localizedDescription
                print("😡 Failure statusCode: ",response.response?.statusCode ?? 0)
                self.handleFailure(response: response, responseError: responseError, completion: completion)
            }
        }
    }

    
    class func setupProfile(with parameters: SetupProfileInfo, image:UIImage, documents:[UIImage], completion: @escaping (Result<TheDefaultResponse, RequestError>) -> () ) {
        guard let url = URL(string: "https://salamtak.redgits.com/doctors/setProfile") else {
            print("Somthing went wrong in the url")
            return
        }
        
        let headers = RequestComponent.headerComponent([.accept, .authorization])
        print("url: \(url)")
        let request = AF.upload(multipartFormData: { (form) in
            
            form.append("\(parameters.fees)".data(using: .utf8)!, withName: "fees")
            form.append("\(parameters.patientHour)".data(using: .utf8)!, withName: "patient_hour")
            form.append("\(parameters.specialistID)".data(using: .utf8)!, withName: "specialist_id")
            form.append("\(parameters.workTimeTo)".data(using: .utf8)!, withName: "work_time_to")
            form.append("\(parameters.workTimeFrom)".data(using: .utf8)!, withName: "work_time_from")
            form.append("\(parameters.gender)".data(using: .utf8)!, withName: "gender")
            form.append("\(parameters.homeVisit)".data(using: .utf8)!, withName: "home_visit")
            form.append("\(parameters.birthDate)".data(using: .utf8)!, withName: "birth_date")
            form.append("\(parameters.supSpecialist)".data(using: .utf8)!, withName: "sub_specialist")
            form.append("\(parameters.seniorityLevel)".data(using: .utf8)!, withName: "seniority_level")
            if let homeVisitFees = parameters.homeVisitFees{
                form.append("\(homeVisitFees)".data(using: .utf8)!, withName: "home_visit_fees")
            }
            
            for (index,item) in parameters.certifications.enumerated() {
                form.append("\(item.title)".data(using: .utf8)!, withName: "certifications[\(index)][title]")
                form.append("\(item.body)".data(using: .utf8)!, withName: "certifications[\(index)][body]")
            }
            
            for (index,item) in parameters.workDays.enumerated() {
                form.append("\(item)".data(using: .utf8)!, withName: "work_days[\(index)]")
            }
            
            for (index,item) in parameters.services.enumerated() {
                form.append("\(item)".data(using: .utf8)!, withName: "services[\(index)]")
            }
            
            if let data = image.jpegData(compressionQuality: 0.5) {
                let random = "\(UUID().uuidString)"
                if let mediaImage = Media(withData: data, forKey: "image", mimeType: "image/jpg", fileName: "\(random).jpg") {
                    form.append(mediaImage.data, withName: mediaImage.key, fileName: mediaImage.fileName, mimeType: mediaImage.mimeType)
                }
            }
            for (index,item) in documents.enumerated() {
                if let data = item.jpegData(compressionQuality: 0.5) {
                    let random = "\(UUID().uuidString)"
                    if let mediaImage = Media(withData: data, forKey: "documents[\(index)]", mimeType: "image/jpg", fileName: "\(random).jpg") {
                        form.append(mediaImage.data, withName: mediaImage.key, fileName: mediaImage.fileName, mimeType: mediaImage.mimeType)
                    }
                }
            }
            
        }, to: url, usingThreshold: MultipartFormData.encodingMemoryThreshold, method: .post, headers: headers)
        request.uploadProgress{ progress in
            print("progress: ",Float(progress.fractionCompleted))
        }
        request.responseJSON { response in
            print("responseJSON: ",response)
            
            switch response.result{
            case .success(_):
                self.decodeResponse(response: response, completion: completion)
            case .failure(let error):
                let responseError = error.localizedDescription
                print("😡 Failure statusCode: ",response.response?.statusCode ?? 0)
                self.handleFailure(response: response, responseError: responseError, completion: completion)
            }
        }
    }
    
    class func updateGeneralInfo(with parameters:ProfileModel,documents:[DocumentUploading] = [],profileImage:UIImage? = nil,completion: @escaping (Result<TheDefaultResponse, RequestError>) -> () ) {
        guard let url = URL(string: "https://www.salam-tech.com/admin/public/doctors/updateInfo") else {
            print("Somthing went wrong in the url")
            return
        }
        let headers = RequestComponent.headerComponent([.authorization])
        let request = AF.upload(multipartFormData: { (form) in
            guard let para = parameters.asDictionary else {return}
            print("############### params in edit general info #############")
            print(para)
//            for (key,value) in para {
//                form.append("\(value as? String)".data(using: .utf8)!, withName: key)
//            }
            form.append("\(parameters.doctor?.name ?? "")".data(using: .utf8)!, withName: "name")
            form.append("\(parameters.doctor?.email ?? "")".data(using: .utf8)!, withName: "email")
            form.append("\(parameters.doctor?.phone ?? "")".data(using: .utf8)!, withName: "phone")
            form.append("\(parameters.doctor?.birthDate ?? "")".data(using: .utf8)!, withName: "birth_date")
            form.append("\(parameters.doctor?.gender ?? "0")".data(using: .utf8)!, withName: "gender")
            form.append("\(parameters.doctor?.specialist?.id ?? 0)".data(using: .utf8)!, withName: "specialist_id")
            form.append("\(parameters.doctor?.subSpecialist?[0] ?? "")".data(using: .utf8)!, withName: "sub_specialist")
            form.append("\(parameters.doctor?.seniorityLevel ?? "")".data(using: .utf8)!, withName: "seniority_level")
            guard let services = parameters.doctor?.clinic?.services else {return}
            guard let certificates = parameters.doctor?.certifications else {return}
            for (index,item) in services.enumerated() {
                form.append("\(item)".data(using: .utf8)!, withName: "services[\(index)]")
            }   
            for (index,item) in certificates.enumerated() {
                form.append("\(item.title)".data(using: .utf8)!, withName: "certifications[\(index)][title]")
                form.append("\(item.body)".data(using: .utf8)!, withName: "certifications[\(index)][body]")
            }
            for (index,item) in documents.enumerated()  {
                if let data = item.image.jpegData(compressionQuality: 0.5) {
                    if let mediaImage = Media(withData: data, forKey: "documents[\(index)]", mimeType: "image/jpg", fileName: documents[index].name) {
                        form.append(mediaImage.data, withName: mediaImage.key, fileName: mediaImage.fileName, mimeType: mediaImage.mimeType)
                    }
                }
            }
            if let data = profileImage?.jpegData(compressionQuality: 0.5) {
                let random = "\(UUID().uuidString)"
                if let mediaImage = Media(withData: data, forKey: "image", mimeType: "image/jpg", fileName: "\(random).jpg") {
                    form.append(mediaImage.data, withName: mediaImage.key, fileName: mediaImage.fileName, mimeType: mediaImage.mimeType)
                }
            }
            //
        }, to: url, usingThreshold: MultipartFormData.encodingMemoryThreshold, method: .post, headers: headers)
        request.uploadProgress{ progress in
            print("progress: ",Float(progress.fractionCompleted))
        }
        request.responseJSON { response in
            print("################### edit genral response ################")
            print("responseJSON: ",response)
            switch response.result{
            case .success(_):
                self.decodeResponse(response: response, completion: completion)
            case .failure(let error):
                let responseError = error.localizedDescription
                print("😡 Failure statusCode: ",response.response?.statusCode ?? 0)
                self.handleFailure(response: response, responseError: responseError, completion: completion)
            }
        }
    }
    class func updateWorkClinicInfo(with parameters:EditWorkClinicInfoParameters,completion: @escaping (Result<TheDefaultResponse, RequestError>) -> () ) {
        guard let url = URL(string: "https://www.salam-tech.com/admin/public/doctors/updateWork") else {
            print("Somthing went wrong in the url")
            return
        }
        let headers = RequestComponent.headerComponent([.authorization])
        let request = AF.upload(multipartFormData: { (form) in
            for (index,item) in parameters.workDays.enumerated() {
                form.append("\(item)".data(using: .utf8)!, withName: "work_days[\(index)]")
            }
            form.append("\(parameters.address ?? "")".data(using: .utf8)!, withName: "address")
            form.append("\(parameters.areaId ?? 0)".data(using: .utf8)!, withName: "area_id")
            form.append("\(parameters.cityID ?? 0)".data(using: .utf8)!, withName: "city_id")
            form.append("\(parameters.longitude ?? 0)".data(using: .utf8)!, withName: "latitude")
            form.append("\(parameters.latitude ?? 0)".data(using: .utf8)!, withName: "longitude")

            form.append("\(parameters.workTimeFrom ?? "")".data(using: .utf8)!, withName: "work_time_from")
            form.append("\(parameters.workTimeTo ?? "")".data(using: .utf8)!, withName: "work_time_to")
            form.append("\(parameters.fees ?? 0)".data(using: .utf8)!, withName: "fees")
            form.append("\(parameters.patientHour ?? 0)".data(using: .utf8)!, withName: "patient_hour")
            form.append("\(parameters.homeVisit ?? 0)".data(using: .utf8)!, withName: "home_visit")
            if parameters.homeVisit == 1 {
                form.append("\(parameters.homeVisitFees ?? 0)".data(using: .utf8)!, withName: "home_visit_fees")
            }
        }, to: url, usingThreshold: MultipartFormData.encodingMemoryThreshold, method: .post, headers: headers)
        request.uploadProgress{ progress in
            print("progress: ",Float(progress.fractionCompleted))
        }
        request.responseJSON { response in
            print("responseJSON: ",response)
            switch response.result{
            case .success(_):
                self.decodeResponse(response: response, completion: completion)
            case .failure(let error):
                let responseError = error.localizedDescription
                print("😡 Failure statusCode: ",response.response?.statusCode ?? 0)
                self.handleFailure(response: response, responseError: responseError, completion: completion)
            }
        }
    }
}


struct RequestError: Error {
    var message: String
    var errors: Dictionary<String, [String]>
    
    var joinedErrors: String{
        var values = [String]()
        for item in errors{
            values.append(contentsOf: item.value)
        }
        
        return values.joined(separator: "\n")
    }
    
    init(_ message: ErrorMessage) {
        self.message = message.message
        self.errors = message.errors
    }
    
    init(_ message: String, errors: Dictionary<String, [String]>) {
        self.message = message
        self.errors = errors
    }
}

enum ErrorMessage{
    
    case unExpected
    case emptyResponse
    case emptyData
    case invalidUrl
    case invalidParameters
    case decodingError
    case internetConnectionError
    
    var message: String{
        switch self {
        case .unExpected:        return "UnExpected Error!"
        case .emptyResponse:     return "Cannot handle Response"
        case .emptyData:         return "Cannot handle data"
        case .invalidUrl:        return "Invalid url"
        case .invalidParameters: return "Invalid parameters"
        case .decodingError:     return "UnExpected decoding Error!"
        case .internetConnectionError: return "Enternet connection error."
        }
    }
    
    var errors: Dictionary<String, [String]>{
        return ["error": ["please try again later!"]]
    }
}
