//
//  AuthService.swift
//  salamtech
//
//  Created by hesham ghalaab on 6/29/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
enum AuthService {
    case login(with: LoginParameters)
    case signUp(with:SignUpParameters)
    case checkOnMailAndPhone(with:CheckOnMailAndPhoneModel)
    case specialists
    case cities
}

extension AuthService: Service{
  
    var path: String {
        switch self {
        case .login: return "login"
        case .signUp: return "register"
        case .checkOnMailAndPhone: return "sendVerifyCode"
        case .specialists: return "specialists"
        case .cities: return "cities"
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .login(let para): return para.asDictionary
        case .signUp(let para): return para.asDictionary
        case .checkOnMailAndPhone(let para): return para.asDictionary
        case .specialists: return nil
        case .cities: return nil
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .login,.signUp,.checkOnMailAndPhone: return .post
        case .specialists, .cities: return .get
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .login, .signUp, .checkOnMailAndPhone, .specialists, .cities: return RequestComponent.headerComponent([.accept])
        }
    }
}
