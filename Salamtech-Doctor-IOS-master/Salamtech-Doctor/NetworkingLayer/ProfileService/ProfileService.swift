//
//  ProfileService.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 10/26/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
import Alamofire
enum ProfileService {
    case getProfileData
    case deleteDocument(para:DeleteDocumentParameters)
}

extension ProfileService: Service{
  
    
    var path: String {
        switch self {
        case .getProfileData: return "profile"
        case .deleteDocument: return "deleteProfileDocument"
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .getProfileData:return nil
        case .deleteDocument(let para): return para.asDictionary
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getProfileData: return .get
        case .deleteDocument: return .delete
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .getProfileData,.deleteDocument: return RequestComponent.headerComponent([.authorization])
        }
    }
}
