//
//  Service.swift
//  salamtech
//
//  Created by hesham ghalaab on 6/29/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

 protocol Service {
    var path: String { get }
    var parameters: [String: Any]? { get }
    var method: HTTPMethod { get }
    var headers: HTTPHeaders { get }

    var endPoint: String { get }
    var url: URL?{ get }
}

extension Service{
    var endPoint: String {return END_POINT}
    var url: URL?{ return URL(string: END_POINT + path) }
}
 
//let END_POINT = "https://salamtak.redgits.com/doctors/"
//let END_POINT = "http://admin.mohamedanis.com/doctors/"

let END_POINT = "http://salam-tech.com/admin/public/doctors/"


