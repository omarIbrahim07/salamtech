//
//  RateCosmos.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/18/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit
import SwiftEntryKit
class RateCosmos: UIView {
    //MARK:-Variables
    private var onTapNext: (() -> Void)?
    private var onCloseTapped:  ( () -> Void)?
    //MARK:-Outlets
    
    //MARK:-Init
    init(onTapNext:@escaping (() -> Void),onCloseTapped: @escaping (() -> Void)) {
        super.init(frame: .zero)
        loadContentView()
        self.onCloseTapped = onCloseTapped
        self.onTapNext = onTapNext
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadContentView()
    }
    //MARK:-Methods
    private func loadContentView() {
        let contentView = Bundle.main.loadNibNamed("RateCosmos", owner: self)?.first as! UIView
        addSubview(contentView)
        contentView.fillSuperview()
    }
    
    @IBAction func onTapNext(_ sender: Any) {
        onTapNext?()
    }
    
}
