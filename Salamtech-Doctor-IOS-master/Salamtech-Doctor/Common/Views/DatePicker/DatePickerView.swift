//
//  DatePickerView.swift
//  salamtech
//
//  Created by AHMED on 9/20/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import Foundation
import UIKit
import SwiftEntryKit
protocol DatePickerViewDelegate:class {
    func onTapDoneBttn(date:Date,datePickerMode:UIDatePicker.Mode)
}
class DatePickerView:UIView {
    
    //MARK:-Variables
    weak var delegate:DatePickerViewDelegate?
    var mode:UIDatePicker.Mode?
    //MARK:-Outlets
    @IBOutlet var datePickerView:UIDatePicker!
    
    //MARK:-Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadContentView()
    }
    
    init(mode:UIDatePicker.Mode) {
        super.init(frame: .zero)
        loadContentView()
        datePickerView.datePickerMode = mode
        self.mode = mode
    }
    //MARK:-Functions
    private func loadContentView() {
        
        let contentView = Bundle.main.loadNibNamed("DatePickerView", owner: self)?.first as! UIView
        addSubview(contentView)
        contentView.fillSuperview()
    }
    //MARK:-Actions
    @IBAction private func onTapDoneBttn(_ sender: UIButton) {
        delegate?.onTapDoneBttn(date: datePickerView.date, datePickerMode: self.mode!)
        SwiftEntryKit.dismiss()
    }
}
