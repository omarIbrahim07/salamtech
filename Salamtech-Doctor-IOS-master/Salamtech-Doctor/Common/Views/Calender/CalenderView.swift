//
//  CalenderView.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/19/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit
import SwiftEntryKit

protocol CalendarViewDelegate:class {
    func didSelectDate(date:String)
}
class CalenderView: UIView {
    
    //MARK:-Variables
    var calenderDays = [Int]()
    weak var delegate:CalendarViewDelegate?
    var dayNames = [String]()
    //MARK:-Outlets
    @IBOutlet weak var dayNamesCollectionView: UICollectionView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var calenderCollectionView: UICollectionView!
    @IBOutlet weak var numberOfAppointmentsLabel: UILabel!
    @IBOutlet weak var todayLabel: UILabel!
    //MARK:-Inits
    init() {
        super.init(frame: .zero)
        loadContentView()
        calculateNumberOfDays()
        configuration()
        setupUI()
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadContentView()
    }
    override func draw(_ rect: CGRect) {
        self.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 20)
    }
    //MARK:-Methods
    
    private func configuration() {
        configureCalenderCell()
        configureDayCell()
        if UserDefaults.Account.string(forKey: .selectedCalendarDate) == nil {
            todayLabel.text = "Today"
        }else {
            todayLabel.text = UserDefaults.Account.string(forKey: .selectedCalendarDate)
        }
    }
    private func setupUI() {
        setupView()
    }
    private func loadContentView() {
        let contentView = Bundle.main.loadNibNamed("CalenderView", owner: self)?.first as! UIView
        addSubview(contentView)
        contentView.fillSuperview()
    }
    private func setupView() {
        containerView.layer.cornerRadius = 20
        setNumberOfTodayAppointments()
    }
    private func setNumberOfTodayAppointments() {
        var numberOfAppointments:Int?
        numberOfAppointments = UserDefaults.Account.integer(forKey: .noOfAppointments)
        if numberOfAppointments ?? 0 > 1 {
            numberOfAppointmentsLabel.text = "\(numberOfAppointments ?? 0) Appointments"
        }else {
            numberOfAppointmentsLabel.text = "\(numberOfAppointments ?? 0) Appointment"
            
        }
    }
    private func configureCalenderCell() {
        let nib = UINib(nibName: HomeCalenderCell.identifier, bundle: nil)
        calenderCollectionView.register(nib, forCellWithReuseIdentifier: HomeCalenderCell.identifier)
        calenderCollectionView.delegate = self
        calenderCollectionView.dataSource = self
    }
    private func configureDayCell() {
        let nib = UINib(nibName: HomeCalenderDayCell.identifier, bundle: nil)
        dayNamesCollectionView.register(nib, forCellWithReuseIdentifier: HomeCalenderDayCell.identifier)
        dayNamesCollectionView.delegate = self
        dayNamesCollectionView.dataSource = self
        dayNamesCollectionView.isScrollEnabled = false
    }
    private func calculateNumberOfDays() {
        dayNames = getDayNames()
        let year = Calendar.current.component(.year, from: Date())
        let month = Calendar.current.component(.month, from: Date())
        let dateComponents = DateComponents(year: year, month: month)
        let calendar = Calendar.current
        let date = calendar.date(from: dateComponents)!

        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        calenderDays = Array(1...numDays)
    }
    private func getCurrentYearAndMonth() -> String {
        let date = Date()
        return date.currentMonthAndYear
    }
    private func handleDateSelection(day:Int) -> String {
        if day < 10 {
            let dayInFormat = "-0\(day)"
            let currentYearAndMonth = getCurrentYearAndMonth()
            return "\(currentYearAndMonth)\(dayInFormat)"
        }else {
            let dayInFormat = "-\(day)"
            let currentYearAndMonth = getCurrentYearAndMonth()
            return "\(currentYearAndMonth)\(dayInFormat)"
        }
    }
    private func getDayNames() -> [String] {
        var days = ["S","M","T","W","T","F","S"]
        let day = Date().firstDayOfTheMonth.weekday
        var removedDays = [String]()
        switch day {
        case 1:
            return days
        case 2:
            removedDays.append(days.remove(at: 0))
            days.append(contentsOf: removedDays)
        case 3:
            for _ in 0...1 {
                removedDays.append(days.remove(at: 0))
            }
            days.append(contentsOf: removedDays)
        case 4:
            for _ in 0...2 {
                removedDays.append(days.remove(at: 0))
            }
            days.append(contentsOf: removedDays)
        case 5:
            for _ in 0...3 {
                removedDays.append(days.remove(at: 0))
            }
            days.append(contentsOf: removedDays)
        case 6:
            for _ in 0...4 {
                removedDays.append(days.remove(at: 0))
            }
            days.append(contentsOf: removedDays)
        case 7:
            for _ in 0...5 {
                removedDays.append(days.remove(at: 0))
            }
            days.append(contentsOf: removedDays)
        default:
            return []
        }
        return days
    }
    private func getTodayDate() -> Int {
        let day = Calendar.current.component(.day, from: Date())
        return day
    }
    
    private func handleCalenderCollectionView(indexPath:IndexPath,collectionView:UICollectionView) -> HomeCalenderCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCalenderCell.identifier, for: indexPath) as! HomeCalenderCell
        let today = getTodayDate()
        let day = calenderDays[indexPath.row]
        cell.configure(day: day)
        return handleLastSelectedDay(indexPath: indexPath, cell: cell, today: today)
    }
    private func handleLastSelectedDay(indexPath:IndexPath,cell:HomeCalenderCell,today:Int) -> HomeCalenderCell {
        if calenderDays[indexPath.row] == UserDefaults.Account.integer(forKey: .selectedCalendarDay) {
            cell.isCellSelected = true
            return cell
        }else if calenderDays[indexPath.row] == today && UserDefaults.Account.integer(forKey: .selectedCalendarDay) == 0 {
            cell.isCellSelected = true
            return cell
        }else {
            return cell
        }
    }
    private func handleDayNamesCollectionView(indexPath:IndexPath,collectionView:UICollectionView) -> HomeCalenderDayCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCalenderDayCell.identifier, for: indexPath) as! HomeCalenderDayCell
        cell.configure(dayName: dayNames[indexPath.row])
        return cell
    }
    //MARK:-Actions

}

extension CalenderView:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case calenderCollectionView:
            return calenderDays.count
        case dayNamesCollectionView:
            return dayNames.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case calenderCollectionView:
            let cell = handleCalenderCollectionView(indexPath: indexPath, collectionView: collectionView)
            return cell
        case dayNamesCollectionView:
            let cell = handleDayNamesCollectionView(indexPath: indexPath, collectionView: collectionView)
            return cell
        default:
            return UICollectionViewCell()
        }
        
    }
}
extension CalenderView:UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case calenderCollectionView:
            let cell = collectionView.cellForItem(at: indexPath) as? HomeCalenderCell
            cell?.isCellSelected = true
            let date = handleDateSelection(day: calenderDays[indexPath.row])
            UserDefaults.Account.set(date, forKey: .selectedCalendarDate)
            UserDefaults.Account.set(calenderDays[indexPath.row], forKey: .selectedCalendarDay)
            todayLabel.text = date
            delegate?.didSelectDate(date:date)
        case dayNamesCollectionView:
            return
        default:
            return
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        switch collectionView {
        case calenderCollectionView:
            let cell = collectionView.cellForItem(at: indexPath) as? HomeCalenderCell
            cell?.isCellSelected = false
        case dayNamesCollectionView:
            return
        default:
            return
        }
    }
}
extension CalenderView:UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case calenderCollectionView:
            return CGSize(width: 40, height: 50)
        case dayNamesCollectionView:
            return CGSize(width: 40, height: dayNamesCollectionView.frame.size.height)
        default:
            return CGSize()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
       return 8
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
}
