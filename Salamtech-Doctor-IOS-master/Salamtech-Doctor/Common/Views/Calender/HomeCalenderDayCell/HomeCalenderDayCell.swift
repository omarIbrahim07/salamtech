//
//  HomeCalenderDayCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/20/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class HomeCalenderDayCell: UICollectionViewCell {
    static let identifier = "HomeCalenderDayCell"
    @IBOutlet weak var dayNameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configure(dayName:String){
        dayNameLabel.text = dayName
    }

}
