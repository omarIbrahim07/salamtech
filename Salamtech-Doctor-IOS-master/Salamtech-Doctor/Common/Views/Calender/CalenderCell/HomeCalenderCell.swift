//
//  CalenderCell.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/20/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class HomeCalenderCell: UICollectionViewCell {
    static let identifier = "HomeCalenderCell"
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var ovalImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ovalImageView.isHidden = true
    }
    var isCellSelected:Bool = false {
        didSet{
            if isCellSelected {
                ovalImageView.isHidden = false
                containerView.backgroundColor = UIColor.SALAMTECH.skyBlue
            }else {
                ovalImageView.isHidden = true
                containerView.backgroundColor = .white
            }
        }
    }
    func configure(day:Int){
        dayLabel.text = "\(day)"
    }
}
