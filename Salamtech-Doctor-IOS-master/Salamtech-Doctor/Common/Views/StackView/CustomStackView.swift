//
//  CustomStackView.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/23/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit

class CustomStackView: UIStackView {
    //MARK:-Variables
    var currentContentOffset:CGPoint?
    var lastOffset:CGPoint? = CGPoint(x: 0.0, y: 0.0)
    var currentPage:Int = 0 {
        didSet{
            print(currentPage)
            lastOffset = currentContentOffset
            if currentContentOffset!.x - lastOffset!.x < 0 {
                images[currentPage + 1].translatesAutoresizingMaskIntoConstraints = false
                images[currentPage + 1].widthAnchor.constraint(equalToConstant: 7.0).isActive = true
                images[currentPage + 1].heightAnchor.constraint(equalToConstant: 7.0).isActive = true
                images[currentPage + 1].image = UIImage(named: "unselectedPage")
            }else {
            if currentPage != 0 {
                images[currentPage - 1].translatesAutoresizingMaskIntoConstraints = false
                images[currentPage - 1].widthAnchor.constraint(equalToConstant: 7.0).isActive = true
                images[currentPage - 1].heightAnchor.constraint(equalToConstant: 7.0).isActive = true
                images[currentPage - 1].image = UIImage(named: "unselectedPage")
            }
//            images[currentPage].translatesAutoresizingMaskIntoConstraints = false
            images[currentPage].widthAnchor.constraint(equalToConstant: 15.0).isActive = true
            images[currentPage].heightAnchor.constraint(equalToConstant: 7.0).isActive = true
            images[currentPage].image = UIImage(named: "SelectedPage")
        }
        }
    }
    var numberOfPages = 0 {
        didSet{
            for _ in 0 ..< numberOfPages  {
                images.append(UIImageView(image: UIImage(named: "unselectedPage")))
            }
        }
    }
    var images = [UIImageView]()

    //MARK:-Inits
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    required init(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    //MARK:-Functions
    func setupStackViews(){
        self.axis  = NSLayoutConstraint.Axis.horizontal
        self.spacing   = 10
        self.translatesAutoresizingMaskIntoConstraints = false
        images[currentPage].translatesAutoresizingMaskIntoConstraints = false
        images[currentPage] = UIImageView(image: UIImage(named: "SelectedPage"))
        images[currentPage].heightAnchor.constraint(equalToConstant: 7).isActive = true
        images[currentPage].widthAnchor.constraint(equalToConstant: 15.0).isActive = true
        self.addArrangedSubview(images[currentPage])
        for x in 1 ..< numberOfPages {
            images[x].translatesAutoresizingMaskIntoConstraints = false
            images[x] = UIImageView(image: UIImage(named: "unselectedPage"))
            images[x].heightAnchor.constraint(equalToConstant: 7.0).isActive = true
            images[x].widthAnchor.constraint(equalToConstant: 7.0).isActive = true
            self.addArrangedSubview((images[x]))
        }
        
        
    }
    
}
