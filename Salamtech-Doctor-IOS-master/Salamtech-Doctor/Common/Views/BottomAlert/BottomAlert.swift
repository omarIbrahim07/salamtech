//
//  BottomAlert.swift
//  salamtech
//
//  Created by hesham ghalaab on 5/22/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import UIKit
import SwiftEntryKit

class BottomAlert: UIView {

    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var messageLabel: UILabel!
    @IBOutlet weak private var actionButton: UIButton!
    
    var onActionTapped: ( () -> Void)?
    private var onCloseTapped:  ( () -> Void)!

    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        loadContentView()
    }

    init(title: String, message: String, actionTitle: String, onCloseTapped: @escaping (() -> Void)) {
        super.init(frame: .zero)
        loadContentView()
        self.titleLabel.text = title
        self.messageLabel.text = message
        self.actionButton.setTitle(actionTitle, for: .normal)
        
        self.onCloseTapped = onCloseTapped
    }

    private func loadContentView() {
        
        let contentView = Bundle.main.loadNibNamed("BottomAlert", owner: self)?.first as! UIView
        addSubview(contentView)
        contentView.fillSuperview()
    }

    @IBAction private func onTapClose(_ sender: UIButton) {
        onCloseTapped()
    }
    
    @IBAction private func onTapOnAction(_ sender: UIButton) {
        onCloseTapped()
        onActionTapped?()
    }

}
