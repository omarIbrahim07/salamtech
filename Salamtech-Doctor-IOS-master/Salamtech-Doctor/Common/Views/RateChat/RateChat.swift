//
//  RateChat.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/18/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit
import SwiftEntryKit
class RateChat: UIView {
    //MARK:-Variable
    private var onCloseTapped:  ( () -> Void)?
    private var onLikeTapped: (() -> Void)?
    private var onDislikeTapped: (() -> Void)?
    //MARK:-Outlets
    
    //MARK:-Init
    init(onTapLike:@escaping (() -> Void),onTapDisLike:@escaping (() -> Void),onCloseTapped: @escaping (() -> Void)) {
        super.init(frame: .zero)
        loadContentView()
        self.onCloseTapped = onCloseTapped
        self.onLikeTapped = onTapLike
        self.onDislikeTapped = onTapDisLike
    }
   required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadContentView()
    }
    //MARK:-Methods
    private func loadContentView() {
        let contentView = Bundle.main.loadNibNamed("RateChat", owner: self)?.first as! UIView
        addSubview(contentView)
        contentView.fillSuperview()
    }
    
    //MARK:-Actions
    @IBAction func onTapClose(_ sender: Any) {
        onCloseTapped?()
    }
    @IBAction func onTapGood(_ sender: Any) {
        onLikeTapped?()
    }
    @IBAction func onTapBad(_ sender: Any) {
        onDislikeTapped?()
    }
}
