//
//  MainFlowPresenter.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/17/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import UIKit
import Foundation
struct MainFlowPresenter {
    
    func navigateToSplash(window: UIWindow){
        
        
    }
    func navigateToOnBoardingNC(window:UIWindow) {
        let nav = OnBoardingNC.make()
        clearPresntedControllersIfNeeded(from: window)
        handlingTransition(for: window, options: .transitionCrossDissolve)
        setRoot(to: nav, for: window)
    }
    func navigateToAuthenticationNC(window: UIWindow){
        let nav = AuthenticationNC.make()
        clearPresntedControllersIfNeeded(from: window)
        handlingTransition(for: window, options: .transitionCrossDissolve)
        setRoot(to: nav, for: window)
    }
    
    func navigateToLoginNC(window: UIWindow){
        let nav = LoginVC.make()
//        let nav = LoginNC.make()
        clearPresntedControllersIfNeeded(from: window)
        handlingTransition(for: window, options: .transitionCrossDissolve)
        setRoot(to: nav, for: window)
    }
    
    func navigateToSetupProfile(window:UIWindow) {
        let setupProfileVC = SetupProfileVC.make()
        clearPresntedControllersIfNeeded(from: window)
        handlingTransition(for: window, options: .transitionFlipFromLeft)
        setRoot(to: setupProfileVC, for: window)
    }
    
    func navigateToSetupProfileDoctor(window:UIWindow) {
        let setupProfileDoctorVC = SetupProfileDoctorVC.make()
        clearPresntedControllersIfNeeded(from: window)
        handlingTransition(for: window, options: .transitionFlipFromLeft)
        setRoot(to: setupProfileDoctorVC, for: window)
    }
    
    func navigateToSetupProfileClinic(window:UIWindow) {
        let setupProfileClinicVC = SetupProfileClinicVC.make()
        clearPresntedControllersIfNeeded(from: window)
        handlingTransition(for: window, options: .transitionFlipFromLeft)
        setRoot(to: setupProfileClinicVC, for: window)
    }
    
    func navigateToSetupDrProfileUploadDocumentsVC(window:UIWindow) {
        let setupProfileUploadDocumentsVC = SetupProfileUploadDocumentsVC.make()
        clearPresntedControllersIfNeeded(from: window)
        handlingTransition(for: window, options: .transitionFlipFromLeft)
        setRoot(to: setupProfileUploadDocumentsVC, for: window)
    }
    
    func navigateToSetupProfileUploadDocumentsVC(window:UIWindow, info: SetupClinicProfileInfo, branchPhotos: [UIImage], clinicLogo: UIImage) {
        let setupProfileUploadDocumentsVC = SetupProfileUploadDocumentsVC.make()
        setupProfileUploadDocumentsVC.previousClinicInfo = info
        setupProfileUploadDocumentsVC.clinicLogo = clinicLogo
        setupProfileUploadDocumentsVC.branchImages = branchPhotos
        clearPresntedControllersIfNeeded(from: window)
        handlingTransition(for: window, options: .transitionFlipFromLeft)
        setRoot(to: setupProfileUploadDocumentsVC, for: window)
    }
    
    func navigateToAccessability(window:UIWindow) {
        let accessabilityVC = EnableLocationNC.make()
        clearPresntedControllersIfNeeded(from: window)
        handlingTransition(for: window, options: .transitionCrossDissolve)
        setRoot(to: accessabilityVC, for: window)
    }
    
    func navigateToHomeNC(window: UIWindow){
        let homeNC = HomeNC.make()
        clearPresntedControllersIfNeeded(from: window)
        handlingTransition(for: window, options: .transitionFlipFromLeft)
        setRoot(to: homeNC, for: window)
    }
    
    func navigateToEnableNotifications(window:UIWindow) {
        let enableNotificationVC = EnableNotificationsVC.make()
        clearPresntedControllersIfNeeded(from: window)
        handlingTransition(for: window, options: .transitionCrossDissolve)
        setRoot(to: enableNotificationVC, for: window)
    }
    private func clearPresntedControllersIfNeeded(from window: UIWindow){
        guard window.rootViewController?.presentedViewController != nil else {return}
        window.rootViewController?.dismiss(animated: false, completion: {
            self.clearPresntedControllersIfNeeded(from: window)
        })
    }
    
    private func setRoot(to rootViewController: UIViewController, for window: UIWindow){
        window.makeKeyAndVisible()
        window.rootViewController = rootViewController
    }
    
    private func handlingTransition(for window: UIWindow, options: UIView.AnimationOptions){
        UIView.transition(with: window, duration: 0.55001, options: options, animations: nil, completion: nil)
    }
}
