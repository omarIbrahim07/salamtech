//
//  LoadingView.swift
//  legalawy
//
//  Created by hesham ghalaab on 6/11/20.
//  Copyright © 2020 Legalawy. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoadingView: UIView {

    @IBOutlet weak private var mainView: UIView!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    @IBOutlet weak private var loadingNamedLabel: UILabel!

    var loadingName: String = "" {
        
        didSet {
            
            self.loadingNamedLabel.text = loadingName
        }
    }

    override func draw(_ rect: CGRect) {
        
        mainView.layer.cornerRadius = 16
        activityIndicator.startAnimating()
    }
}
