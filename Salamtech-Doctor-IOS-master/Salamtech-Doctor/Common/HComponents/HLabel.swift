//
//  HLabel.swift
//  NCA
//
//  Created by hesham ghalaab on 3/3/20.
//  Copyright © 2020 naqla. All rights reserved.
//

import UIKit

class HLabel: UILabel {
    
    var animationGroup: CAAnimationGroup = CAAnimationGroup()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setFont()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setFont()
    }
}

extension HLabel: Animationable {}
extension HLabel: Shadowable {}
extension HLabel: Layerable {}
extension HLabel: Fontable {}
