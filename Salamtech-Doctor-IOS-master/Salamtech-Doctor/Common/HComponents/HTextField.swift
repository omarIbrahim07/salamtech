//
//  HTextField.swift
//  NCA
//
//  Created by hesham ghalaab on 3/3/20.
//  Copyright © 2020 naqla. All rights reserved.
//

import UIKit

class HTextField: UITextField {
    
    var animationGroup: CAAnimationGroup = CAAnimationGroup()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setFont()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setFont()
    }
}

extension HTextField: Animationable {}
extension HTextField: Shadowable {}
extension HTextField: Layerable {}
extension HTextField: Fontable {}
