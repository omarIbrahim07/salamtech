//
//  HTextView.swift
//  NCA
//
//  Created by hesham ghalaab on 3/3/20.
//  Copyright © 2020 naqla. All rights reserved.
//

import UIKit

class HTextView: UITextView {
    
    var animationGroup: CAAnimationGroup = CAAnimationGroup()
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setFont()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setFont()
    }
}

extension HTextView: Animationable {}
extension HTextView: Shadowable {}
extension HTextView: Layerable {}
extension HTextView: Fontable {}
