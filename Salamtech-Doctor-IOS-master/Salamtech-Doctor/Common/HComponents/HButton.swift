//
//  HButton.swift
//  NCA
//
//  Created by hesham ghalaab on 3/3/20.
//  Copyright © 2020 naqla. All rights reserved.
//

import UIKit

class HButton: UIButton {
    var animationGroup: CAAnimationGroup = CAAnimationGroup()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setFont()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setFont()
    }
}

extension HButton: Animationable {}
extension HButton: Shadowable {}
extension HButton: Layerable {}
extension HButton: Fontable {}
