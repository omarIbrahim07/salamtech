//
//  HView.swift
//  Unigate
//
//  Created by hesham ghalaab on 4/27/20.
//  Copyright © 2020 Z-Novation. All rights reserved.
//

import UIKit

class HView: UIView {
    var animationGroup: CAAnimationGroup = CAAnimationGroup()
}

extension HView: Animationable {}
extension HView: Shadowable { }
extension HView: Layerable { }
