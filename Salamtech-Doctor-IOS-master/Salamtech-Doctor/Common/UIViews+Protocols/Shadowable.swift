//
//  Shadowable.swift
//  NCA
//
//  Created by hesham ghalaab on 10/29/19.
//  Copyright © 2019 naqla. All rights reserved.
//

import UIKit

protocol Shadowable { }

extension Shadowable where Self: UIView{
    /**
     This function is to Adding shadow to the UIView..
     - Parameter color: is the shadow color , it's default value is black.
     - Parameter offset: is the shadow offset , it's default value is zero.
     - Parameter opacity: is the shadow opacity , it's  default value is 0.2.
     - Parameter radius: is the shadow radius , it's default value is 10.
     - Parameter cornerRadius: it's default value is 8.
     - Returns: It returns void.
     */
    
    func addShadow(withColor color: UIColor = UIColor.black,
                   offset: CGSize = CGSize.zero,
                   opacity: Float = 0.2,
                   radius: CGFloat = 10,
                   cornerRadius: CGFloat = 0){
        layer.masksToBounds  = false
        
        if layer.shadowColor != color.cgColor{
            layer.shadowColor = color.cgColor
        }
        
        if layer.shadowOffset != offset{
            layer.shadowOffset = offset
        }
        
        if layer.shadowOpacity != opacity{
            layer.shadowOpacity = opacity
        }
        
        if layer.shadowRadius != radius{
            layer.shadowRadius = radius
        }
        
        if layer.cornerRadius != cornerRadius{
            layer.cornerRadius = cornerRadius
        }
    }
}

