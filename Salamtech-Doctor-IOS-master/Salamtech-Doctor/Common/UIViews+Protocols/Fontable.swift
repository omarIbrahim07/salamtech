//
//  Fontable.swift
//  NCA
//
//  Created by hesham ghalaab on 3/3/20.
//  Copyright © 2020 naqla. All rights reserved.
//

import UIKit

protocol Fontable {
    func setFont()
}

extension Fontable where Self: UILabel{
    func setFont(){
        let face = font.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.face) as! String
        let size = font.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.size) as! CGFloat
        let thefont = Font(Font.Wight(value: face), size: size).instance
        self.font = thefont
    }
}
extension Fontable where Self: UIButton{
    func setFont(){
        guard let font = titleLabel?.font else{return}
        let face = font.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.face) as! String
        let size = font.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.size) as! CGFloat
        let thefont = Font(Font.Wight(value: face), size: size).instance
        self.titleLabel?.font = thefont
    }
}
extension Fontable where Self: UITextField{
    func setFont(){
        let face = font?.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.face) as! String
        let size = font?.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.size) as! CGFloat
        let thefont = Font(Font.Wight(value: face), size: size).instance
        self.font = thefont
    }
}
extension Fontable where Self: UITextView{
    func setFont(){
        let face = font?.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.face) as! String
        let size = font?.fontDescriptor.object(forKey: UIFontDescriptor.AttributeName.size) as! CGFloat
        let thefont = Font(Font.Wight(value: face), size: size).instance
        self.font = thefont
    }
}

struct Font {
    enum Wight {
        case light
        case regular
        case medium
        case bold
        
        init(value: String) {
            switch value{
            case "Light":   self = .light
            case "Regular": self = .regular
            case "Medium":  self = .medium
            case "Bold":    self = .bold
            case "Semibold":self = .bold
            default:
                self = .regular
                print("Font is Not handled")
            }
        }
    }
    
    var wight: Wight
    var size: CGFloat
    
    init(_ wight: Wight, size: CGFloat) {
        self.wight = wight
        self.size = size
    }
}

extension Font {
    var instance: UIFont {
        switch wight {
        case .light:
            guard let font =  UIFont(name: lightName, size: size) else {
                fatalError("fontname font is not installed")
            }
            return font
        case .regular:
            guard let font =  UIFont(name: regularName, size: size) else {
                fatalError("fontname font is not installed")
            }
            return font
        case .medium:
            guard let font =  UIFont(name: mediumName, size: size) else {
                fatalError("fontname font is not installed")
            }
            return font
        case .bold:
            guard let font =  UIFont(name: boldName, size: size) else {
                fatalError("fontname font is not installed")
            }
            return font
        }
    }
    
    //private var language: Languages{
        //return LanguageManger.shared.currentLanguage
    //}
    private var lightName: String {
        return "HelveticaNeue-Light"
        //return language == .en ? "HelveticaNeue-Light": "HelveticaNeue-Light"
    }
    private var regularName: String {
        return "HelveticaNeue"
        //return language == .en ? "HelveticaNeue": "HelveticaNeue"
    }
    private var mediumName: String {
        return "HelveticaNeue-Medium"
        //return language == .en ? "HelveticaNeue-Medium": "HelveticaNeue-Medium"
    }
    private var boldName: String {
        return "HelveticaNeue-Bold"
        //return language == .en ? "HelveticaNeue-Bold": "HelveticaNeue-Bold"
    }
    
}




/*
 The available Fonts For Helvetica Neue Family:
 HelveticaNeue-UltraLightItalic
 HelveticaNeue-Medium
 HelveticaNeue-MediumItalic
 HelveticaNeue-UltraLight
 HelveticaNeue-Italic
 HelveticaNeue-Light
 HelveticaNeue-ThinItalic
 HelveticaNeue-LightItalic
 HelveticaNeue-Bold
 HelveticaNeue-Thin
 HelveticaNeue-CondensedBlack
 HelveticaNeue
 HelveticaNeue-CondensedBold
 HelveticaNeue-BoldItalic
*/
