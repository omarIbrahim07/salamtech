//
//  Animationable.swift
//  NCA
//
//  Created by hesham ghalaab on 10/29/19.
//  Copyright © 2019 naqla. All rights reserved.
//

import UIKit

public enum AnimationDirection {
    case right
    case left
    case top
    case bottom
}

/// CABasicAnimation key paths.
public enum AnimationKey: String {
    case shadowOpacity = "shadowOpacity"
    case yPosition = "position.y"
    case xPosition = "position.x"
    case opacity = "opacity"
    case none
    
    init(_ key: String) {
        switch key{
        case "shadowOpacity": self = .shadowOpacity
        case "opacity":       self = .opacity
        case "position.y":    self = .yPosition
        case "position.x":    self = .xPosition
        default:              self = .none
        }
    }
}

protocol Animationable: CAAnimationDelegate {
    var animationGroup: CAAnimationGroup {get set}
}

extension Animationable where Self: UIView{
    
    /**
     This function need to be called after prepareToAnimate() and after adding animations in order to make the animation.
     - Parameter key: Used if you need to add a specific key for the animation.
     */
    func startAnimation(forKey key: String = "groupOfAnimations"){
        layer.removeAllAnimations()
        layer.add(animationGroup, forKey: key)
        setupLayerAfterAnimation()
    }
    
    /**
     After adding the CAAnimation to the layer we should add all the values to the layer in order to make it the new values.
     */
    private func setupLayerAfterAnimation(){
        guard let animations = animationGroup.animations as? [CABasicAnimation] else { return }
        for animation in animations{
            
            let keyPath = animation.keyPath ?? ""
            let animationKey = AnimationKey(keyPath)
            switch animationKey{
            case .opacity:
                layer.opacity = animation.toValue as! Float
            case .xPosition:
                layer.position.x = animation.toValue as! CGFloat
            case .yPosition:
                layer.position.y = animation.toValue as! CGFloat
            case .shadowOpacity:
                layer.shadowOpacity = animation.toValue as! Float
            case .none: break
            }
        }
    }
    
    /**
     This function is to append a new position animation to CAAnimationGroup object in order to make multiple animation at the same time..
     - Parameter to: Direction of the animation, we user it for making the calculations of the new position..
     - Parameter value: Positive value to change the position with.
     - Parameter duration: Animation duration , default is 1
     */
    func addPositionAnimation(to: AnimationDirection, with value: CGFloat, duration: CFTimeInterval = 1){
        let position = layer.position
        switch to {
        case .top:
            let toValue = position.y - value
            add(for: .yPosition, from: layer.position.y, to: toValue, with: duration)
        case .bottom:
            let toValue = position.y + value
            add(for: .yPosition, from: layer.position.y, to: toValue, with: duration)
            
        case .left:
            let toValue = position.x - value
            add(for: .xPosition, from: layer.position.x, to: toValue, with: duration)
        case .right:
            let toValue = position.x + value
            add(for: .xPosition, from: layer.position.x, to: toValue, with: duration)
        }
    }
    
    func initPosition(to: AnimationDirection, with value: CGFloat){
        let position = layer.position
        switch to {
        case .top:
            let toValue = position.y - value
            layer.position.y = toValue
        case .bottom:
            let toValue = position.y + value
            layer.position.y = toValue
            
        case .left:
            let toValue = position.x - value
            layer.position.x = toValue
        case .right:
            let toValue = position.x + value
            layer.position.x = toValue
        }
    }
    
    /**
    This function is to append a new fade animation to CAAnimationGroup object in order to make multiple animation at the same time..
    - Parameter from: opacity value in which we need to start the animation with.
    - Parameter to: opacity value in which we need to end the animation with.
    - Parameter duration: Animation duration , default is 1
    */
    func addFadeAnimation(from: Float, to: Float, and duration: CFTimeInterval = 1){
        add(for: .opacity, from: from, to: to, with: duration)
    }
    
    /**
    This function is to append a new shadow opacity animation to CAAnimationGroup object in order to make multiple animation at the same time..
    - Parameter value: opacity value in which we need to start the animation with.
    - Parameter duration: Animation duration , default is 1
    */
    func addShadowOpacityAnimation(with value: Float, with duration: CFTimeInterval = 1){
        add(for: .shadowOpacity, from: layer.shadowOpacity, to: value, with: duration)
    }
    
    func removeAllAnimationsIfNeeded(){
        animationGroup.animations?.removeAll()
    }
}

extension Animationable where Self: UIView{
    
    /**
     Adding a new CABasicAnimation in animationGroup
     - Parameter key: A key path for the animation.
     - Parameter from: Any Value.
     - Parameter to: Any Value.
     - Parameter duration: Animation duration.
     - Returns: It returns void.
     */
    func add(for key: AnimationKey, from: Any, to: Any, by: Any? = nil, with duration: CFTimeInterval){
        let animation = createAnimation(for: key, from: from, to: to, by: by, with: duration)
        prepareGroup(by: animation)
        append(animation)
    }
    
    private func createAnimation(for key: AnimationKey, from: Any, to: Any, by: Any?, with duration: CFTimeInterval) -> CABasicAnimation{
        let animation = CABasicAnimation(keyPath: key.rawValue)
        animation.fromValue = from
        animation.toValue = to
        animation.byValue = by
        animation.duration = duration
        return animation
    }

    private func prepareGroup(by animation: CABasicAnimation){
        animationGroup.duration = maxDuration(with: animation)
    }
    
    private func maxDuration(with basicAnimation: CABasicAnimation) -> CFTimeInterval{
        if basicAnimation.duration > animationGroup.duration{
            return basicAnimation.duration
        }
        return animationGroup.duration
    }

    private func append(_ animation: CABasicAnimation){
        guard animationGroup.animations == nil else {
            animationGroup.animations?.append(animation)
            return
        }
        animationGroup.animations = [animation]
    }
}

