//
//  Layerable.swift
//  NCA
//
//  Created by hesham ghalaab on 10/29/19.
//  Copyright © 2019 naqla. All rights reserved.
//

import UIKit

protocol Layerable { }

extension Layerable where Self: UIView{
    var layerBackGroundColor: CGColor?{
        get{ return layer.backgroundColor }
        set{ layer.backgroundColor = newValue }
    }
}

