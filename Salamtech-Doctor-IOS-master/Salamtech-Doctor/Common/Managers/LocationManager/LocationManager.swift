//
//  LocationManager.swift
//  salamtech
//
//  Created by hesham ghalaab on 5/28/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import UIKit
import MapKit

protocol LocationManagerDelegate {
//    func didGet(userLocation: UserLocation)
    func didChangeAuthorization(_ status: CLAuthorizationStatus)
}

struct UserLocation {
    var address: String
    var region: String
    var city: String
    var longitude: Double
    var latitude: Double
    var fullAddress: String
}

class LocationManager: NSObject {
    
    private var locationManager = CLLocationManager()
    var delegate: LocationManagerDelegate!
    
    init(_ viewController: UIViewController) {
        super.init()
        self.delegate = viewController as? LocationManagerDelegate
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func requestAuthorization(){
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    private func getFullAddress(from location: CLLocation, completion: @escaping (_ userLocation: UserLocation?, _ error: String?) -> () ){
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            if let error = error{
                let theError = "reverse geodcode fail: \(error.localizedDescription)"
                print(theError)
                completion(nil, theError)
                return
            }
            
            guard let placemarks = placemarks , let placeMark = placemarks.first else {
                let theError = "No placemark found."
                print(theError)
                completion(nil, theError)
                return
            }
            
            
            //print(placeMark.country ?? "")
            //print(placeMark.locality ?? "")
            //print(placeMark.subLocality ?? "")
            //print(placeMark.thoroughfare ?? "")
            //print(placeMark.postalCode ?? "")
            //print(placeMark.subThoroughfare ?? "")
            var fullAddress : String = ""
            if placeMark.subLocality != nil {
                fullAddress += placeMark.subLocality! + ", "
            }
            if placeMark.name != nil {
                fullAddress += placeMark.name! + ", "
            }
            if placeMark.locality != nil {
                fullAddress += placeMark.locality! + ", "
            }
            if placeMark.country != nil {
                fullAddress += placeMark.country!
            }

            let region = (placeMark.locality ?? "") + " " + (placeMark.subLocality ?? "")
            let lat = location.coordinate.latitude
            let long = location.coordinate.longitude
            
            let userLocation = UserLocation(address: placeMark.name ?? "", region: region, city: placeMark.administrativeArea ?? "", longitude: long, latitude: lat, fullAddress: fullAddress)
            //print(userLocation)
            completion(userLocation, nil)
        }
    }
}

extension LocationManager: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
        guard let location = locations.last else {return}
        
        getFullAddress(from: location) { (userLocation, error) in
            if let _ = error {
                return
            }
            if let userLocation = userLocation{
//                self.delegate.didGet(userLocation: userLocation)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        delegate.didChangeAuthorization(status)
    }
}

