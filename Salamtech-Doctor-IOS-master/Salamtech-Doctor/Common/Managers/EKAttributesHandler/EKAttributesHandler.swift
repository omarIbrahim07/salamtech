//
//  EKAttributesHandler.swift
//  salamtech
//
//  Created by hesham ghalaab on 5/22/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import UIKit
import SwiftEntryKit

/// Handling SwiftEntryKit EKAttributes
protocol EKAttributesHandler {
    
    func getBottomAlertFloatAttributes() -> EKAttributes
    func getTopFloatAttributes() -> EKAttributes
    func getCalenderAttributes() -> EKAttributes
}

extension EKAttributesHandler {
    
    func getBottomAlertFloatAttributes() -> EKAttributes {
        
        var attributes: EKAttributes
        attributes = .bottomFloat
        attributes.displayMode = .dark
        attributes.hapticFeedbackType = .success
        attributes.displayDuration = .infinity
        attributes.entryBackground = .clear
        attributes.screenBackground = .color(color: EKColor.black.with(alpha: 0.38))
        attributes.screenInteraction = .dismiss
        attributes.entryInteraction = .absorbTouches
        attributes.entranceAnimation = .init(translate: .init(duration: 0.5, spring: .init(damping: 0.9, initialVelocity: 0)), scale: .init(from: 0.8, to: 1, duration: 0.5, spring: .init(damping: 0.8, initialVelocity: 0)), fade: .init(from: 0.7, to: 1, duration: 0.3))
        attributes.exitAnimation = .init(translate: .init(duration: 0.5), scale: .init(from: 1, to: 0.8, duration: 0.5), fade: .init(from: 1, to: 0, duration: 0.5))
        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.8, duration: 0.3)))
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.3, radius: 6))
        attributes.positionConstraints.verticalOffset = 0
        attributes.positionConstraints.size = .init(width: .fill, height: .intrinsic)
        attributes.positionConstraints.maxSize = .init(width: .fill, height: .intrinsic)
        attributes.statusBar = .dark
        return attributes
    }
    
    func getTopFloatAttributes() -> EKAttributes{
           
           var attributes: EKAttributes
           attributes = .topFloat
           attributes.displayMode = .dark
           attributes.hapticFeedbackType = .success
           attributes.displayDuration = 3
           attributes.entryBackground = .clear
           attributes.screenBackground = .clear
           attributes.screenInteraction = .dismiss
           attributes.entryInteraction = .absorbTouches
           attributes.entranceAnimation = .init(translate: .init(duration: 0.5, spring: .init(damping: 0.9, initialVelocity: 0)), scale: .init(from: 0.8, to: 1, duration: 0.5, spring: .init(damping: 0.8, initialVelocity: 0)), fade: .init(from: 0.7, to: 1, duration: 0.3))
        attributes.exitAnimation = .init(translate: .init(duration: 0.5), scale: .init(from: 1, to: 0.8, duration: 0.5), fade: .init(from: 1, to: 0, duration: 0.5))
        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.8, duration: 0.3)))
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.3, radius: 6))
        attributes.positionConstraints.verticalOffset = 10
        attributes.positionConstraints.size = .init(width: .offset(value: 4), height: .intrinsic)
        attributes.positionConstraints.maxSize = .init(width: .offset(value: 4), height: .intrinsic)
        attributes.statusBar = .ignored
        return attributes
    }
    func getCalenderAttributes() -> EKAttributes {
        var attributes: EKAttributes
        attributes = .topFloat
        attributes.displayMode = .dark
        attributes.hapticFeedbackType = .success
        attributes.displayDuration = .infinity
        attributes.windowLevel = .normal
        attributes.screenBackground = .color(color: EKColor.black.with(alpha: 0.38))
        attributes.screenInteraction = .dismiss
        attributes.entryInteraction = .absorbTouches
        attributes.entranceAnimation = .init(translate: .init(duration: 0.5, spring: .init(damping: 0.9, initialVelocity: 0)), scale: .init(from: 0.8, to: 1, duration: 0.5, spring: .init(damping: 0.8, initialVelocity: 0)), fade: .init(from: 0.7, to: 1, duration: 0.3))
        attributes.exitAnimation = .init(translate: .init(duration: 0.5), scale: .init(from: 1, to: 0.8, duration: 0.5), fade: .init(from: 1, to: 0, duration: 0.5))
        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.8, duration: 0.3)))
        attributes.positionConstraints.verticalOffset = 0
        attributes.positionConstraints.size = .init(width: .offset(value: 0), height: .offset(value: 0))
        attributes.positionConstraints.maxSize = .init(width: .offset(value: 0), height: .offset(value: 0))
        attributes.statusBar = .hidden
        return attributes
    }
}

extension CGRect {
    
    var minEdge: CGFloat {
        
        return min(width, height)
    }
}

extension UIScreen {
    
    var minEdge: CGFloat {
        
        return UIScreen.main.bounds.minEdge
    }
}

