//
//  Account+Defaults.swift
//  Unigate
//
//  Created by hesham ghalaab on 3/27/20.
//  Copyright © 2020 Z-Novation. All rights reserved.
//

import Foundation

extension UserDefaults {
    struct Account : KeyDefaultable, KeyNameSpaceable {
        private init() { }
        
        /// Key Descripion
        enum Key: String, CaseIterable{
            /// String value
            case token
            /// User object
            case user
            
            case setupProfileStatus
            
            case locationLat
            
            case locationLong
            
            case noOfAppointments
            
            case selectedCalendarDate
            
            case selectedCalendarDay
        }
        
        ///Clear the Parent defaults in case User is logged out.
        static func clear(){
            for key in Key.allCases{
                let item = namespace(key)
                UserDefaults.standard.removeObject(forKey: item)
            }
            UserDefaults.standard.synchronize()
        }
        
        static func clear(for key: Key){
            let item = namespace(key)
            UserDefaults.standard.removeObject(forKey: item)
            UserDefaults.standard.synchronize()
        }
    }
}

