//
//  DefaultsHandler.swift
//  Unigate
//
//  Created by hesham ghalaab on 3/27/20.
//  Copyright © 2020 Z-Novation. All rights reserved.
//

import Foundation

protocol KeyDefaultable: KeyNameSpaceable {
    associatedtype Key : RawRepresentable
}

extension KeyDefaultable where Key.RawValue == String{
    
    // MARK: - Object Defaults
    static func set(_ object: Any, forKey key: Key) {
        let key = namespace(key)
        UserDefaults.standard.set(object, forKey: key)
    }
    static func object(forKey key: Key) -> Any? {
        let key = namespace(key)
        return UserDefaults.standard.object(forKey: key)
    }
    
    // MARK: - Integer Defaults
    static func set(_ integer: Int, forKey key: Key) {
        let key = namespace(key)
        UserDefaults.standard.set(integer, forKey: key)
    }
    static func integer(forKey key: Key) -> Int {
        let key = namespace(key)
        return UserDefaults.standard.integer(forKey: key)
    }
    // MARK: - String Defaults
    static func set(_ string:String, forKey key:Key) {
        let key = namespace(key)
        UserDefaults.standard.set(string, forKey: key)
    }
    static func string(forKey key:Key) -> String? {
        let key = namespace(key)
        return UserDefaults.standard.string(forKey: key)
    }
    // MARK: - Double Defaults
    static func set(_ double: Double, forKey key: Key) {
        let key = namespace(key)
        UserDefaults.standard.set(double, forKey: key)
    }
    static func double(forKey key: Key) -> Double {
        let key = namespace(key)
        return UserDefaults.standard.double(forKey: key)
    }
    
    // MARK: - Float Defaults
    static func set(_ float: Float, forKey key: Key) {
        let key = namespace(key)
        UserDefaults.standard.set(float, forKey: key)
    }
    static func float(forKey key: Key) -> Float {
        let key = namespace(key)
        return UserDefaults.standard.float(forKey: key)
    }
    
    // MARK: - Bool Defaults
    static func set(_ bool: Bool, forKey key: Key) {
        let key = namespace(key)
        UserDefaults.standard.set(bool, forKey: key)
    }
    static func bool(forKey key: Key) -> Bool {
        let key = namespace(key)
        return UserDefaults.standard.bool(forKey: key)
    }
    
    // MARK: - URL Defaults
    static func set(_ url: URL, forKey key: Key) {
        let key = namespace(key)
        UserDefaults.standard.set(url, forKey: key)
    }
    static func url(forKey key: Key) -> URL? {
        let key = namespace(key)
        return UserDefaults.standard.url(forKey: key)
    }
    
    static func clear(keys: [Key]){
        for item in keys{
            let key = namespace(item)
            UserDefaults.standard.removeObject(forKey: key)
        }
        UserDefaults.standard.synchronize()
    }
    
    static func clear(for key: Key){
        let key = namespace(key)
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    static func set<T: Codable>(value: T, key: Key){
        let key = namespace(key)
        do {
            let data = try JSONEncoder().encode(value)
            UserDefaults.standard.set(data, forKey: key)
        }catch{
            fatalError("cannot encode \(value), for key: \(key)")
        }
    }
    
    static func get<T: Codable>(forKey key: Key) -> T?{
        let key = namespace(key)
        guard let data = UserDefaults.standard.object(forKey: key) as? Data else{
            return nil }
        do {
            let details = try JSONDecoder().decode(T.self, from: data)
            return details
        }catch{
            fatalError("cannot decode key: \(key)")
        }
    }
}

// MARK: - Key Namespaceable
protocol KeyNameSpaceable { }

extension KeyNameSpaceable {
    private static func namespace(_ key: String) -> String {
        return "\(Self.self).\(key)"
    }
    
    static func namespace<T: RawRepresentable>(_ key: T) -> String where T.RawValue == String {
        return namespace(key.rawValue)
    }
    /*
     BEFORE KeyNameSpaceable
     let account = Account.BoolDefaultKey.isUserLoggedIn.rawValue
     let default = UserDefaults.BoolDefaultKey.isUserLoggedIn.rawValue
     account == default -> "isUserLoggedIn" == "isUserLoggedIn"
     let account = namespace(Account.BoolDefaultKey.isUserLoggedIn)
     let default = namespace(UserDefaults.BoolDefaultKey.isUserLoggedIn)
     account != default -> "Account.isUserLoggedIn" != "UserDefaults.isUserLoggedIn"
     */
}

