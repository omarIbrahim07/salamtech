//
//  StoryboardNames.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/17/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation

enum Storyboard: String {
    case authenticationSB = "AuthenticationSB"
    case codeVerificationSB = "CodeVerificationSB"
    case workClinicInfoSB = "WorkClinicInfoSB"
    case generalInfoSB = "GeneralInfoSB"
    case profileSB = "ProfileSB"
    case editWorkClinicInfoSB = "EditWorkClinicInfoSB"
    case editGeneralInfoSB = "EditGeneralInfoSB"
    case splashSB = "SplashSB"
    case searchSB = "SearchSB"
    case loginSB = "LoginSB"
    case signUpSB = "SignUpSB"
    case sideMenuSB = "SideMenuSB"
    case homeSB = "HomeSB"
    case consultationSB = "ConsultationSB"
    case burgerSideMenuSB = "BurgerSideMenu"
    case onBoardingSB = "OnBoardingSB"
    case accessabilitySB = "AccessabilitySB"
    case chooseLangSB = "ChooseLangSB"
    case setupProfileSB = "SetupProfileSB"
}

