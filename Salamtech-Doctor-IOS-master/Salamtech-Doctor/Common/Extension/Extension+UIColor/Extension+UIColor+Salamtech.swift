//
//  Extension+UIColor+Unigate.swift
//  Unigate
//
//  Created by hesham ghalaab on 3/27/20.
//  Copyright © 2020 Z-Novation. All rights reserved.
//

import UIKit

extension UIColor{
    
    /// defaults colors used in as a common in all of the projects.
    public struct SALAMTECH{
        static let blue      = UIColor(named: "Blue")
        static let skyBlue   = UIColor(named: "SkyBlue")
        static let darkGray  = UIColor(named: "DarkGrey")
        static let lightGray = UIColor(named: "LightGrey")
        
    }
}
   
