//
//  UINavigationBar+Extensions.swift
//  GameOn
//
//  Created by Omar on 2/3/18.
//  Copyright © 2018 Omar. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    func adjustDefaultNavigationBar() {
//        self.barTintColor = mainBlueColor
        self.barTintColor = #colorLiteral(red: 0.3607843137, green: 0.7764705882, blue: 0.7294117647, alpha: 1)
        
        self.setBackgroundImage(UIImage(), for: .default)
//        self.backgroundColor = mainBlueColor
        self.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        self.tintColor = UIColor.white

        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.titleTextAttributes = (titleDict as! [NSAttributedString.Key : Any])
        
        self.isTranslucent = false
        self.shadowImage = UIImage()
    }
}
