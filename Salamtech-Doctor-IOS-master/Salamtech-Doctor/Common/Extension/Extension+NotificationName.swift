//
//  Extension+NotificationName.swift
//  Unigate
//
//  Created by hesham ghalaab on 4/4/20.
//  Copyright © 2020 Z-Novation. All rights reserved.
//

import UIKit

extension NSNotification.Name {
    public static let openMenu = Notification.Name("openMenu")
    public static let selectBarItem = Notification.Name("selectBarItem")
    public static let deletedBookedAppointment = Notification.Name("deletedBookedAppointment")
}
