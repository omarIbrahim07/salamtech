//
//  Extension + Date.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/13/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
extension Date {
    var weekday: Int { Calendar.current.component(.weekday, from: self)}
    var firstDayOfTheMonth: Date {
        Calendar.current.dateComponents([.calendar, .year,.month], from: self).date! }
    
    
    var currentMonthAndYear: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM"
        return dateFormatter.string(from: self)
    }

    func convertFromTimeStamp(format: String? = "h:mm a") -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_AU")
        dateFormatter.timeZone = TimeZone(abbreviation: "Africa/Cairo")
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }

}

class Dates {
    static func printDatesBetweenInterval(_ startDate: Date, _ endDate: Date) -> [String] {
        var dates = [String]()
        var startDate = startDate
        let calendar = Calendar.current

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"

        while startDate <= endDate {
            dates.append(dateFormatter.string(from: startDate))
            startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
        }
        return dates
    }

    static func dateFromString(_ dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: dateString)!
    }
}
