
//
//  File.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 9/6/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
extension RangeReplaceableCollection where Element: Hashable {
    var orderedSet: Self {
        var set = Set<Element>()
        return filter { set.insert($0).inserted }
    }
    mutating func removeDuplicates() {
        var set = Set<Element>()
        removeAll { !set.insert($0).inserted }
    }
}
