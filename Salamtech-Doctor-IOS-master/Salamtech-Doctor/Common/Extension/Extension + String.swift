//
//  Extension + String.swift
//  Salamtech-Doctor
//
//  Created by Ahmed on 8/30/20.
//  Copyright © 2020 Ahmed. All rights reserved.
//

import Foundation
extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    func isValidPassword() -> Bool {
        count < 6 ? false:true
    }
}

extension String {
    var localized:String {
        return NSLocalizedString(self, comment: "")
    }
}
extension String {
    func getSubString(startIndex:Int,endIndex:Int) -> Substring {
        let start = self.index(self.startIndex, offsetBy: startIndex)
        let end = self.index(self.startIndex, offsetBy: endIndex)
        let range = start..<end
        let mySubString = self[range]
        return mySubString
    }
}

extension String {
    func convertDateFormat(format:String) -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let date = df.date(from: self)
        df.dateFormat = format
        let dateInString = df.string(from: date!)
        return dateInString
    }
}
