//
//  Extension+UIViewController.swift
//  Unigate
//
//  Created by hesham ghalaab on 3/27/20.
//  Copyright © 2020 Z-Novation. All rights reserved.
//

import UIKit
import SwiftEntryKit
import DropDown

extension UIViewController: EKAttributesHandler{
    
    var isModal: Bool {
        if let count = self.navigationController?.viewControllers.count, count > 0 {
            if self.navigationController?.viewControllers[0] != self {
                return false
            }
        }
        let presentingIsModal = presentingViewController != nil
        let presentingIsNavigation = navigationController?.presentingViewController?.presentedViewController == navigationController
        let presentingIsTabBar = tabBarController?.presentingViewController is UITabBarController
        
        return presentingIsModal || presentingIsNavigation || presentingIsTabBar
    }

    func configureNavigationBarWithCloseButton() {
        self.navigationController?.navigationBar.adjustDefaultNavigationBar()
        if isModal {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "CloseIcon"), style: .plain, target: self, action: #selector(exitAction))
        }
    }
    
    @objc fileprivate func exitAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func alert(title: String, message: String){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: { (action: UIAlertAction!) in
                // Handle Cancel Logic here
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func alert(title: String, message: String, actionTitle: String = "Ok", handler: @escaping () -> Void){
        let okAction = UIAlertAction(title: actionTitle, style: .default) { _ in handler() }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func isNavigationBarShadowHidden(_ isHidden: Bool) {
        
        var image: UIImage? = UIImage()
        if !isHidden { image = nil }
        navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        navigationController?.navigationBar.shadowImage = image
    }
    @discardableResult func showDropDown(dataSource:[String],onView:UIView) -> DropDown {
        let dropDown = DropDown()
        dropDown.anchorView = onView
        dropDown.direction = .bottom
        dropDown.dataSource = dataSource
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        return dropDown
    }
    func showDatePicker(mode:UIDatePicker.Mode) -> DatePickerView {
           let datePicker = DatePickerView(mode: mode)
           SwiftEntryKit.display(entry: datePicker, using: getBottomAlertFloatAttributes())
           return datePicker
       }
    func bottomAlert(title: String, message: String, actionTitle: String = "Ok", onCloseTapped: (() -> Void)? = nil) -> BottomAlert{
        
        let alert = BottomAlert(title: title, message: message, actionTitle: actionTitle, onCloseTapped: {
            SwiftEntryKit.dismiss()
            onCloseTapped?()
        })
        
        SwiftEntryKit.display(entry: alert, using: getBottomAlertFloatAttributes())
        return alert
    }
    func rateChat(onCloseTapped:(() -> Void)? = nil , onTapLike:(() -> Void)? = nil,onTapDisLike:(() -> Void)? = nil ) -> RateChat {
        let alert = RateChat(onTapLike: {
            onTapLike?()
        }, onTapDisLike: {
            onTapDisLike?()
        }) {
            SwiftEntryKit.dismiss()
            onCloseTapped?()
        }
        SwiftEntryKit.display(entry: alert, using: getBottomAlertFloatAttributes())
        return alert
    }
    func rateWithCosmos(onTapNext:(() -> Void)? = nil , onTapClose:(() -> Void)? = nil) -> RateCosmos {
        let alert = RateCosmos(onTapNext: {
            onTapNext?()
        }) {
            SwiftEntryKit.dismiss()
            onTapClose?()
        }
        SwiftEntryKit.display(entry: alert, using: getBottomAlertFloatAttributes())
        return alert
    }
    func showCalender(view:UIView,viewController:HomeVC) -> CalenderView {
        let alert = CalenderView()
        alert.delegate = viewController
        SwiftEntryKit.display(entry: alert, using: getCalenderAttributes())
        return alert
    }
    func showSearch() -> SearchVC {
        let search = SearchVC()
        SwiftEntryKit.display(entry: search, using: getCalenderAttributes())
        return search
    }
    func convertTimeFormat(time:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        
        let date = dateFormatter.date(from: time)
        dateFormatter.dateFormat = "HH:mm"
        
        let Date24 = dateFormatter.string(from: date!)
        return Date24
    }
    func downloadImage(downloadableImage:String,completion:@escaping (UIImage?) -> () ) {
        showLoading()
        var image:UIImage?
        guard let url = URL(string: downloadableImage) else {
            self.hideLoading()
            return
        }
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf:url ) {
                image = UIImage(data: data)
                DispatchQueue.main.async {
                    completion(image)
                    self.hideLoading()
                }
            }
        }
    }
    func errorToast(with title: String) {

        var attributes: EKAttributes
        attributes = .topNote
        attributes.displayMode = .light
        attributes.name = "Top Note"
        attributes.hapticFeedbackType = .error
        attributes.popBehavior = .animated(animation: .translation)
        attributes.entryBackground = .color(color: EKColor.init(red: 255, green: 0, blue: 0))
        attributes.shadow = .active(with: .init(color: EKColor.init(red: 48, green: 47, blue: 48), opacity: 0.5, radius: 2))
        attributes.statusBar = .light
        let style = EKProperty.LabelStyle(font: UIFont.boldSystemFont(ofSize: 14), color: .white, alignment: .center)
        let labelContent = EKProperty.LabelContent(text: title, style: style)
        let contentView = EKNoteMessageView(with: labelContent)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    func successToast(with title: String) {

        var attributes: EKAttributes
        attributes = .topNote
        attributes.displayMode = .light
        attributes.name = "Top Note"
        attributes.hapticFeedbackType = .success
        attributes.popBehavior = .animated(animation: .translation)
        attributes.entryBackground = .color(color: EKColor.init(red: 0, green: 143, blue: 0))
        attributes.shadow = .active(with: .init(color: EKColor.init(red: 48, green: 47, blue: 48), opacity: 0.5, radius: 2))
        attributes.statusBar = .light
        let style = EKProperty.LabelStyle(font: UIFont.boldSystemFont(ofSize: 14), color: .white, alignment: .center)
        let labelContent = EKProperty.LabelContent(text: title, style: style)
        let contentView = EKNoteMessageView(with: labelContent)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
}



extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        if let topViewController = presentedViewController{
            return topViewController.preferredStatusBarStyle
        }
        if let topViewController = viewControllers.last {
            return topViewController.preferredStatusBarStyle
        }
        
        return .default
    }
}

extension UIViewController {
    func getDayOfWeek(_ today:String) -> String? {
         let formatter  = DateFormatter()
         formatter.dateFormat = "dd-MM-yyyy"
         guard let todayDate = formatter.date(from: today) else { return nil }
         let myCalendar = Calendar(identifier: .gregorian)
         let weekDay = myCalendar.component(.weekday, from: todayDate)
         switch weekDay {
         case 1: return "Sunday"
         case 2: return "Monday"
         case 3: return "Tuesday"
         case 4: return "Wednesday"
         case 5: return "Thursday"
         case 6: return "Friday"
         case 7: return "Saturday"
         default: return ""
         }
     }
}


