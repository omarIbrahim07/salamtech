//
//  Extension+TableView.swift
//  salamtech
//
//  Created by Ahmed on 8/12/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import Foundation
import UIKit
class SelfSizedTableView: UITableView {
    override var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}


class SelfSizingTableView: UITableView {
  var maxHeight: CGFloat = UIScreen.main.bounds.size.height
  
  override func reloadData() {
    super.reloadData()
    self.invalidateIntrinsicContentSize()
    self.layoutIfNeeded()
  }
  
  override var intrinsicContentSize: CGSize {
    let height = min(contentSize.height, maxHeight)
    return CGSize(width: contentSize.width, height: height)
  }
}
extension UITableViewCell {
    func getDayOfWeek(_ today:String) -> String? {
         let formatter  = DateFormatter()
         formatter.dateFormat = "dd-MM-yyyy"
         guard let todayDate = formatter.date(from: today) else { return nil }
         let myCalendar = Calendar(identifier: .gregorian)
         let weekDay = myCalendar.component(.weekday, from: todayDate)
         switch weekDay {
         case 1: return "Sunday"
         case 2: return "Monday"
         case 3: return "Tuesday"
         case 4: return "Wednesday"
         case 5: return "Thursday"
         case 6: return "Friday"
         case 7: return "Saturday"
         default: return ""
         }
     }
}
