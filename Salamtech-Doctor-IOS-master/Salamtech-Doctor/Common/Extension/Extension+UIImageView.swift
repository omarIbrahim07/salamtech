//
//  Extension+UIImageView.swift
//  salamtech
//
//  Created by Ahmed on 8/11/20.
//  Copyright © 2020 hesham ghalaab. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension UIImageView {
    func addImage(withImage _theImage:String?,andPlaceHolder holder:String) {
        guard !holder.isEmpty else {
            guard let theImage = _theImage , let imageURL = URL(string: theImage) else {return}
            self.sd_setImage(with: imageURL, completed: nil)
            return
        }
        
        guard let placeHolder = UIImage(named: holder) else {
            guard let theImage = _theImage , let imageURL = URL(string: theImage) else {return}
            self.sd_setImage(with: imageURL, completed: nil)
            return
        }
        
        guard let theImage = _theImage , let imageURL = URL(string: theImage) else {
            self.image = placeHolder
            return
        }
        
        self.sd_setImage(with: imageURL, placeholderImage: placeHolder, options: [], completed: { (_, error,_ , _) in
            if error != nil{
                self.image = placeHolder
            }
        })
    }
    
}
